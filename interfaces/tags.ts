export class Tags {
  public static USERDATA_REG_READY: string = 'userData:reg:ready';
  public static USERDATA_READY: string = 'userData:ready';
  public static USERID_READY: string = 'userId:ready';
  public static USERHIST_READY: string = 'userHist:ready';
  public static MENUESD_READY: string = 'menuesD:ready';
  public static MENUESL_READY: string = 'menuesL:ready';
  public static MENUESLD_READY: string = 'menuesLD:ready';
  public static NEWID_READY: string = 'newId:ready';
  public static IGONOW_READY: string = 'iGoNow:ready';
  public static MENSAOPEN_DINNER: string = 'mensaOpen:dinner';
  public static MENSAOPEN_DAY: string = 'mensaOpen:day';
  public static MENSAOPEN_TIME: string = 'mensaOpen:time';
  public static MENSAUSE_READY: string = 'mensaUse:ready';
  public static MENSALINE_READY: string = 'mensaLine:ready';


  public static INTROENTER: number = 0;
  public static INTRO1: number = 1;
  public static INTRO2: number = 2;
  public static INTRO3: number = 3;
  public static INTRO4: number = 4;
  public static INTRO5: number = 5;
  public static INTRO6: number = 6;
  public static INTRO7: number = 7;
  public static INTRO8: number = 8;
  public static INTROCHANGENAME: number = 9;
  public static INTROCHANGEAGE: number = 10;
  public static INTROSELECTGENDER: number = 11;
  public static INTROCHANGESIZE: number = 12;
  public static INTROCHANGEWEIGHT: number = 13;
  public static INTROSELECTACTIVITY: number = 14;
  public static INTROSELECTBREAKFAST: number = 15;
  public static INTROINSERTSNACKS: number = 16;
  public static INTROGOTOHOMEFROMLASTINTROPAGE: number = 17;
  public static INTROTOCHAT: number = 18;
  

  public static INTRO: string[] = [
    "ii0000",                        // goto intro 0
    "ii0001",                        // goto intro 1 
    "ii0002",                        // goto intro 2
    "ii0003",                        // goto intro 3
    "ii0004",                        // goto intro 4
    "ii0005",                        // goto intro 5
    "ii0006",                        // goto intro 6
    "ii0007",                        // goto intro 7
    "ii0008",                        // goto intro 8
    "ii0010",                       // change Name
    "ii0011",                       // change Age
    "ii0012",                        // select Gender
    "ii0013",                       // change Size
    "ii0014",                       // change Weight
    "ii0015",                       // select1 / cancel0 Activity
    "ii0016",                       // select1 / cancel0 Breakfast
    "ii0017",                       // insert Snackts
    "ii0018",                       // gotoHome from LastPage
    "iic000",                        // switch direct from Intro to chat
    ];

  public static TABS: string[] = [
    "mt000",      
    "mt001",
    "mt002",
    "mt003",
    "mt004",
    ];

  public static MENUECHATSWITCH: number = 0;
  public static MENUEGOTODAY: number = 1;
  public static MENUEGOBACK: number = 2;
  public static MENUEGONEXT: number = 3;
  public static MENUESHOWRECEPT: number = 4;
  public static MENUESHOWRECEPTADD: number = 5;
  public static MENUESHOWRECEPTCANCEL: number = 6;
  public static MENUESWAPLD: number = 7;
  public static MENUEADDHISTORY: number = 8;
  public static MENUEENTER: number = 9;
  public static MENUESETTINGSTOAST: number = 10;


  public static MENUE: string[] = [
    "mmc000",                        //switch from Menue to chat
    "mm0001",                        //goToday 
    "mm0002",                        //goBack 
    "mm0003",                        //goNext 
    "mm0004",                        //showRecept 
    "mm00040",                       //showRecept Add
    "mm00041",                       //showRecept Cancel + 0 when no profil
    "mm0005",                        // Lunch/Dinner change results in mm00050 und mm00051
    "mm0006",                        // Add menues to history
    "mm0000",                        // EnterMenues
    "mm0007",                        // CloseSettingsToast
    
    ];

  public static HISTORYENTER: number = 0;
  public static HISTORYSHOWINFO: number = 1;
  public static HISTORYCHARTCLICKED: number = 2;
  public static HISTORYADDFOODALERT: number = 3;
  public static HISTORYADDFOODALERTCHOOSEDATE: number = 4;
  public static HISTORYADDFOODALERTCLOSE: number = 5;
  public static HISTORYADDFOODALERTDATECHOOSEN: number = 6;
  public static HISTORYADDFOODMODULEOPEN: number = 7;
  public static HISTORYADDFOODFOODADDED: number = 8;
  public static HISTORYSWITCHTOCHAT: number = 9;
  public static HISTORYEATENDISHES: number = 10;

  public static HISTORY: string[] = [
    "mh1000",                        // Enter 
    "mh1001",                        // show Info for Charts 
    "mh1002",                        // Chart clicked 
    "mh1003",                        // FAB button for add dish to history clicked
    "mh10031",                       // continue with choosedate
    "mh10032",                       // cancel back to histoy
    "mh10033",                       // Date choosen from datepicker
    "mh10034",                       // Choose-Food Modul open
    "mh10035",                       // Dish chosen and added
    "mhc000",                        // switched from history to chat
    "mhc1004",                       // eaten dishes clicked
    ];


  public static RECOENTER: number = 0;
  public static RECOSWITCHTOCHAT: number = 1;
  public static RECOCHANGEMENUTIME: number = 2;
  public static RECOCHANGEMEALTYP: number = 3;
  public static RECOCHANGEMENUTYPD: number = 4;
  public static RECOCHANGEMENUTYPL: number = 5;
  public static RECODIDNOTWORK: number = 6;
  public static RECORECOMMENDATIONBUTTON: number = 7;
  public static RECOIWILLEATTHIS: number = 8;
  public static RECOSHOWINFO: number = 11;
  public static RECORECOMMENDATIONTOASTBECAUSEMISSINGPROFIL: number = 9;
  public static RECOTOASTWHATTODO: number = 10;
  public static RECOCLICKEDONRECOCARD: number = 12;


  public static RECO: string[] = [
     "mr2000",                        // Enter 
     "mrc200",                        // switch from recommendation to chat
     "mr2001",                        // change menutime to 0 lunch / 1 for dinner
     "mr2002",                        // change mensa menu size typ
     "mr2003",                        // change menutyp dinner
     "mr2004",                        // change menutyp lunch Type
     "mr2005",                        // reco did not work because profile not ready
     "mr2006",                        // clicked to get recommendations
     "mr20061",                       // i will eat this clicked 0 cancel, 1 add to history
     "mr20062",                       // Profile infos missing
     "mr2007",                        // toast what to do: 0 okay, 1 never show again
     "mr2007",                        // show info was clicked -> 0 ok
     "mr2008",                        // clicked on Recocard with number + index 
    ];

  public static WEBCAMENTER: number = 0;
  public static WEBCAMCHECKINNOW: number = 1;
  public static WEBCAMCHECKINNOWCANCEL: number = 2;
  public static WEBCAMCHECKINNOWBUTCLOSED: number = 3;
  public static WEBCAMCHECKINLATER: number = 4;
  public static WEBCAMCHECKINLATERCANCEL: number = 5;
  public static WEBCAMCHECKINLATERBUTCLOSED: number = 6;
  public static WEBCAMCHECKINLATERSAVED: number = 7;
  public static WEBCAMSHOWDAY: number = 8; 
  public static WEBCAMSHOWDAYCANCEL: number = 9; 
  public static WEBCAMSWITCHTOCHAT: number = 10; 
  public static WEBCAMTOASTWHATTODO: number = 11; 


  public static WEBCAM: string[] = [
    "mw3000",                        //ENTER
    "mw3001",                        //checked in now clicked
    "mw30010",                       //checked in now cancel clicked
    "mw30011",                       //checked in now clicked but mensa closed
    "mw3002",                        //check in later 
    "mw30020",                       //check in later cancel
    "mw30021",                       //check in later clicked but mensa closed
    "mw30022",                       //check in later hour saved
    "mw3003",                        //clicked on date (average visitors)
    "mw30030",                       //clicked on date (average visitors) but cancel
    "mwc300",                        //switch from webcam to chat
    "mwc3004",                        //webcam toast what to do closed
    ];


  public static SETTINGSENTER: number = 0;
  public static SETTINGSWITCHTOCHAT: number = 1;
  public static SETTINGSCHANGENAME: number = 2;
  public static SETTINGSCHANGEGENDER: number = 3;
  public static SETTINGSCHANGEAGE: number = 4;
  public static SETTINGSCHANGEWEIGHT: number = 5;
  public static SETTINGSCHANGEHEIGHT: number = 6;
  public static SETTINGSCHANGEACTITYP: number = 7;
  public static SETTINGSCHANGEBREAKFAST: number = 8;
  public static SETTINGSCHANGESNACKS: number = 9;
  public static SETTINGSPERSONALITYLINK: number = 10;
  public static SETTINGSRECOMMENDATIONLINK: number = 11;
  public static SETTINGSSAVESETTINGSBUTTON: number = 12;
  public static SETTINGSPROFILINFOMISSING: number = 13;
  public static SETTINGSCHANGESETTINGSBUTTONCANCEL: number = 14;
  public static SETTINGSGETMOREINFO: number = 15;
  public static SETTINGSDOSURVEY: number = 16;


  public static SETTINGS: string[] = [
    "ms4000",                        // Enter
    "msc400",                        //switch from Settings to chat
    "ms4001",                        // name change (ie Return) 
    "ms4002",                        // gender changed ( 0 cancel, 1 change) 
    "ms4003",                        // age blur 
    "ms4004",                        // weight blur 
    "ms4005",                        // height blur
    "ms4006",                        // activity type ( 0 cancel, 1 change)
    "ms4007",                        // breakfast type ( 0 cancel, 1 change)
    "ms4008",                        // snack blur
    "ms4009",                        // link to personality questionary clicked
    "ms4010",                        // link to more information about recommendations clicked
    "ms4011",                        // click save settings -> 1 changed profile -> 0 register
    "ms4012",                        // profile settings missing alert
    "ms4013",                        // toast not able to change settings ok
    "ms4020",                        // get more info about recommendations
    "ms4030",                        // do survey
    ];




//CHAT

  public static HOMECHATSWITCH: number = 0;
  public static HOMECHATCHATS: number = 1;
  public static HOMECHATIATE: number = 3;
  public static HOMECHATMENUE: number = 2;
  public static HOMECHATRECOMMENT: number = 4;
  public static HOMECHATWEBCAM: number = 5;
  public static HOMECHATSETTINGS: number = 6;
  public static HOMECHATINTRO: number = 7;
  public static HOMECHATCONT: number = 8;

  public static HOMECHAT: string[] = [
    "ccm000",                        //switch from homechat to menu 
    "cc0001",                        // open chats list 
    "cm0002",                        // start show menu chat
    "cm0003",                        // start I ate chat
    "cm0004",                        // start recommendation chat
    "cm0005",                        // start WEBCAM chat
    "cm0006",                        // start show settings chat
    "cm0007",                        // intro chat started
    "cm0008",                        // continue a left chat
    ];


  public static INTRORUNNER0s0: number = 0;
  public static INTRORUNNER0s1: number = 1;   
  public static INTRORUNNER0s2: number = 2;   
  public static INTRORUNNER0s3: number = 3;   
  public static INTRORUNNER0s4: number = 4; 
  public static INTRORUNNER0s5: number = 5; 
  public static INTRORUNNER0s6: number = 6;
  public static INTRORUNNER0s7: number = 7;
  public static INTRORUNNER0s8: number = 8;  
  public static INTRORUNNER0s9: number = 9;    
  public static INTRORUNNER0s10: number = 10;
  public static INTRORUNNER_0s10: number = 18; 
  public static INTRORUNNER1s0: number = 11;
  public static INTRORUNNER1s1: number = 12;   
  public static INTRORUNNER1s2: number = 13;   
  public static INTRORUNNER1s3: number = 14;
  public static INTRORUNNER1s4: number = 15;  
  public static INTRORUNNER1s5: number = 16;  
  public static INTRORUNNER1s6: number = 17;  
  public static INTRORUNNER1s9: number = 19;
               
  public static INTRORUNNER: string[] = [
    "cim0000x0s0",                        //Enter intro  
    "cim0000x0s1",                        //Tell me more
    "cim0000x0s2",                        //gender changed to female
    "cim0000x0s3",                        //breakfast habits changed
    "cim0000x0s4",                        //age changed
    "cim0000x0s5",                        //height changed
    "cim0000x0s6",                        //weight changed
    "cim0000x0s7",                        //activity want to change
    "cim0000x0s8",                        //activity changed, should save? and see now overview, save 1
    "cim0000x0s10",                       //Bye
    "cim0000x1s0",                        //Bye
    "cim0000x1s1",                        //change name?
    "cim0000x1s2",                        //gender male chosen
    "cim0000x1s3",                        //
    "cim0000x1s4",                        //
    "cim0000x_0s10",                      //Bye
    "cim0000x1s5",                        //Bye
    "cim0000x1s6",                        //Bye
    "cim0000x1s9",                        //OK
  ];


  public static IATERUNNER0s0: number = 0;
  public static IATERUNNER0s1: number = 1;
  public static IATERUNNER0s2: number = 2;
  public static IATERUNNER0s3: number = 3;
  public static IATERUNNER0s4: number = 4;
  public static IATERUNNER0s5: number = 5;
  public static IATERUNNER0s6: number = 6;
  public static IATERUNNER0s7: number = 7;
  public static IATERUNNER0s8: number = 8;
  public static IATERUNNER0s9: number = 9;
  public static IATERUNNER0s10: number = 10;
  public static IATERUNNER1s0: number = 11;
  public static IATERUNNER1s1: number = 12;
  public static IATERUNNER1s2: number = 13;
  public static IATERUNNER1s3: number = 14;
  public static IATERUNNER1s4: number = 15;
  public static IATERUNNER1s5: number = 16;
  public static IATERUNNER1s6: number = 17;
  public static IATERUNNER1s7: number = 18;
  public static IATERUNNER1s8: number = 19;
  public static IATERUNNER0s12: number = 20;
  public static IATERUNNER0s11: number = 21;

  public static IATERUNNER: string[] = [
    "cm0002x0s0",                //yep button to i ate runner enter
    "cm0002x0s1",                //today/yesterday selected
    "cm0002x0s2",                //EMPTY
    "cm0002x0s3",                //EMPTY
    "cm0002x0s4",                //want to log Lunch
    "cm0002x0s5",                //Lunch/Dinner was selected
    "cm0002x0s6",                //Another day for selection after no selection in checkbox
    "cm0002x0s7",                //Want to know more about history - 0 without profil
    "cm0002x0s8",                //see more about history
    "cm0002x0s9",                //Thanks and bye
    "cm0002x0s10",               //want to log Dinner
    "cm0002x1s0",                //Nope from already eat something(goodbye)
    "cm0002x1s1",                //Log Another Day
    "cm0002x1s2",                //show history 
    "cm0002x1s3",                //Calendar Cancel selected
    "cm0002x1s4",                 //Decision for Dinner
    "cm0002x1s5",                 //Nothing selected from menue
    "cm0002x1s6",                 //No other day and Bye
    "cm0002x1s7",                 //No Lunch/Dinner want to log after something was logged
    "cm0002x1s8",                 //EMPTY
    "cm0002x0s011",                //impossible day choosed from datepicker
    "cm0002x0s12",                //See dishes i ate

  ];

  public static MENUECHATRUNNER0s0: number = 0;
  public static MENUECHATRUNNER0s1: number = 1;
  public static MENUECHATRUNNER0s2: number = 2;
  public static MENUECHATRUNNER0s3: number = 3;
  public static MENUECHATRUNNER0s4: number = 4;
  public static MENUECHATRUNNER0s5: number = 5;
  public static MENUECHATRUNNER0s6: number = 6;
  public static MENUECHATRUNNER0s7: number = 7;
  public static MENUECHATRUNNER0s8: number = 8;
  public static MENUECHATRUNNER0s9: number = 9;
  public static MENUECHATRUNNER0s10: number = 10;
  public static MENUECHATRUNNER0s11: number = 11;
  public static MENUECHATRUNNER0s12: number = 12;
  public static MENUECHATRUNNER1s0: number = 13;
  public static MENUECHATRUNNER1s1: number = 14;
  public static MENUECHATRUNNER1s2: number = 15;
  public static MENUECHATRUNNER1s3: number = 16;
  public static MENUECHATRUNNER1s4: number = 17;
  public static MENUECHATRUNNER1s5: number = 17;
  public static MENUECHATRUNNER1s6: number = 19;
  public static MENUECHATRUNNER1s7: number = 20;
  public static MENUECHATRUNNER1s8: number = 21;
  public static MENUECHATRUNNER1s9: number = 22;
  public static MENUECHATRUNNER: string[] = [
    "cm0003x0s0",   //Enter with Yes
    "cm0003x0s1",   //Today selected -> Mensa closed 9
    "cm0003x0s2",   //EMPTY
    "cm0003x0s3",   //Lunch for Menu 
    "cm0003x0s4",   //Thank you
    "cm0003x0s5",   //EMPTY
    "cm0003x0s6",   //Another Day for menu
    "cm0003x0s7",   //Okay for can you do something
    "cm0003x0s8",   //EMPTY
    "cm0003x0s9",   //Quit
    "cm0003x0s10",  //EMPTY
    "cm0003x0s11",  //Time for when going mensa chosen
    "cm0003x0s12",  //Going to Mensa Today True
    "cm0003x1s0",   //Cancel from Time Picker /No from Enter
    "cm0003x1s1",   //Another Day
    "cm0003x1s2",   //EMPTY
    "cm0003x1s3",   //CalenderShown and Cancel clicked
    "cm0003x1s4",   //Show me Dinner
    "cm0003x1s5",   //From MenuPopup with no selection
    "cm0003x1s6",   //No Time for help Buddy
    "cm0003x1s7",   //EMPTY
    "cm0003x1s8",   //EMPTY
    "cm0003x1s9",   //Dinner Time CheckIn
  ];

  public static RECOMMENDERCHATRUNNER0s0: number = 0;
  public static RECOMMENDERCHATRUNNER0s1: number = 1;
  public static RECOMMENDERCHATRUNNER0s2: number = 2;
  public static RECOMMENDERCHATRUNNER0s3: number = 3;
  public static RECOMMENDERCHATRUNNER0s4: number = 4;
  public static RECOMMENDERCHATRUNNER0s5: number = 5;
  public static RECOMMENDERCHATRUNNER0s6: number = 6;
  public static RECOMMENDERCHATRUNNER0s7: number = 7;
  public static RECOMMENDERCHATRUNNER1s0: number = 8;
  public static RECOMMENDERCHATRUNNER1s1: number = 9;
  public static RECOMMENDERCHATRUNNER1s2: number = 10;
  public static RECOMMENDERCHATRUNNER1s3: number = 11;
  public static RECOMMENDERCHATRUNNER0s8: number = 12;
  public static RECOMMENDERCHATRUNNER0s11: number = 13;
  public static RECOMMENDERCHATRUNNER0s12: number = 14;
  public static RECOMMENDERCHATRUNNER0s13: number = 15;
  public static RECOMMENDERCHATRUNNER: string[] = [
    "cm0004x0s0", // Great give me recommendations
    "cm0004x0s1", // Exactly give recommendation
    "cm0004x0s2", // recommendation for lunch
    "cm0004x0s3", // Okay to one other thing to know
    "cm0004x0s4", // best choices 1 recom -> 0 no recos
    "cm0004x0s5", // show another -> 0 no more recos
    "cm0004x0s51", // show more -> 0 no more recos
    "cm0004x0s7", // what kind of dinner/lunch
    "cm0004x1s0", // bye
    "cm0004x1s1", // not possible
    "cm0004x1s2", // reco for dinner
    "cm0004x1s3", //i will eat this recommendation
    "cm0004x0s8", //save to history 
    "cm0004x0s52", //show more -> 0 no more recos
    "cm0004x0s53", //show more -> 0 no more recos
    "cm0004x0s54", //show more -> 0 no more recos
  ];

  public static WEBCAMRUNNER0s0: number = 0;
  public static WEBCAMRUNNER0s1: number = 1;
  public static WEBCAMRUNNER0s2: number = 2;
  public static WEBCAMRUNNER0s3: number = 3;
  public static WEBCAMRUNNER0s4: number = 4;
  public static WEBCAMRUNNER0s5: number = 5;
  public static WEBCAMRUNNER0s6: number = 6;
  public static WEBCAMRUNNER0s7: number = 7;
  public static WEBCAMRUNNER1s0: number = 8;
  public static WEBCAMRUNNER1s1: number = 9;
  public static WEBCAMRUNNER1s2: number = 10;
  public static WEBCAMCHATRUNNER: string[] = [
    "cm0005x0s0",   // Enter with Check-In  -> 0 when mensa not open -> 00 also not dinner
    "cm0005x0s1",   // Going to Mensa Now
    "cm0005x0s2",   // See waiting line yes YES
    "cm0005x0s3",   // Today for waiting line
    "cm0005x0s4",   // Tell me more
    "cm0005x0s5",   // When are you going today -> OKAY
    "cm0005x0s6",   // TIME CHOSEN
    "cm0005x0s7",   // DE NIENTE --> END
    "cm0005x1s0",   // BYE
    "cm0005x1s1",   // Enter wit prediction line
    "cm0005x1s2",   // TOMORROW
  ];


  public static SETTINGSRUNNER0s0: number = 0;
  public static SETTINGSRUNNER0s1: number = 1;
  public static SETTINGSRUNNER0s2: number = 2;
  public static SETTINGSRUNNER0s3: number = 3;
  public static SETTINGSRUNNER0s4: number = 4;
  public static SETTINGSRUNNER0s5: number = 5;
  public static SETTINGSRUNNER0s6: number = 6;
  public static SETTINGSRUNNER0s7: number = 7;
  public static SETTINGSRUNNER0s8: number = 8;
  public static SETTINGSRUNNER0s9: number = 9;
  public static SETTINGSRUNNER0s10: number = 10;
  public static SETTINGSRUNNER0s11: number = 11;
  public static SETTINGSRUNNER0s12: number = 12;
  public static SETTINGSRUNNER0s13: number = 13;
  public static SETTINGSRUNNER0s14: number = 26;
  public static SETTINGSRUNNER0s15: number = 27;
  public static SETTINGSRUNNER1s0: number = 14;
  public static SETTINGSRUNNER1s1: number = 15;
  public static SETTINGSRUNNER1s2: number = 16;
  public static SETTINGSRUNNER1s3: number = 17;
  public static SETTINGSRUNNER1s4: number = 18;
  public static SETTINGSRUNNER1s5: number = 19;
  public static SETTINGSRUNNER1s6: number = 20;
  public static SETTINGSRUNNER1s7: number = 21;
  public static SETTINGSRUNNER1s8: number = 22;
  public static SETTINGSRUNNER1s9: number = 23;
  public static SETTINGSRUNNER1s10: number = 24;
  public static SETTINGSRUNNER1s11: number = 25;
  public static SETTINGSRUNNER0s16: number = 26;
  public static SETTINGSRUNNER: string[] = [
    "cm0006x0s0",   //Let's Go Changes
    "cm0006x0s1",   //Change Weight
    "cm0006x0s2",   //Weight pushed
    "cm0006x0s3",   //Change Snacks
    "cm0006x0s4",   //Snacks pushed
    "cm0006x0s5",   //Change Breakfast
    "cm0006x0s6",   //Pick Breakfast
    "cm0006x0s7",   //Change Activity
    "cm0006x0s8",   //Go on with changes (4 more)
    "cm0006x0s9",   // Change Age
    "cm0006x0s10",  //New Height Entered
    "cm0006x0s11",  // NEW NAME 
    "cm0006x0s12",  //Change gender
    "cm0006x0s13",  //Gender has changed --> 1 male
    "cm0006x1s0",   //Cancel /Quit
    "cm0006x1s1",   //NO WEIGHT, CHANGE OTHER
    "cm0006x1s2",   //CANCEL
    "cm0006x1s3",   //NO Snacks, others
    "cm0006x1s4",   //EMPTY
    "cm0006x1s5",   //NO BREAKFAST, Other
    "cm0006x1s6",   //EMPTY
    "cm0006x1s7",   //no activity chnage, other
    "cm0006x1s8",   //EMPTY
    "cm0006x1s9",   //No new age, change height
    "cm0006x1s10",  //No new height, cnahnge name
    "cm0006x1s11",  //no new name, change gender
    "cm0006x0s14",  //activity changed
    "cm0006x0s15",  //save profil -> 1 saved 2 changed
    "cm0006x0s16",  //see overview before saving
  ];




  constructor() {}

}