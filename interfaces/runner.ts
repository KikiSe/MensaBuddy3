export interface Runner {
  init(caller: any, chat: any): void;
  cont0(): void;
  cont1(): void;
  setStates(yes: string, yesGoon: number, no:string, noGoon: number);
  setYesButton(yes: string);
}
