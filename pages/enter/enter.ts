import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import { UserModel } from '../../models/user-model';
import { Sergelution } from '../../providers/sergelution';
import { SwitchService }     from '../../services/switchservice';
import { Data } from '../../providers/userdata';
import { MiscData } from '../../providers/miscdata';
import { Tags } from '../../interfaces/tags';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-enter',
  templateUrl: 'enter.html',
})
export class EnterPage {
  user: UserModel = new UserModel();
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public sergelution: Sergelution,
    public misc: MiscData,
    public switchService: SwitchService,
    public userService: Data,
    ) {
      console.log("enter.ts")
       this.events.subscribe(Tags.USERDATA_READY, (user) => {
        this.events.unsubscribe(Tags.USERDATA_READY);
        this.user = user;
      });
      this.userService.prepData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Enter');
  }

  switch() {
      this.sergelution.postUserAction(this.user, Tags.INTRO[Tags.INTROTOCHAT]);
      this.misc.setLastTab(0);
      this.switchService.switchTo("chat");
  }

  goToHome(){
    this.sergelution.postUserAction(this.user, Tags.INTRO[17]);
 //   this.events.subscribe(Tags.USERDATA_READY, (data) => {
 //     this.events.unsubscribe(Tags.USERDATA_READY);
 //     this.sergelution.postUserData(data);
      this.navCtrl.setRoot(TabsPage);
 //   })
 //   this.changeModel();
    
  }
}
