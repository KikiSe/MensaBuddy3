import { Component } from '@angular/core';
import { Events,ModalController, NavController, NavParams,ViewController } from 'ionic-angular';
import { SurveyModel } from '../../models/survey-model';
import { UserModel } from '../../models/user-model';
import { Data } from '../../providers/userdata';
import { Sergelution } from '../../providers/sergelution';
import { Tags } from '../../interfaces/tags';

import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-modalsurvey',
  templateUrl: 'modalsurvey.html',
})
export class ModalSurvey {
  public survey: SurveyModel = new SurveyModel();
  public user: any;
  public prq_1: string[] = []; 
  public prq_2: string[] = []; 
  public prq_3: string[] = []; 
  public frequentlyMensa: string[] = []; 
  public prq_4: string[] = [];
  public prq_5: string[] = [];  
  public prq_6: string[] = [];
  public prq_7: string[] = [];
  public lts_1: string[] = [];
  public lts_2: string[] = [];  
  public lts_3: string[] = [];
  public lts_4: string[] = [];
  public usageReco: string[] = [];
  public persoReco: string[] = [];  
  public knowChatbot: string[] = [];
  public usageChatbot: string[] = [];
  public preferedUI: string[] = [];
  public askRecos: string[] = [];     
  public ateRecos: string[] = []; 
  public opinionGeneralReco: string[] = [];     
  public opinionBuddyReco: string[] = [];
  public setrestultshowing: boolean = false;
  public lerntyptextAR: string = '';
  public lerntyptextSI: string = '';
  public lerntyptextGS: string = '';
  public lerntyptextVV: string = '';
  public lerntyp_sens_text: string = "Sensing learners tend to like learning facts. Sensors often like solving problems by well-established methods and dislike complications and surprises. Sensors are more likely than intuitors to resent being tested on material that has not been explicitly covered in class. Sensors tend to be patient with details and good at memorizing facts and doing hands-on (laboratory) work. Sensors tend to be more practical and careful. Sensors don't like courses that have no apparent connection to the real world.";
  public lerntyp_intu_text: string = "Intuitive learners often prefer discovering possibilities and relationships. Intuitors like innovation and dislike repetition. Intuitors may be better at grasping new concepts and are often more comfortable with abstractions and mathematical formulations. Intuitors tend to work faster and to be more innovative. Intuitors don't like 'plug-and-chug' courses that involve a lot of memorization and routine calculations. "; 
  public lerntyp_vis_text: string = 'Visual learners remember best what they see pictures, diagrams, flow charts, time lines, films, and demonstrations.'; 
  public lerntyp_verb_text: string = 'Verbal learners get more out of words written and spoken explanations.'; 
  public lerntyp_refl_text: string = "Reflective learners prefer to think about it quietly first and prefer to work alone.  Let's think it through first is the reflective learner's response.";
  public lerntyp_act_text: string = "Active learners tend to retain and understand information best by doing something active with it discussing or applying it or explaining it to others. Let's try it out and see how it works is an active learners phrase. Active learners also tend to like group work."; 
  public lerntyp_glob_text: string = "Global learners tend to learn in large jumps, absorbing material almost randomly without seeing connections, and then suddenly 'getting it'. Global learners may be able to solve complex problems quickly or put things together in novel ways once they have grasped the big picture, but they may have difficulty explaining how they did it.";
  public lerntyp_sequ_text: string = 'Sequential learners tend to gain understanding in linear steps, with each step following logically from the previous one. Sequential learners tend to follow logical stepwise paths in finding solutions. ';
  public lerntyp_sens: string = 'Sensing';
  public lerntyp_intu: string = 'Intuitive'; 
  public lerntyp_vis: string = 'Visual'; 
  public lerntyp_verb: string = 'Verbal'; 
  public lerntyp_refl: string = 'Reflective'; 
  public lerntyp_act: string = 'Active';
  public lerntyp_glob: string = 'Global'; 
  public lerntyp_sequ: string = 'Sequential';
  public typAR: string = "";
  public typSI: string = "";
  public typVV: string = "";
  public typGS: string = "";
  public question1: string = "How often do you eat at the mensa usually?";
  public question2: string = "I liked the items recommended by the system.";
  public question3: string = "The recommended items fitted my preference.";
  public question4: string = "The recommended items were well-chosen.";
  public question5: string = "The recommended items were relevant.";
  public question6: string = "The system recommended too many bad items.";
  public question7: string = "I did not like any of the recommended items.";
  public question8: string = "The items I selected were the 'best among the worst'.";
  public question9: string = "I would rather first";
  public question10: string = "It is more important to me that an instructor";
  public question11: string = "I prefer lectures that emphasize";
  public question12: string = "I prefer to get new information in ";

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController, 
    public navParams: NavParams,
    public events: Events,
    public userService: Data,
    public sergelution: Sergelution,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController
    ) {
        
        for(let i = 0, j=SurveyModel.FREQUENTLYMENSA.length;i<j;i++) {
            this.frequentlyMensa.push(SurveyModel.FREQUENTLYMENSA[i]);
        } 
         for(let i = 0, j=SurveyModel.PRQ1.length;i<j;i++) {
            this.prq_1.push(SurveyModel.PRQ1[i]);
        }
        for(let i = 0, j=SurveyModel.PRQ2.length;i<j;i++) {
            this.prq_2.push(SurveyModel.PRQ2[i]);
        } 
        for(let i = 0, j=SurveyModel.PRQ3.length;i<j;i++) {
            this.prq_3.push(SurveyModel.PRQ3[i]);
        } 
        for(let i = 0, j=SurveyModel.PRQ4.length;i<j;i++) {
            this.prq_4.push(SurveyModel.PRQ4[i]);
        } 
        for(let i = 0, j=SurveyModel.PRQ5.length;i<j;i++) {
            this.prq_5.push(SurveyModel.PRQ5[i]);
        } 
        for(let i = 0, j=SurveyModel.PRQ6.length;i<j;i++) {
            this.prq_6.push(SurveyModel.PRQ6[i]);
        } 
        for(let i = 0, j=SurveyModel.PRQ7.length;i<j;i++) {
            this.prq_7.push(SurveyModel.PRQ7[i]);
        } 
        for(let i = 0, j=SurveyModel.LSQ1.length;i<j;i++) {
            this.lts_1.push(SurveyModel.LSQ1[i]);
        }
        for(let i = 0, j=SurveyModel.LSQ2.length;i<j;i++) {
            this.lts_2.push(SurveyModel.LSQ2[i]);
        } 
        for(let i = 0, j=SurveyModel.LSQ3.length;i<j;i++) {
            this.lts_3.push(SurveyModel.LSQ3[i]);
        } 
        for(let i = 0, j=SurveyModel.LSQ4.length;i<j;i++) {
            this.lts_4.push(SurveyModel.LSQ4[i]);
        }
        
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Modalsurvey');
     this.events.subscribe(Tags.USERDATA_READY, (data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      console.log("ionViewDidEnter() first time data received");
      console.log(data);
      this.user = data;
      this.survey.surveyid = this.user.id;
      this.survey.surveyage = this.user.age;
      this.survey.surveygender = this.user.gender;
      });
    this.userService.prepData();
  }
  
  isAllSet(): boolean {
    let isOk = true;
    if(this.survey.frequentlyMensa == 0) isOk = false;
    if(this.survey.prq_1 == 0) isOk = false;
    if(this.survey.prq_2 == 0) isOk = false;
    if(this.survey.prq_3 == 0) isOk = false;
    if(this.survey.prq_4 == 0) isOk = false;
    if(this.survey.prq_5 == 0) isOk = false;
    if(this.survey.prq_6 == 0) isOk = false;
    if(this.survey.prq_7 == 0) isOk = false;
    if(this.survey.lt_SI == 0) isOk = false;
    if(this.survey.lt_VV == 0) isOk = false;
    if(this.survey.lt_AR == 0) isOk = false;
    if(this.survey.lt_GS == 0) isOk = false;

    return isOk;
  }



  sendSurvey(){
    if(this.isAllSet()){
        this.events.subscribe(Sergelution.SURVEY_READY, (ret) => {
            this.events.unsubscribe(Sergelution.SURVEY_READY);
            this.events.unsubscribe(Sergelution.SURVEY_READY_ERR);
            let alert = this.alertCtrl.create({
              title: "Survey Data transmitted",
              message: "Thanks for the time spended!", 
              buttons: [{text: 'Show me result',role: 'cancel',handler: ( (data) => { this.showResults(); return;}) }]
            });
            alert.present();
        });
        this.events.subscribe(Sergelution.SURVEY_READY_ERR, (err) => {
            this.events.unsubscribe(Sergelution.SURVEY_READY);
            this.events.unsubscribe(Sergelution.SURVEY_READY_ERR);
            let alert = this.alertCtrl.create({
              title: "Survey Data transmit error",
              message: err,
              buttons: [{text: 'Back',role: 'cancel',}]
            });
            alert.present();
        });
        this.sergelution.postSurveyData(this.user, this.survey);
       
        console.log('survey was send');
        //sergelution send survey
    }else{
        let alert = this.alertCtrl.create({
              title: "Missing Answers",
              message: 'Please fulfill all questions!',
              buttons: [{text: 'Back',role: 'cancel',}]
            });
            alert.present();
    }

    console.log(this.survey);
    
  }
  dismiss() {
      this.viewCtrl.dismiss();
  }
  showResults() {
    if(this.survey.lt_AR == 1){
      this.typAR = this.lerntyp_act;
      this.lerntyptextAR = this.lerntyp_act_text;
    }else{
      this.typAR = this.lerntyp_refl;
      this.lerntyptextAR = this.lerntyp_refl_text;
    }
    if(this.survey.lt_GS == 1){
      this.typGS = this.lerntyp_sequ;
      this.lerntyptextGS = this.lerntyp_sequ_text;
    }else{
      this.typGS = this.lerntyp_glob;
      this.lerntyptextGS = this.lerntyp_glob_text;
    }
    if(this.survey.lt_SI == 1){
      this.typSI = this.lerntyp_sens;
      this.lerntyptextSI = this.lerntyp_sens_text;
    }else{
      this.typSI = this.lerntyp_intu;
      this.lerntyptextSI = this.lerntyp_intu_text;
    }
    if(this.survey.lt_VV == 1){
      this.typVV = this.lerntyp_vis;
      this.lerntyptextVV = this.lerntyp_vis_text;
    }else{
      this.typVV = this.lerntyp_verb;
      this.lerntyptextVV = this.lerntyp_verb_text;
    }  
    this.setrestultshowing = true;
    
  }

}
