import { Component, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ModalController, AlertController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';

//import { DatePicker } from'ionic-native';

import { SwitchService }     from '../../services/switchservice';

import { Unibz }     from '../../providers/unibz';
import { Sergelution }     from '../../providers/sergelution';
import { Recommendation }     from '../../providers/recommendation';
import { TimeTicker }     from '../../providers/akttime';

import { MiscData }     from '../../providers/miscdata';
import { Data }     from '../../providers/userdata';
import { Datum }     from '../../providers/datum';

import {ModalMenueLD }     from '../modalmenueld/modalmenueld';
import {MenueModel }     from '../../models/menue-model';
import { Tags } from '../../interfaces/tags';

import * as moment from 'moment';

@Component({
  selector: 'homemenu-page',
  templateUrl: 'homemenu.html',
  providers: [Unibz]
})


export class HomeMenuPage implements OnDestroy {
  @ViewChild('datePicker') datePicker;

  
  public user: any;
  public today: string = '2017-7-3';
  public minday: string = '2017-1-1';
  public maxday: string = '2017-31-12';

  public first:  MenueModel[] ;
  public second:  MenueModel[] ;
  public side:  MenueModel[] ;
  public needs: any;
  public history: any;
  public weeklyHistory: any;
  public eatendishes: any;
  public dishes: any[] = [];
  public dishesAll: any[] = [];

  public  idealcarbs: number = 0;
  public  idealfiber: number = 0;
  public  idealcalories: number = 0;
  public  idealproteins: number = 0;
  public  idealfats: number = 0;
  public  idealSFA: number = 0;
  public  idealsalt: number = 0;
  public  idealsugars: number = 0;
  public  idealcarbsweekly: number = 0;
  public  idealfiberweekly: number = 0;
  public  idealcaloriesweekly: number = 0;
  public  idealproteinsweekly: number = 0;
  public  idealfatsweekly: number = 0;
 /* public  idealSFAweekly: number = 1500;
  public  idealsaltweekly: number = 1000;
  public  idealsugarsweekly: number = 1000;*/

  public carbs: number = 0;
  public fiber: number = 0;
  public proteins: number = 0;
  public fats: number = 0;
  public calories: number = 0;
 /* public salt: number = 0;
  public sugars: number = 0;
  public sfa: number = 0;
*/
  
  public wCarb: number = 0;
  public wFiber: number = 0;
  public wProteins: number = 0;
  public wFats: number = 0;
  public wKcals: number = 0;
  /*public wsalt: number = 0;
  public wsugars: number = 0;
  public wsfa: number = 0;*/

  public dailyHistoryHeader: string = "History of your daily Nutritions"
  public weeklyHistoryHeader: string = "History of your weekly Nutritions"
  

  public bcHeader: string = "What did you eat exactly?";
  public bcDataset: any = [{data: [0,0,0,0,0], label: ""}]; //,{data: [0,0,0,0,0],label: ""} ];
  public bcDatasetweek: any = [{data: [0,0,0,0,0], label: ""}]; //,{data: [0,0,0,0,0],label: ""} ];
  public bcLabels: any = ["ddd", "", "", "", ""];
  public bcOptions:any = { responsive: true, scaleShowVerticalLines: false, scales: {
          xAxes: [{
            display: true,
            gridLines : {
                display : false
            }
          }],
          yAxes: [{
            ticks: { min: 0, 
                     max: 100 ,
                     callback: function(label, index, labels){ return label + " %"}
                   },
            display: true,
            gridLines : {
                display : true
            }
          }],} };
  public bcColors:Array<any> = [
    { // grey
      backgroundColor: [ 'rgba(255, 23, 68, 0.7)', 'rgba(255, 235, 59, 0.7)',  'rgba(3, 169, 244, 0.7)', 'rgba(0, 203, 118, 0.7)', 'rgba(101, 31, 255, 0.7)', 'rgba(0, 229, 255, 0.7)'/*,'rgba(63, 81, 181, 0.7)','rgba(205, 220, 57, 0.7)','rgba(96, 125, 139, 0.7)'*/],     
      borderColor: [ 'rgba(255, 23, 68, 1)','rgba(255, 235, 59, 1)', 'rgba(3, 169, 244, 1)', 'rgba(0, 203, 118, 1)', 'rgba(101, 31, 255, 1)', 'rgba(0, 229, 255, 1)'/*,'rgba(63, 81, 181, 1)','rgba(205, 220, 57, 1)','rgba(96, 125, 139, 1)'*/ ],
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: [ 'rgba(255, 23, 68, 0.7)', 'rgba(255, 235, 59, 0.7)',  'rgba(3, 169, 244, 0.7)', 'rgba(0, 203, 118, 0.7)', 'rgba(101, 31, 255, 0.7)', 'rgba(0, 229, 255, 0.7)'/*,'rgba(63, 81, 181, 0.7)','rgba(205, 220, 57, 0.7)','rgba(96, 125, 139, 0.7)'*/],     
      borderColor: [ 'rgba(255, 23, 68, 1)','rgba(255, 235, 59, 1)', 'rgba(3, 169, 244, 1)', 'rgba(0, 203, 118, 1)', 'rgba(101, 31, 255, 1)', 'rgba(0, 229, 255, 1)','rgba(63, 81, 181, 1)'/*,'rgba(205, 220, 57, 1)','rgba(96, 125, 139, 1)'*/ ],
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];

 public bcLegend:boolean = false;
 public bcType = 'bar';
 public bcTypeWeek = 'bar';

 public showDatePicker:boolean = false;
 
  constructor(
    public navCtrl: NavController,
    public events: Events,
    public innerNav: NavController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public misc: MiscData,
    public datum: Datum,
    public userData: Data,
    public switchService: SwitchService,
    public unibz: Unibz,
    public sergelution: Sergelution,
    public changeDet: ChangeDetectorRef,
    public tt: TimeTicker,
    ) { }

  ionViewDidLoad() {
     this.events.subscribe(Tags.USERDATA_READY,(user) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = user;
//      this.buildidealneeds();
//      this.buildEatenDishes();
    });
    this.userData.prepData();
  }

  ionViewDidEnter() {
    console.log("homemenu: ionViewDidEnter");
    this.events.subscribe(Tags.USERDATA_READY,(user) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      console.log("homemenu: ionViewDidEnter data received");
      this.user = user;
      console.log("ionviewenter: ");
      console.log(this.user);
      if(!user.logged) {
        let alert = this.alertCtrl.create({
          title: "Profile missing",
          message: "Please complete your Profile",
          buttons: [{ text: 'Ok', role: 'cancel', handler: ( (data) => { alert.dismiss(); return;}) }]
        });
       alert.present();
      } else {
        this.buildEatenDishes();
        this.buildBarChartLabels();
        this.buildidealneeds(); 
      }
      this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYENTER]);
    });
    this.userData.prepData();
    console.log("homemenu: ionViewDidEnter prepData");
  
  }

  switch() {
    this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYSWITCHTOCHAT]);
    this.events.unsubscribe(Tags.USERDATA_READY);    
    this.misc.setLastTab(this.navCtrl.parent.getSelected().index);
    this.switchService.switchTo("chat");
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    //this.subscription.unsubscribe();
    console.log("homemenu: onDestroy");
    this.events.unsubscribe('userHist:ready');
  }
  ngDoCheck(e) {
  //  console.log("ngDoCheck");
}

  buildBarChartLabels(): void {
    this.bcLabels = [ "kCal" , "Carbs", "Fiber", "Proteins", "Fats"/*, "Sugar", "SFA", "Salt"*/];
  }

  buildEatenDishes(){
    this.events.subscribe(Unibz.UNIBZ_EATENDISHES, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
      this.dishes = data;
      /*this.eatendishes.forEach(element => {
        this.dishes.push(element);
        console.log(this.dishes);
      });*/
     // console.log(this.eatendishes)
      
    });
    this.events.subscribe(Unibz.UNIBZ_EATENDISHES_ERR, (error) => {
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
      console.log("ERROR EATEN");
      let alert2 = this.alertCtrl.create({
        title: "Error",
        message: "Sorry something went wrong.. "+ error,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert2.present();
    });
    this.unibz.getEatenDishes(this.user, moment().format("YYYY-MM-DD"));

  }


  buildidealneeds() {
    this.events.subscribe(Unibz.UNIBZ_DAILYNEEDS, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS);
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS_ERR);
      this.needs = data;
      this.idealcalories = this.needs.calories;
      console.log("idealcal: " + this.idealcalories);
      this.idealcarbs = this.needs.max_carbs;
      console.log("idealcarbs: " + this.idealcarbs);
      this.idealfats = this.needs.max_fats;
      console.log("idealfats: " + this.idealfats);
      this.idealfiber = this.needs.max_fiber;
      console.log("idealfiber: " + this.idealfiber);
      this.idealproteins = this.needs.proteins_PRI;
      console.log("idealproteins: " + this.idealproteins);
   //   this.idealsugars = this.needs.sugars;
   //   console.log("idealsugaaaarrr: " + this.idealsugars);
   //   this.idealSFA = this.needs.saturated_fatty_acids;
   //   console.log("idealsfa: " + this.idealSFA);
   //   this.idealsalt = this.needs.salt;
   //   console.log("saltideal: " + this.idealsalt);
      this.buildhistoryToday();
    });
     this.events.subscribe(Unibz.UNIBZ_DAILYNEEDS_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS);
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS_ERR);
      let alert = this.alertCtrl.create({
        title: "Error",
        message: "Sorry something went wrong.. "+ err,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
    //  this.buildhistoryToday();
    });
    this.unibz.getDailyNeeds(this.user);
  }

  buildhistoryToday(){
    console.log("buildhistoryToday: ");
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY,(data) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR);
      this.history = data;
      this.carbs = this.history.carbs;
      this.fiber = this.history.fiber;
      this.proteins = this.history.proteins;
      this.fats = this.history.fats;
      this.calories = this.history.kcal;
  /*    this.salt = this.history.salt;
      this.sfa = this.history.saturated_fatty_acids;
      this.sugars = this.history.sugars;*/
      console.log("history: " + this.history);
      this.buildhistoryWeekly();
    });

    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR);
      let alert = this.alertCtrl.create({
        title: "Missing History",
        message: "Add some dishes to your history!",
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      this.buildhistoryWeekly();
      return;
    });
    this.unibz.getNutritionalHistory(this.user, moment().format("YYYY-MM-DD"));
  }

  buildhistoryWeekly(){
    console.log("buildhistoryWeekly: ");
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY,(data) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR);
      console.log(data);
      this.weeklyHistory = data;
      this.buildBarChartDataToday(); 
      this.buildBarChartDataWeekly();
   });
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR,(err) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR);
      console.log(err);
      let alert = this.alertCtrl.create({
          title: "Error",
          message: err,
          buttons: [{ text: 'Ok', role: 'cancel', handler: ( (data) => { alert.dismiss(); return;}) }]
        });
       alert.present();
   });
   this.unibz.getNutritionalHistoryWeek(this.user);
  }
  
  buildBarChartDataToday(): void {
    let idealdata: number[] = [];
    /*
    idealdata.push(this.idealcalories)
    idealdata.push(this.idealcarbs);
    idealdata.push(this.idealfiber);
    idealdata.push(this.idealproteins);
    idealdata.push(this.idealfats);
    */
    idealdata.push(100)
    idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);
  /*  idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);
*/
    let data: number[] = [];
    data.push((100/this.idealcalories)*this.calories);
    data.push((100/this.idealcarbs)*this.carbs);
    data.push((100/this.idealfiber)*this.fiber);
    data.push((100/this.idealproteins)*this.proteins);
    data.push((100/this.idealfats)*this.fats);
   /* data.push((100/this.idealsugars)*this.sugars);
    data.push((100/this.idealSFA)*this.sfa);
    data.push((100/this.idealsalt)*this.salt);*/
   
 //   console.log([{data: data, label: 'ingredients'}]);
   // idealdata.push(this.idealcalories);

    this.bcDataset = [{data: data, label: 'nutrition %'}]; //,{data: idealdata,label: 'ingredients ideal' }];
    
}

buildBarChartDataWeekly(): void {
    this.idealcarbsweekly = this.idealcarbs*7;
    this.idealfiberweekly = this.idealfiber*7;
    this.idealproteinsweekly = this.idealproteins*7;
    this.idealfatsweekly = this.idealfats*7;
    this.idealcaloriesweekly= this.idealcalories*7;

  /*  let idealdata: number[] = [];
    idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);*/
   /* idealdata.push(100);
    idealdata.push(100);
    idealdata.push(100);
*/


    let data: number[] = [];
     this.wCarb = 0;
      this.wFiber = 0;
      this.wProteins = 0;
      this.wFats = 0;
      this.wKcals = 0;
    this.weeklyHistory.forEach((day)=> {
      this.wCarb += day.carbs;
      this.wFiber += day.fiber;
      this.wProteins += day.proteins;
      this.wFats += day.fats;
      this.wKcals += day.kcal;
   /*   this.wsugars += day.sugars;
      this.wsfa += day.saturated_fatty_acids;
      this.wsalt += day.salt;*/
    });
    data.push((100/this.idealcaloriesweekly)*this.wKcals);
    data.push((100/this.idealcarbsweekly)*this.wCarb);
    data.push((100/this.idealfiberweekly)*this.wFiber);
    data.push((100/this.idealproteinsweekly)*this.wProteins);
    data.push((100/this.idealfatsweekly)*this.wFats);
   /* data.push((100/this.idealsugarsweekly)*this.wsugars);
    data.push((100/this.idealSFAweekly)*this.wsfa);
    data.push((100/this.idealsaltweekly)*this.wsalt);*/
 //   data.push(calories);
    console.log([{data: data, label: 'ingredients weekly'}]);

   // console.log([{data: idealdata, label: 'ingredients ideal'}]);
    this.bcDatasetweek = [{data: data, label: 'nutrition %'}]; //,{data: idealdata,label: 'ingredients ideal' }];
    return this.bcDatasetweek;
}

  addToHistoryAlert(){
    let alert = this.alertCtrl.create({
      title: "Add dish to your History",
      message: 'By adding meals to your history, we can provide you better recommendations!',
      buttons: [
        {
          text: "Choose Date",
          handler: () => {
            this.openAddFoodinHistory();
            this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODALERTCHOOSEDATE]);
          }
        },
        {
          text: 'Back',
          role: 'cancel',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODALERTCLOSE]);
          }
        }
        ]
    });
    this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODALERT]);
    alert.present();
  }

  showEatenInfo(dat){
    this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYEATENDISHES]);
    dat.datum = moment().format("dddd");
    let alert = this.alertCtrl.create({
        title: "Dish Info",
        subTitle: "You ate " + dat.name + " on " + dat.datum,
        message: "Calories: " + dat.kcal + '<br />' + 
                              "Fats: " + dat.fats + '<br />' + 
//                              " containing " + data.saturated_fatty_acids + " saturated_fatty_acids" + '<br />' + 
                              "Carbs: " + dat.carbs + '<br />' + 
//                              " containig " + data.sugars + " sugars" + '<br />' + 
                              "Proteins: " + dat.proteins + '<br />' + 
                              "Fiber: " + dat.fiber +'<br />',
        buttons: [{ 
          text: 'Back', 
          role: 'cancel'}]
      });
      alert.present();
  }

  openAddFoodinHistory() {
     this.today = new Date().toISOString();
     let aktMoment = moment(this.today);
     this.maxday = aktMoment.format();
     this.minday = "2017";
     if((aktMoment.format("HH:mm") < '11:45')) {
       aktMoment.subtract(1, 'days');
       this.maxday = aktMoment.format();
       this.today = aktMoment.format();
     }
    //    console.log(this.datePicker);
     this.today = new Date().toISOString();
     // If done in this way, we need not to wait for these two ones.
     this.datePicker.min = this.minday;
     this.datePicker.max = this.maxday;
    this.showDatePicker = true;
    this.changeDet.detectChanges();

  //  console.log(this.datePicker);
    // because of setting the values today, min and max, datePicker is updated.
    // that takes some time, but we have no information when it is finished 
    // so we wait a little.
    //  this.datePicker.open();
     setTimeout(() => { 
      this.datePicker.open();
    }, 200);
  }


  ngOnChanges(changes) {
  //  console.log('ngOnChanges');
  //  console.log(changes);
  }

  showAlert(text: string) {
    let alert = this.alertCtrl.create({
      title: "Notification",
      message: text,
      buttons: [ { text: 'Ok', role: 'cancel' }]
    });
    alert.present();
  }


  public dayChanged() {
    this.showDatePicker = false;
    if(moment(this.today).isoWeekday() ==7) {
      this.showAlert("Sorry, on sundays the Mensa wasn't open!");
      return;
    }
    this.events.subscribe(Tags.MENSAOPEN_DAY, (open) => {
      this.events.unsubscribe(Tags.MENSAOPEN_DAY);
      if(open) {
        this.contDayChanged();
        this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODALERTDATECHOOSEN]);
      } else {
        let day = moment(this.today).format("dddd MMMM DD, YYYY");
        this.showAlert("Sorry, the Mensa wasn't open on <br />" + day);
      }
    });
    this.tt.isDayMensaOpen(moment(this.today));
  }
  contDayChanged() {
      this.events.subscribe(Tags.USERDATA_READY, (user) => {
        this.events.unsubscribe(Tags.USERDATA_READY);
        this.events.subscribe(Tags.MENUESLD_READY,(fL, sL, siL, fD, sD, siD) => {
          this.events.unsubscribe(Tags.MENUESLD_READY);
          Recommendation.recommendIt(fD, user);
          Recommendation.recommendIt(fL, user);
          Recommendation.recommendIt(sD, user);
          Recommendation.recommendIt(sL, user);
          Recommendation.recommendIt(siD, user);
          Recommendation.recommendIt(siL, user);
          let titel = moment(this.today).format("ddd MMM DD, YYYY")
          let profileModal;

          if(moment().isSame(this.today, "day")) {
            if( moment().isBefore(moment().set({'hour': 18, 'minute':45 }))) {
              profileModal = this.modalCtrl.create(ModalMenueLD, { fL: fL, sL: sL, siL: siL, fD: [], sD: [], siD:[], titel: titel });
              this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODMODULEOPEN]);
            
            } else {
              profileModal = this.modalCtrl.create(ModalMenueLD, { fL: fL, sL: sL, siL: siL, fD: fD, sD: sD, siD:siD, titel: titel });
              this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODMODULEOPEN]);
            }
          } else {
            profileModal = this.modalCtrl.create(ModalMenueLD, { fL: fL, sL: sL, siL: siL, fD: fD, sD: sD, siD:siD, titel: titel });
            this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODMODULEOPEN]);
          }
          profileModal.onDidDismiss(data => {
            console.log("onDismiss");
            console.log(data);
            if(data.menues.length > 0) {
              this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYADDFOODFOODADDED]);
              this.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY, (data) => {
                this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
                this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
                let msg = "Calories: " + data.kcal + 'g <br />' + 
                              "Fats: " + data.fats + 'g<br />' + 
                              "Saturated Fatty Acids: " + data.saturated_fatty_acids + "g" + '<br />' + 
                              "Carbs: " + data.carbs + 'g<br />' + 
                              "Sugar" + data.sugars + "g" + '<br />' + 
                              "Proteins: " + data.proteins + 'g<br />' + 
                              "Fiber: " + data.fiber +'g<br />';
                              "Salt: " + data.salt + "g";
                let alert = this.alertCtrl.create({
                  title: "Your History has been updated",
                  subTitle: "Here is an overview of your nutritions by now",
                  message: msg,
                  buttons: [{ text: 'Ok', role: 'cancel', handler:() => { this.buildEatenDishes();this.buildidealneeds(); } }]
                });
                alert.present();
              });
              this.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR, (err) => {
                this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
                this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
                let msg = err;
                let alert = this.alertCtrl.create({
                    title: "Error updating Your History",
                    subTitle: "",
                    message: msg,
                    buttons: [{ text: 'Ok', role: 'cancel'}]
                });
                alert.present();
              });
              let dishes = [];
              for(let menue of data.menues) {
                dishes.push(menue.name);
              }
              this.unibz.updateUserHistory(this.user, data.kind == 'lunch' ? 'L' : 'D', dishes, moment(this.today).format("YYYY-MM-DD"));
            } // data length == 0, nothing was selected. Info??
        });
        profileModal.present();
      });
      this.unibz.setToday(moment(this.today).format("YYYY-MM-DD"));
    });
    this.userData.prepData();
}

  public dayCancelled() {
//    console.log('day cacelled');
    this.showDatePicker = false;
  }


  // events
  public chartClicked(e:any):void {
    this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYCHARTCLICKED]);
  }

  public chartHovered(e:any):void {
//    console.log(e);
  }

  showInfoAlert(){
    let alert = this.alertCtrl.create({
      title: 'What did you eat exactly',
      subTitle: 'This chart shows what are the nutritions you have eaten by now. We can only show you the information, if you added all your mensa food to your history! Get more information about the Recommendation Algorithm at the Link in the Settings Page!',
      buttons: ['OK']
    });
    alert.present();
    this.sergelution.postUserAction(this.user, Tags.HISTORY[Tags.HISTORYSHOWINFO]);
  }

}

