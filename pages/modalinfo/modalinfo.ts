import { Component, } from '@angular/core';
import { Modal, NavController, NavParams, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-modalinfo',
  templateUrl: 'modalinfo.html',
})
export class Modalinfo {
    

  constructor(
    public navCtrl: NavController,
     public navParams: NavParams, 
     public viewCtrl: ViewController,

     ) {
      }
      
  ionViewDidLoad() {
    console.log('ionViewDidLoad Modalinfo');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
