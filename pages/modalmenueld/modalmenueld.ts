import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { MenueModel } from '../../models/menue-model';
import { Sergelution } from '../../providers/sergelution';
import { Data } from '../../providers/userdata';
import { MiscData }     from '../../providers/miscdata';
import { Datum } from '../../providers/datum';

@Component({
  selector: 'modalmenueld-page',
  templateUrl: 'modalmenueld.html'
})
 
export class ModalMenueLD {
  offset: number = 0;
  today: string;
  firstL:  MenueModel[] ;
  secondL:  MenueModel[] ;
  sideL:  MenueModel[] ;
  firstD:  MenueModel[] ;
  secondD:  MenueModel[] ;
  sideD:  MenueModel[] ;
  user: any = false;

  checkedMenues: MenueModel[] = [];
  showDinner: boolean = true;

  mealtime: string ="lunch";

  constructor(public viewCtrl: ViewController, 
              public params: NavParams,
              public sergelution: Sergelution,
              public userData: Data,
              public datum: Datum,
              public misc: MiscData
              ) {
                this.firstL = params.get("fL");
                this.secondL = params.get("sL");
                this.sideL = params.get("siL");
                this.firstD = params.get("fD");
                this.secondD = params.get("sD");
                this.sideD = params.get("siD");
                this.today = params.get("titel");
                console.log('modalmenuesd');
                console.log(this.firstD);
                console.log(this.firstD.length);
                this.showDinner = this.firstD.length == 0;
  } 

  cbChanged(event: any, menue: any) {
    if(event.checked) {
      this.checkedMenues.push(menue);
    } else {
      let index = this.checkedMenues.indexOf(menue);
      if(index > -1) {
        this.checkedMenues.splice(index, 1);
      }
    }
  }

setRecept(dat,slidingItem) {
  this.checkedMenues.push(dat);
  slidingItem.close();
}

  dismiss() {
      let data = { menues : this.checkedMenues, kind : this.mealtime };
      this.viewCtrl.dismiss(data);
  }
  
}
