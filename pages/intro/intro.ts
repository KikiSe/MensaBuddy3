// Intro is showing the introduction slides the first Time

import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { SettingsPage } from '../settings/settings';
//import { HomeChatPage } from '../pages/homechat/homechat';
//import { ItemSliding } from 'ionic-angular';
import { Tags } from '../../interfaces/tags';

import { UserModel } from '../../models/user-model';
import { Data } from '../../providers/userdata';
import { Sergelution } from '../../providers/sergelution';
import { Unibz } from '../../providers/unibz';
import { SwitchService }     from '../../services/switchservice';
import { MiscData } from '../../providers/miscdata';

import { AlertController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';


@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html'
})
export class IntroPage {
  swiper:any;
 @ViewChild('slider') slider: Slides;
  @ViewChild('introNameInput') introNameInput;
  @ViewChild('introHeightInput') introHeightInput;
  @ViewChild('introSnacksInput') introSnacksInput; 
  user: UserModel = new UserModel();
  isMale: boolean = false; 
  isFemale: boolean = false; 
  public activities: string[] = []; 
  public breakfasts: string[] = []; 
  
  constructor(
    public navCtrl: NavController,
    public events: Events,
    public sergelution: Sergelution,
    public unibz: Unibz,
    public userService: Data,
    public misc: MiscData,
    public switchService: SwitchService,
    public alertCtrl: AlertController,
    ) {
      console.log("intro.ts")
    
      this.events.subscribe(Tags.USERDATA_READY, (user) => {
        this.events.unsubscribe(Tags.USERDATA_READY);
        this.user = user;
        console.log('receiving subscribed Data');
        console.log(user);
        this.isMale = (user.gender == UserModel.GENDERMALE);
        this.isFemale = (user.gender == UserModel.GENDERFEMALE);
      });
      this.userService.prepData();
      for(let i = 0, j=UserModel.ACTIVITIES.length;i<j;i++) {
          this.activities.push(UserModel.ACTIVITIES[i]);
        } 
        for(let i = 0, j=UserModel.BREAKFASTS.length;i<j;i++) {
          this.breakfasts.push(UserModel.BREAKFASTS[i]);
        } 
  }

  ionViewDidEnter() {
  /*
    console.log('ionViewDidEnter IntroPage');
    this.events.subscribe(Tags.USERDATA_READY, (user) => {
      console.log('receiving subscribed Data');
        this.user = user;
        console.log(this.user);
    });
    */
  }
  ionViewDidLeave() {
//    console.log('ionViewDidLeave IntroPage');
//    this.events.unsubscribe(Tags.USERDATA_READY);
  }

  switch() {
      this.sergelution.postUserAction(this.user, Tags.INTRO[Tags.INTROTOCHAT]);
      this.misc.setLastTab(0);
      this.switchService.switchTo("chat");
  }

  slideChanged() {
    let cI = this.slider.getActiveIndex();
    this.events.subscribe(Tags.USERDATA_READY, (user) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      console.log('receiving subscribed Data');
      this.user = user;
      console.log(this.user);
    });
    this.sergelution.postUserAction(this.user, Tags.INTRO[cI]);
    console.log("Current index is", cI);
   
  }

  slideToNext(){
    
    if(this.swiper){
    this.swiper.unlockSwipes();
  }
  this.slider.slideNext();
  }


  goToHome(){
    console.log(this.user);
    if(this.userService.isAllSet(this.user)) {
      console.log("saveSettings" );
      console.log(event );
      this.events.subscribe(Unibz.UNIBZ_REGISTRATION, (data) => {
        this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION);
        this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION_ERR);
        console.log("UNIBZ_REGISTRATION" );
        console.log(data);
        this.events.subscribe(Tags.USERDATA_REG_READY, (data) => {
          this.events.unsubscribe(Tags.USERDATA_REG_READY);
          let alert = this.alertCtrl.create({
            title: "Profile settings",
            message: 'Your profile is successfully registered!',
            buttons: [{text: 'Ok',role: 'cancel',}]
          });
          alert.present();
          this.navCtrl.setRoot(TabsPage);
          return;          
        });
        this.user.logged = true;
        this.user.email = data.username;
        this.userService.registrationSave(this.user);
      });
      this.events.subscribe(Unibz.UNIBZ_REGISTRATION_ERR, (err) => {
        this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION);
        this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION_ERR);
        console.log("UNIBZ_REGISTRATION_ERR" );
        console.log(err);
        let alert = this.alertCtrl.create({
          title: "Profile settings error",
          message: err._body,
          buttons: [{text: 'Back',role: 'cancel',}]
        });
        alert.present();
      });
  //    this.user.email = "u" + this.user.id;
  //    this.user.password = this.user.email;
      this.unibz.doRegister(this.user);
      this.sergelution.postUserAction(this.user, Tags.INTRO[17]);
    } else {
      let alert = this.alertCtrl.create({
        title: "Profile settings",
        message: 'Please fullfill your settings!',
        buttons: [{text: 'Ok',role: 'cancel',}]
      });
      alert.present();
      this.navCtrl.setRoot(TabsPage);
    }
  }
  changeName() : void {
    this.sergelution.postUserAction(this.user, Tags.INTRO[9]);
    this.userService.save(this.user);
  }
  changeAge() : void {
    this.sergelution.postUserAction(this.user, Tags.INTRO[10]);
    this.userService.save(this.user);
  }
  changeGender(gender: number) : void {
    this.sergelution.postUserAction(this.user, Tags.INTRO[11]);
    this.isMale = (gender == UserModel.GENDERMALE);
    this.isFemale = (gender == UserModel.GENDERFEMALE);
    this.user.gender = gender;
    console.log("Saving gender: " + this.user.gender);
    this.userService.save(this.user);
  }
  changeSize() : void {
    this.sergelution.postUserAction(this.user, Tags.INTRO[12]);
    this.userService.save(this.user);
  }
  changeWeight() : void {
    this.sergelution.postUserAction(this.user, Tags.INTRO[13]);
    this.userService.save(this.user);
  }
  changeSnacks() : void {
    this.sergelution.postUserAction(this.user, Tags.INTRO[16]);
    this.userService.save(this.user);
  }



  changeModel() : void {
    this.userService.save(this.user);
  }

  chooseActivity() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Choose your Type of Activity');
    alert.addInput({type: 'radio', label: UserModel.ACTIVITIES[1], value: '1'});
    alert.addInput({type: 'radio', label: UserModel.ACTIVITIES[2], value: '2'});
    alert.addInput({type: 'radio', label: UserModel.ACTIVITIES[3], value: '3'});
    alert.addInput({type: 'radio', label: UserModel.ACTIVITIES[4], value: '4'});
    alert.addButton({
      text: 'Cancel',
      handler: () => {
        this.sergelution.postUserAction(this.user, Tags.INTRO[14] + 0);
      }
    });
    alert.addButton({
      text: 'Save',
      handler: data => {
        this.sergelution.postUserAction(this.user, Tags.INTRO[14] + 1);
        this.user.activity = data;
        this.changeModel();
      }
    });
     alert.present();
  }

  chooseBreakfast(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Choose your Type of Breakfast');
    alert.addInput({type: 'radio', label: UserModel.BREAKFASTS[1], value: '1'});
    alert.addInput({type: 'radio', label: UserModel.BREAKFASTS[2], value: '2'});
    alert.addInput({type: 'radio', label: UserModel.BREAKFASTS[3], value: '3'});
    alert.addButton({
      text: 'Cancel',
      handler: () => {
        this.sergelution.postUserAction(this.user, Tags.INTRO[15] + 0);
      }
    });
    alert.addButton({
      text: 'Save',
      handler: data => {
        this.sergelution.postUserAction(this.user, Tags.INTRO[15] + 1);
        this.user.breakfasttyp = data;
        this.changeModel();
      }
    });
     alert.present();
  }

}
