import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { MenueModel } from '../../models/menue-model';
import { Sergelution } from '../../providers/sergelution';
import { Data } from '../../providers/userdata';
import { MiscData }     from '../../providers/miscdata';
import { Datum } from '../../providers/datum';

@Component({
  selector: 'modalmenue-page',
  templateUrl: 'modalmenue.html'
})
 
export class ModalMenue {
  offset: number = 0;
  today: string;
  first:  MenueModel[] ;
  second:  MenueModel[] ;
  side:  MenueModel[] ;
  user: any = false;

  checkedMenues: MenueModel[] = [];

  mealtime: string ="lunch";

  constructor(public viewCtrl: ViewController, 
              public params: NavParams,
              public sergelution: Sergelution,
              public userData: Data,
              public datum: Datum,
              public misc: MiscData
              ) {
                this.first = params.get("prM");
                this.second = params.get("seM");
                this.side = params.get("siM");
                this.today = params.get("titel");
  } 

  cbChanged(event: any, menue: any) {
    if(event.checked) {
      this.checkedMenues.push(menue);
    } else {
      let index = this.checkedMenues.indexOf(menue);
      if(index > -1) {
        this.checkedMenues.splice(index, 1);
      }
    }
  }

  dismiss() {
      let data = this.checkedMenues;
      this.viewCtrl.dismiss(data);
  }
}
