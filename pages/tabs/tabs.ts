import { Component } from '@angular/core';

import { MiscData }     from '../../providers/miscdata';

import { HomeMenuPage } from '../homemenu/homemenu';
import { MenuePage } from '../menue/menue';
import { WebcamPage } from '../webcam/webcam';
import { SettingsPage } from '../settings/settings';
import { RecommendationPage } from '../recommendation/recommendation';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = MenuePage;
  tab2Root: any = HomeMenuPage;
  tab3Root: any = RecommendationPage;
  tab4Root: any = WebcamPage;
  tab5Root: any = SettingsPage;

  tabpos: number = 2;

  constructor(private miscData: MiscData) {
     this.tabpos = this.miscData.getLastTab();
  }

  ngOnDestroy() {
    console.log("tabs.ts onDestroy");
  }

  ionViewDidEnter() {
    console.log("Tabs: ionViewDidEnter tabpos: " + this.tabpos);
  }

}
