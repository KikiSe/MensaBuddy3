// Menu is showing the List with Menu of the day + selection

import { Component } from '@angular/core';

import { AlertController, ToastController } from 'ionic-angular';
import { NavController, Platform, Events} from 'ionic-angular';

import { SwitchService }     from '../../services/switchservice';

import { MenueModel } from '../../models/menue-model';
//import { UserModel } from '../../models/user-model';
import { Sergelution } from '../../providers/sergelution';
import { Unibz } from '../../providers/unibz';
import { Data } from '../../providers/userdata';
import { MiscData }     from '../../providers/miscdata';
import { Recommendation } from '../../providers/recommendation';
import { Datum } from '../../providers/datum';
import { Tags } from '../../interfaces/tags';

import * as moment from 'moment';

import { ItemSliding } from 'ionic-angular';

@Component({
  selector: 'page-menue',
  templateUrl: 'menue.html'
})
 
export class MenuePage {
  offset: number = 0;
  today: string;
  hostToday: string;
  firstL:  MenueModel[] ;
  firstD:  MenueModel[] ;
  secondL:  MenueModel[] ;
  secondD:  MenueModel[] ;
  sideL:  MenueModel[] ;
  sideD:  MenueModel[] ;
  user: any = false;
  hideDinner: boolean = false;
  inc: boolean = true;
  ioncard: boolean = false;
  mealtime: string ="lunch";

  constructor(public navCtrl: NavController, 
              public alertCtrl: AlertController,
              public sergelution: Sergelution,
              public unibz: Unibz,
              public userData: Data,
              public datum: Datum,
              public misc: MiscData,
              public platform: Platform,
              public events: Events,
              public switchService: SwitchService,
              private toastCtrl: ToastController
              ) { 
            }

  getMenueModels() {
    this.ioncard = false;
    if(this.userData) {
        Recommendation.recommendIt(this.firstD, this.user);
        Recommendation.recommendIt(this.firstL, this.user);
        Recommendation.recommendIt(this.secondD, this.user);
        Recommendation.recommendIt(this.secondL, this.user);
        Recommendation.recommendIt(this.sideD, this.user);
        Recommendation.recommendIt(this.sideL, this.user);
    }
  }

  ionViewDidLoad() {
    console.log("menue.ts ionViewDidLoad")
    this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = data;
      
    });
    this.userData.prepData();

    this.events.subscribe(Tags.MENUESLD_READY,(pL, sL, sideL, pD, sD, sideD) => {
      this.events.unsubscribe(Tags.MENUESLD_READY);
      this.events.unsubscribe(Unibz.UNIBZ_RECIPES_ERR);
      this.firstD = pD;
      this.firstL = pL;
      this.secondD = sD;
      this.secondL = sL;
      this.sideD = sideD;
      this.sideL = sideL;
      this.hideDinner = this.firstD.length == 0; 
      this.getMenueModels();
      this.buildDayString(); 
      this.ioncard = false;       
      if(pL.length == 0) {
        this.ioncard = true;       
      }
      console.log(this.ioncard);
    });
    this.events.subscribe(Unibz.UNIBZ_RECIPES_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_RECIPES_ERR);
      this.events.unsubscribe(Tags.MENUESLD_READY);
      let alert = this.alertCtrl.create({
        title: "Error getting recoms",
        message: err._body,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      this.buildDayString(); 
//      this.ioncard = false;       
//      if(pL.length == 0) {
//        this.ioncard = true;       
//      }
//      console.log(this.ioncard);
    });
    this.unibz.setToday(moment().format("YYYY-MM-DD"));
    
  }


  ionViewDidEnter() {
    this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = data;
      if(!this.user.logged) {
        console.log("ionenter unlogged")
      this.showToastWithCloseButtonSettings();
    }
      this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUEENTER]);
    });
    this.userData.prepData();
      
  }

  showToastWithCloseButtonSettings() {
    let toast = this.toastCtrl.create({
      message: "Did you already fulfill all of your settings? Let's go to your Profile!",
      showCloseButton: true,
      position: "top",
      closeButtonText: "OK",
    });
    toast.present();
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUESETTINGSTOAST]);
}

  ngOnDestroy() {
    console.log("menue: onDestroy");
    this.events.unsubscribe(Tags.USERDATA_READY);
  }

  switch() {
      this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUECHATSWITCH]);
      this.misc.setLastTab(this.navCtrl.parent.getSelected().index);
      this.switchService.switchTo("chat");
  }

  goToday() {
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUEGOTODAY]);
    console.log("goToday");
    this.offset = 0;
    this.inc=true;
    this.ioncard = false
    this.goTo();
  }
  goBack() {
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUEGOBACK]);
    console.log("goBack");
    this.offset--;
    this.ioncard = false
    this.inc=false;
    this.goTo();
  }
  goNext() {
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUEGONEXT]);
    console.log("goNext");
    this.offset++;
    this.inc=true;
    this.ioncard = false
    this.goTo();
  }

  goTo() {
    this.events.subscribe(Tags.MENUESLD_READY,(pL, sL, sideL, pD, sD, sideD) => {
      this.events.unsubscribe(Tags.MENUESLD_READY);
      this.events.unsubscribe(Unibz.UNIBZ_RECIPES_ERR);
      this.firstD = pD;
      this.firstL = pL;
      this.secondD = sD;
      this.secondL = sL;
      this.sideD = sideD;
      this.sideL = sideL;
      this.hideDinner = this.firstD.length == 0; 
      this.getMenueModels();
      this.buildDayString(); 
      this.ioncard = false;       
      if(pL.length == 0) {
        this.ioncard = true;       
      }
      console.log(this.ioncard);
    });
    this.events.subscribe(Unibz.UNIBZ_RECIPES_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_RECIPES_ERR);
      this.events.unsubscribe(Tags.MENUESLD_READY);
      let alert = this.alertCtrl.create({
        title: "Error getting daily menues",
        message: err._body,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      this.buildDayString(); 
      this.ioncard = true;       
    });
    let d = moment().format("YYYY-MM-DD");  
    if(this.offset != 0) {
      d = moment().add(this.offset, "d").format("YYYY-MM-DD");  
      console.log(d + " " + this.offset);
    }
    console.log(d + " " + this.offset);
    this.unibz.setToday(d);
    this.hostToday = d;
}

  buildDayString(): void {
    if(this.offset == 0) {
      this.today = "Today";
    } else if(this.offset == 1)  {
      this.today = "Tomorrow";
    } else if(this.offset == -1)  {
      this.today = "Yesterday";
    } else  {
      this.today = this.datum.getMenueDatePlus(this.offset);
      console.log(this.today);
    }
  }

  showRecept(dat: MenueModel, slidingItem: ItemSliding) {
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUESHOWRECEPT]);
    let submsg = "";
    if(this.offset >= 1){
      submsg = "Will you eat this?"
    }else{
      submsg = 'Did you eat this?'
    }
    if(this.user.logged){
    let prompt = this.alertCtrl.create({
      title: dat.name,
      subTitle: submsg,
      message: 'Portion grams: ' + dat.portion_grams + 'g'+ '<br />' + 'Calories: ' + dat.kcal+ 'kcal' + '<br />' +
                'Carbs: ' + dat.carbs + 'g'+ '<br />' + 'Fiber: ' + dat.fiber + 'g'+ '<br />' +
                'Sugars: ' + dat.sugars+ 'g' + '<br />' + 'Proteins: ' + dat.proteins+ 'g' + '<br />' +
                'Fats: ' + dat.fats + 'g'+ '<br />' + 'Salt: ' + dat.salt + 'g'+ '<br />',

      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUESHOWRECEPTCANCEL]);
          }
        },
        {
          text: "Add to History",
          handler: () => {
            this.addToHistory(dat,slidingItem);
            this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUESHOWRECEPTADD]);
          }
        },
        ]
    });
    prompt.present();
  }else{
    submsg = "Complete your profile to add it to your history."
    let prompt = this.alertCtrl.create({
      title: dat.name,
      subTitle: submsg,
      message: 'Portion grams: ' + dat.portion_grams + 'g'+ '<br />' + 'Calories: ' + dat.kcal+ 'kcal' + '<br />' +
                'Carbs: ' + dat.carbs + 'g'+ '<br />' + 'Fiber: ' + dat.fiber + 'g'+ '<br />' +
                'Sugars: ' + dat.sugars+ 'g' + '<br />' + 'Proteins: ' + dat.proteins+ 'g' + '<br />' +
                'Fats: ' + dat.fats + 'g'+ '<br />' + 'Salt: ' + dat.salt + 'g'+ '<br />',

      buttons: [
        {
          text: 'Back',
          role: 'cancel',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUESHOWRECEPTCANCEL]+0);
          }
        }
        ]
    });
    prompt.present();
  }
  }

 inSwipe: boolean;
  swipeIt(event) {
    console.log("swipeIt");
    console.log(event);
    /*if(this.inSwipe) return;
    this.inSwipe = true;
    this.events.subscribe(Tags.MENUESL_READY,(dL, sL, sideL) => {
      this.events.unsubscribe(Tags.MENUESL_READY);
      this.getMenueModels();
      this.buildDayString();        
      this.inSwipe = false;
    });*/
    if(event.direction == 2) {
      this.goNext();
    } else if(event.direction == 4) {
      this.goBack();
    }
  }

  addToHistory(dat: MenueModel, slidingItem: ItemSliding) {
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUEADDHISTORY]);
    this.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
      let msg = "Calories: " + data.kcal + 'kcal'+'<br />' + 
                              "Fats: " + data.fats + '<br />' + 
//                              " containing " + data.saturated_fatty_acids + " saturated_fatty_acids" + '<br />' + 
                              "Carbs: " + data.carbs + '<br />' + 
//                              " containig " + data.sugars + " sugars" + '<br />' + 
                              "Proteins: " + data.proteins + '<br />' + 
                              "Fiber: " + data.fiber +'<br />';
                            //  "salt: " + data.salt;
      let alert = this.alertCtrl.create({
        title: "Your History has been updated",
        subTitle: "Here is an overview of your nutritions for " + moment(data.date,"YYYY_MM_DD").format("dddd MMM DD") ,
        message: msg,
        buttons: [{ text: 'Ok', role: 'cancel'}]
      });
      alert.present();
    });
    this.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
      let msg = err;
      let alert = this.alertCtrl.create({
        title: "Error updating Your History",
        subTitle: "",
        message: msg,
        buttons: [{ text: 'Ok', role: 'cancel'}]
      });
      alert.present();
    });
    let kind = "L";
    if(!(this.mealtime == "lunch")) {
      kind = "D";
    }
    let dishes = [];
    dishes.push(dat.name);
    this.unibz.updateUserHistory(this.user, kind, dishes, this.hostToday);
//    this.userData.addToHist(dat);
    slidingItem.close();
  }

  show(index:number) {
    console.log("show: " + index);
    this.ioncard = false;
    this.sergelution.postUserAction(this.user, Tags.MENUE[Tags.MENUESWAPLD]+index);
  }


}
