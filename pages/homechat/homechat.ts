import { Component, OnDestroy, ViewChild } from '@angular/core';

import { NavController, ModalController, AlertController, Events } from 'ionic-angular';

import { SwitchService }     from '../../services/switchservice';

import { Chat } from '../../providers/chat';
import { ChatModel } from '../../models/chat-model';
import { UserModel } from '../../models/user-model';

import { ActionSheetController } from 'ionic-angular'

//import { ModalInput } from '../../pages/modalinput/modalinput';

import { Recommendation } from '../../providers/recommendation';
import { Data } from '../../providers/userdata';
import { TimeTicker } from '../../providers/akttime';
import { MiscData } from '../../providers/miscdata';
import { Sergelution } from '../../providers/sergelution';
import { Unibz } from '../../providers/unibz';
//import { FlurryService } from '../../providers/flurryservice';
import { Datum } from '../../providers/datum';

import { Tags } from '../../interfaces/tags';
import { Runner } from '../../interfaces/runner';
import { Ate2Runner } from '../../runners/ate2-runner';
import { MenueRunner } from '../../runners/menue-runner';
import { WebcamRunner } from '../../runners/webcam-runner';
import { RecommenderRunner } from '../../runners/recommender-runner';    
import { IntroRunner } from '../../runners/intro-runner';         
import { SettingsRunner } from '../../runners/settings-runner';         

import * as moment from 'moment';

@Component({
  selector: 'homechat-page',
  templateUrl: 'homechat.html',
  providers: [Chat, Ate2Runner, MenueRunner, WebcamRunner, RecommenderRunner, IntroRunner, SettingsRunner, Unibz],
  
})
export class HomeChatPage implements OnDestroy {
  @ViewChild('content') content;
  @ViewChild('datePicker') datePicker;
  @ViewChild('threePicker') threePicker;
  @ViewChild('lineCanvas') lineCanvas;
  @ViewChild('inputId') inputId;

  user: UserModel = new UserModel();
 leaving: boolean = false;

  runner: Runner;
  chatTitle = "Chat Home";
  chats: ChatModel[] = [];
  idNumber: number = 1;
  again: string;  
  kcalCount: number = 0;
  lunchWas: boolean = false;
  dinnerWas: boolean = false;

  theDay: any;    //thats really today
  today: any;     // thats the selected day from DatePicker
  minday: string;
  maxday: string;
  chatKind: string;
  mealtime: string ="lunch";

  public fits0: any;
  public fits1: any;
  public fits2: any;
  public recoms: any;

  public  idealcarbs: number = 2000;
  public  idealfiber: number = 1000;
  public  idealcalories: number = 1500;
  public  idealproteins: number = 1000;
  public  idealfats: number = 1000;
  public  idealsugar: number = 10;
  public  idealsfa: number = 10;
  public  idealsalt: number = 10;
  public  carbs: number = 0;
  public  fiber: number = 0;
  public  proteins: number = 0;
  public  fats: number = 0;
  public  calories: number = 0;
  public  salt: number = 0;
  public  sfa: number = 0;
  public  sugar: number = 0;

  public todayCarb:number = 0;
  public todayFiber:number = 0;
  public todayProteins: number = 0;
  public todayFats: number = 0;
  public todayCalories: number = 0;

  public  history: any;
  public  weeklyHistory: any;
  public  needs: any;
  public  bcDataset: any = [{data: [0,0,0,0,0,], label: ""}]; //,{data: [0,0,0,0,0],label: ""} ];
  public  bcDatasetweek: any = [{data: [0,0,0,0,0], label: ""}]; //,{data: [0,0,0,0,0],label: ""} ];
  public  bcLabels: any = ["kCal", "Carbs", "Fiber", "Proteins", "Fats"];
  public  bcColors:Array<any> = [
    { // grey
      backgroundColor: [ 'rgba(255, 23, 68, 0.7)' /*'rgba(255, 255, 0, 0.7)'*/, 'rgba(0, 203, 118, 0.7)', 'rgba(101, 31, 255, 0.7)', 'rgba(0, 229, 255, 0.7)' ],     
      borderColor: [ 'rgba(255, 23, 68, 1)', /*'rgba(255, 255, 0, 1)',*/ 'rgba(0, 203, 118, 1)', 'rgba(101, 31, 255, 1)', 'rgba(0, 229, 255, 1)' ],
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: ['rgba(255, 23, 68, 0.5)', /*'rgba(255, 255, 0, 0.5)',*/'rgba(0, 203, 118, 0.5)','rgba(101, 31, 255, 0.5)', 'rgba(0, 229, 255, 0.5)' ],
      borderColor: [          'rgba(255, 23, 68, 0.6)',/*'rgba(255, 255, 0, 0.6)',*/'rgba(0, 203, 118, 0.6)','rgba(101, 31, 255, 0.6)', 'rgba(0, 229, 255, 0.6)' ],
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];

  pickKind: string = 'threeNumbers';
  pickVal: any = "25 75 175";
  settingsColumns: any[] = [
        {
          name: 'age',
          options: []
        }, {
          name: 'weight',
          options: []
        }, {
          name: 'height',
          options: []
        }
      ];

  constructor(
    public navCtrl: NavController, 
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public switchService: SwitchService,
    public chat: Chat,
    public misc: MiscData,
    public tt: TimeTicker,
    public sergelution: Sergelution,
    public unibz: Unibz,
 //   public flurryService: FlurryService,
    public datum: Datum,
    public events: Events,
    public userService: Data,

    public ate2Runner: Ate2Runner,
    public menueRunner: MenueRunner,
    public webcamRunner: WebcamRunner,
    public recommenderRunner: RecommenderRunner,
    public introRunner: IntroRunner,
    public settingsRunner: SettingsRunner,

    
    ) {
    this.leaving = false;
    this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = data;
      console.log("homechat.ts ionViewDidLoad")
      console.log("homechat: user.id=" + this.user.id);
      if(!this.user.logged) {
          console.log("homechat.ts not logged + " + this.user.logged);
          this.startChatting();
      } else {
        this.buildidealneeds();
      //  this.goOnChatting();
        this.fillPickerWithBreakfast();
        console.log("didload is logged")
      }
    });
    this.userService.prepData();
    
    this.again = "";
    this.theDay = moment().format();
    this.today = moment().format();
    console.log("HomeChatPage");
    console.log(this.today);
    console.log(this.datePicker);
    this.minday = moment(this.today).format("YYYY");
    this.maxday = moment(this.today).format("YYYY-MM-DD");
    console.log(this.maxday);
  }

  ngOnDestroy() {
    console.log("homechat: onDestroy");
    this.events.unsubscribe(Tags.USERDATA_READY);
    this.misc.setLastRunner(this.runner);
  }

  ionViewDidLoad() {
    /*this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = data;
      console.log("homechat.ts ionViewDidLoad")
      console.log("homechat: user.id=" + this.user.id);
      if(!this.user.logged) {
          console.log("homechat.ts not logged + " + this.user.logged);
          this.startChatting();

    } else {
      this.buildidealneeds();
      this.goOnChatting();
      console.log("didload is logged")
    
    }
    });
    this.userService.prepData();*/
  //  this.user = this.userService.getData();
   
  }

  ionViewDidEnter() {
    this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = data;
      if(this.user.logged){
        console.log("onenter in homechat");
        this.goOnChatting();
      }
    });
    this.userService.prepData();
  }

  buildidealneeds() {
    this.events.subscribe(Unibz.UNIBZ_DAILYNEEDS, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS);
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS_ERR);
      console.log("buildIdealNeeds");
      console.log(data);
      this.needs = data;
      this.idealcalories = this.needs.calories;
      this.idealcarbs = this.needs.max_carbs;
      this.idealfats = this.needs.max_fats;
      this.idealfiber = this.needs.max_fiber;
      this.idealproteins = this.needs.proteins_PRI;
 //     this.idealsugar = this.needs.sugar;
 //     this.idealsfa = this.needs.saturated_fetty_acids;
 //     this.idealsalt = this.needs.salt;
      this.buildhistoryToday();
      this.buildhistoryWeekly();
    });
    this.events.subscribe(Unibz.UNIBZ_DAILYNEEDS_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS);
      this.events.unsubscribe(Unibz.UNIBZ_DAILYNEEDS_ERR);
      let alert = this.alertCtrl.create({
        title: "Missing DailyNeeds",
        message: "Sorry.. something went wrong",
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      return;
    });
    this.unibz.getDailyNeeds(this.user);
  }

  buildhistoryToday(){
    console.log("buildhistoryToday: ");
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY,(data) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR);
      this.history = data;
      this.carbs = this.history.carbs;
      this.fiber = this.history.fiber;
      this.proteins = this.history.proteins;
      this.fats = this.history.fats;
      this.calories = this.history.kcal;
//      this.sugar = this.needs.sugar;
//      this.sfa = this.needs.saturated_fetty_acids;
//      this.salt = this.needs.salt;
      console.log("history: " + this.fats);
      console.log(data);
      
    });
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR);
      let alert = this.alertCtrl.create({
        title: "Missing History",
        message: "Add some Dishes to your History.",
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      return;
    });
    this.unibz.getNutritionalHistory(this.user, moment().format("YYYY-MM-DD"));
  }

  buildhistoryWeekly(){
    console.log("buildhistoryWeekly: ");
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY,(data) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR);
      console.log(data);
      this.weeklyHistory = data;
   });
    this.events.subscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR,(err) => {
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY);
      this.events.unsubscribe(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR);
      console.log(err);
      let alert = this.alertCtrl.create({
          title: "Host Error",
          message: err,
          buttons: [{ text: 'Ok', role: 'cancel', handler: ( (data) => { alert.dismiss(); return;}) }]
        });
       alert.present();
   });
   this.unibz.getNutritionalHistoryWeek(this.user);
  }

  startChatting(){   
    this.runner = this.introRunner;
    this.runner.init(this, this.chat);
    this.chat.addFirstChat(this.chat.bot("..."));
    this.getAllChats(false,false);
    this.chat.inChat = false;
    setTimeout(() => {
      this.chat.aktStep = "intro handler";
      console.log("startChatting: " + this.user.name );
      if(this.user.name == "") {
        this.chat.addFirstChat(this.chat.bot("Hi, my name is Buddy! I'm gonna chat with you to give you recommendations, show the menu or check your history!"  ));
        this.chat.addChat(this.chat.bot("To give you this information, we need to register a new profile."));
        this.chat.addChat(this.chat.bot("So, let me know who you are."));
        this.chat.setInputField(true, "", "Enter your name","", true, "Ok");
        console.log(this.inputId);
        this.runner.setStates("",9,"Yes!",9);
        this.getAllChats(false,false);
        setTimeout(() => {
          this.inputId.setFocus();
        }, Chat.veryShortWait);
      } else {
        this.chat.addFirstChat(this.chat.bot("Hi " + this.user.name  ));
        this.chat.addChat(this.chat.bot("This is your name right?"));
        this.runner.setStates("Yes!",0,"No",1);
        this.getAllChats(false,true);
      }
    }, Chat.longWait);
  }

  goOnChatting(){
    this.runner = this.misc.getLastRunner();
    console.log("homechat: goOnChatting...");
    console.log(this.runner);
    console.log(this.content);
    if(this.runner) {
      this.sergelution.postUserAction(this.user, Tags.HOMECHAT[8]);
      this.runner.init(this, this.chat);
    } else {
      this.openMenu();
      return;
    }
    this.chats = this.chat.getChatsPur();
    setTimeout(() => {
      if(this.leaving) return;
      this.content.scrollToBottom(); 
    }, 300);
  }

  onChange(val) {
    console.log("onChange");
    console.log(val);
  }


  fillPicker() {
    this.pickKind = 'threeNumbers';
    this.pickVal = "25 75 175";
    this.settingsColumns = [
        {
          name: 'age',
          options: []
        }, {
          name: 'weight',
          options: []
        }, {
          name: 'height',
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 8;i<116;i++) {
      let tok = '' + i;
      opts.push({text: tok, value: tok});
    }
    this.settingsColumns[0].options = opts;

    opts = [];
    for(let i = 30;i<180;i++) {
      let tok = '' + i;
      opts.push({text: tok, value: tok});
    }
    this.settingsColumns[1].options = opts;

    opts = [];
    for(let i = 90;i<220;i++) {
      let tok = '' + i;
      opts.push({text: tok, value: tok});
    }
    this.settingsColumns[2].options = opts;
  }

  fillPickerWithActivity() {
    this.pickKind = UserModel.ACTIVITY;
    this.pickVal = this.user.activity;
    this.settingsColumns = [
        {
          name: UserModel.ACTIVITY,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 1,j=UserModel.ACTIVITIES.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.ACTIVITIES[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }
  fillPickerWithBreakfast() {
    this.pickKind = UserModel.BREAKFASTTYP;
    this.pickVal = this.user.breakfasttyp;
    this.settingsColumns = [
        {
          name: UserModel.BREAKFASTTYP,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 1,j=UserModel.BREAKFASTS.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.BREAKFASTS[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }

  fillPickerWithMealTyp() {
    this.pickKind = UserModel.MEALTYP;
    this.pickVal = this.user.mealtyp;
    this.settingsColumns = [
        {
          name: UserModel.MEALTYP,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 1,j=UserModel.MEAL.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.MEAL[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }

 fillPickerWithMENULUNCHTyp() {
    this.pickKind = UserModel.MENUTYPL;
    this.pickVal = this.user.menutypL;
    this.settingsColumns = [
        {
          name: UserModel.MENUTYPL,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 1,j=UserModel.MENUL.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.MENUL[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }

  fillPickerWithMENUDINNERTyp() {
    this.pickKind = UserModel.MENUTYPD;
    this.pickVal = this.user.menutypD;
    this.settingsColumns = [
        {
          name: UserModel.MENUTYPD,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 1,j=UserModel.MENUD.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.MENUD[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }

  fillPickerWithLunchTime(){
    this.pickKind = UserModel.MENSATIMETYPLD;
 //   this.pickVal = this.user.mensatimetypL;
    this.settingsColumns = [
        {
          name: UserModel.MENSATIMETYPLD,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = 0,j=UserModel.MENSATIMELD.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.MENSATIMELD[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }
  fillPickerWithDinnerTime(){
    this.pickKind = UserModel.MENSATIMETYPLD;
 //   this.pickVal = this.user.mensatimetypL;
    this.settingsColumns = [
        {
          name: UserModel.MENSATIMETYPLD,
          options: []
        }
      ];
    let opts: any[] = [];
    for(let i = UserModel.MENSATIMEDSTART,j=UserModel.MENSATIMELD.length;i<j;i++) {
      let tok = '' + i;
      opts.push({text: UserModel.MENSATIMELD[i], value: tok});
    }
    this.settingsColumns[0].options = opts;
  }

  pickerChanged() {
    this.chat.set3Picker(false);
    console.log("pickerChanged");
    console.log(this.pickVal);
    console.log(this.pickKind);
    if(this.pickKind == UserModel.BREAKFASTTYP) {
      this.user.breakfasttyp = this.pickVal;
    } else if (this.pickKind == UserModel.ACTIVITY) {
      this.user.activity = this.pickVal;
    } else if (this.pickKind == UserModel.MEALTYP){
      this.user.mealtyp = this.pickVal;
    } else if (this.pickKind == UserModel.MENUTYPD){
      this.user.menutypD = this.pickVal;
    } else if (this.pickKind == UserModel.MENUTYPL){
      this.user.menutypL = this.pickVal;
    } else if (this.pickKind == UserModel.MENSATIMETYPLD){
      this.runner.setYesButton(UserModel.MENSATIMELD[this.pickVal]);
      this.events.subscribe(Tags.IGONOW_READY, (akt) => {
        this.events.unsubscribe(Tags.IGONOW_READY);
        if(this.tt.isLunchTime(akt)) {
          console.log("save iGoLunch");
          this.user.iGoLunch = akt.format();
          this.userService.save(this.user);
        
          this.runner.cont0();
        } else if(this.tt.isDinnerTime(akt)) {
          console.log("save iGoDinner");
          this.user.iGoDinner = akt.format();
          this.userService.save(this.user);
         
          this.runner.cont0();
        }
      });
      let aktMoment = moment().set('hour', this.misc.hourMinutes[this.pickVal].hour).set('minute', this.misc.hourMinutes[this.pickVal].minute);
      this.sergelution.postAccessDate(this.user.id, aktMoment);
      return;
    }
    this.events.subscribe(Tags.USERDATA_READY, (data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      this.user = data;      
      this.runner.cont0();
    });
    this.userService.save(this.user)
  }

  pickerCancelled() {
    this.chat.set3Picker(false);
    console.log("pickerCancelled");
    console.log(this.pickVal);
    this.runner.cont1();
  }

// Besser wäre es, vor dem DatePicker-Aufruf die states zu setzen und dann bei cancelled cont1 und bei changed cont0 aufzurufen.
// Dann wäre die Zuordnun
  // If no depending on the chatKind should occur, every runner must imlement noRunState2 as a reaction to dayChanged!
  dayChanged() {
    this.chat.setDatePicker(false);
    this.runner.cont0();
  }
  // same to this: noRUnState3 as a reaction to dayCancelled!
  dayCancelled() {
    this.chat.setDatePicker(false);
    this.runner.cont1();
  }

  timeChanged() {
    this.chat.setTimePicker(false);
    this.chat.yesRunState = 10;
    this.runner.cont1();
  }
  // same to this: noRUnState3 as a reaction to dayCancelled!
  timeCancelled() {
    this.chat.setTimePicker(false);
    this.chat.noRunState = 6;
    this.runner.cont1();
  }

  switch() {
    this.leaving = true;
    this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATSWITCH]);
    this.misc.setLastChat(this.chat);
    this.switchService.switchTo('menu');
  }

  openMenu(){

    let actionSheet = this.actionSheetCtrl.create(
      {
      title: 'Choose a chat',
      cssClass: 'action-sheets-basic-page',
      buttons: [
       {
          text: 'Show Menu',
          icon: 'clipboard',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATMENUE]);
            this.chatTitle = "Show menu chat"
            this.chat.inChat = false;
            this.theDay = moment().format();
            this.today = moment().format();
            this.minday = moment(this.today).format("YYYY-MM-DD");
            this.maxday = "2017-12-31";
            this.chatKind = 'menue';

            this.runner = this.menueRunner;
            this.runner.init(this, this.chat);
            this.chat.addFirstChat(this.chat.bot("..."));
            this.getAllChats(false,false);
            setTimeout(() => { 
              this.chat.aktStep = "show menu handler";
              this.chat.addFirstChat(this.chat.bot("Hi I'm Buddy! We can chat and i can help you!"));
              this.chat.addChat(this.chat.bot("Yeah let's check the menu!"));
              this.runner.setStates("Yes",0,"No",0);
      
              this.getAllChats(false, true);
            }, Chat.longWait);
          }
        }, {
          text: 'Show/Add History',
          icon: 'pizza',
          handler: () => {
            
            this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATIATE]);
            this.chat.aktStep = "";
            this.chatTitle = "Show or Add History"
            this.chat.inChat = false;
            this.today = new Date().toISOString();
            let aktMoment = moment(this.today);
            let weekday = aktMoment.isoWeekday();
            console.log(aktMoment.format("HH:mm") + " #" + weekday);
            console.log( (aktMoment.format("HH:mm") < '11:45'));
            if(aktMoment.isBefore(moment().set({'hour':11, 'minute': 45}))) {
              aktMoment.subtract(1, 'days');
            }
            this.maxday = aktMoment.format("YYYY-MM-DD");
            this.today = aktMoment.format();
            this.minday = "2017";
            this.chatKind = 'ate2';
            this.kcalCount = 0;
            this.lunchWas = false;
            this.dinnerWas = false;

            this.runner = this.ate2Runner;
            this.runner.init(this, this.chat);
            this.chat.addFirstChat(this.chat.bot("..."));
            this.getAllChats(false,false);
            setTimeout(() => {
              this.chat.aktStep = "I ate handler";
              this.chat.addFirstChat(this.chat.bot("Hi I'm Buddy! We can chat and i can help you!"));
              this.chat.addChat(this.chat.bot("Oh you already ate something?"));
              this.runner.setStates("Yep",0,"Show History",2);
              console.log("after runnersetStates");
              console.log(this.chat.yesButton + " / " + this.chat.noButton);
              this.getAllChats(false,true);
            }, Chat.longWait);
          }

        },{
          text: 'Show Recommendations',
          icon: 'star',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATRECOMMENT]);
            this.chatTitle = "Show recommendation.."
            this.chat.inChat = false;
            this.theDay = moment().format();
            this.today = moment().format();
            this.minday = moment(this.today).format("YYYY-MM-DD");
            this.maxday = "2017-12-31";
            this.chatKind = 'recommender';

            this.runner = this.recommenderRunner;
            this.runner.init(this, this.chat);
            this.chat.addFirstChat(this.chat.bot("..."));
            this.getAllChats(false,false);
            setTimeout(() => { 
              this.events.subscribe(Tags.USERDATA_READY,(user) => {
                this.events.unsubscribe(Tags.USERDATA_READY);
                this.user = user;
                console.log(user);
                if(this.user.logged){
                  this.chat.aktStep = "recommenderRunner";
                  this.chat.addFirstChat(this.chat.bot("Hi I'm Buddy! We can chat and i can help you!"));
                  this.chat.addChat(this.chat.bot("I can give you recommendations for your mensa menu!?"));
                  this.runner.setStates("Great!",1,"No thanks",0);
      
                  this.getAllChats(false, true);
                }else{
                  this.chat.aktStep = "show settings handler";
                  this.chat.addFirstChat(this.chat.bot("Hi I'm Buddy! Before giving you recommendations, please complete your Profile!"));
                  this.runner.setStates("Let's go",0,"Not really",0);
                  this.getAllChats(false, true);
                }
              
            });
            this.userService.prepData();
            }, Chat.longWait);
          }
        },{
          text: 'Show Waiting Line',
          icon: 'camera',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATWEBCAM]);
            console.log('waiting line clicked');
            this.chatTitle = "Show waiting line.."
            this.chat.inChat = false;
            this.theDay = moment().format();
            this.today = moment().format();
            this.minday = moment(this.today).format("YYYY-MM-DD");
            this.maxday = "2017-12-31";
            this.chatKind = 'webcam';

            this.runner = this.webcamRunner;
            this.runner.init(this, this.chat);
            this.chat.addFirstChat(this.chat.bot("..."));
            this.getAllChats(false,false);
            setTimeout(() => { 
              this.chat.aktStep = "show outlook handler";
              this.chat.addFirstChat(this.chat.bot("Hi I'm Buddy! We can chat and i can help you!"));
              this.chat.addChat(this.chat.bot("So what do you want to do?"));
              this.runner.setStates("Check-In",0,"See predicitions",1);
              this.getAllChats(false, true);
            }, Chat.longWait);
          }
        },{
          text: 'Show Profile',
          icon: 'settings',
          handler: () => {
            this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATSETTINGS]);
            this.chatTitle = "Show Profile.."
            this.chat.inChat = false;
            this.theDay = moment().format();
            this.today = moment().format();
            this.minday = moment(this.today).format("YYYY-MM-DD");
            this.maxday = "2017-12-31";
            this.chatKind = 'settings';

            this.runner = this.settingsRunner;
            this.runner.init(this, this.chat);
            this.chat.addFirstChat(this.chat.bot("..."));
            this.getAllChats(false,false);
            this.events.subscribe(Tags.USERDATA_READY,(data) => {
              this.events.unsubscribe(Tags.USERDATA_READY);
              this.user = data;
            });
            this.userService.prepData();
            setTimeout(() => { 
              this.chat.aktStep = "show settings handler";
              this.chat.addFirstChat(this.chat.bot("Hi I'm Buddy! We can chat and i can help you!"));
              this.chat.addChat(this.chat.bot("Ah okay so you want to change some settings?"));
              this.runner.setStates("Let's go",0,"Not really",0);
              this.getAllChats(false, true);
            }, Chat.shortWait);
          }
        }
      ]
    });
    this.sergelution.postUserAction(this.user, Tags.HOMECHAT[Tags.HOMECHATCHATS]);
    actionSheet.present();
  }



  //dummy  
  setTabPos(pos:number):void {
  }

 
  cont0() {
    if(this.chat.yesRunState > -1) {
      this.runner.cont0();
    }
    return;
  }

  cont1() {
    if(this.chat.noRunState > -1) {
      this.runner.cont1();
    }
  }

  getAllChats(three: boolean, yn: boolean) {
    this.chats = this.chat.getChats(three, yn);
//    console.log("homechat: num chats: " + this.chats.length);
//    console.log(this.chats[this.chats.length-1]);
    setTimeout(() => {
      if(this.leaving) return;
      this.content.scrollToBottom();
    }, 200);    
  }


  buildBarChartLabel():void{
    this.bcLabels = [ "kCals", "Carbs", "Fiber", "Proteins", "Fats"];
    return this.bcLabels;
  }

  buildBarChartDataToday(): void {
     let data: number[] = [];

     console.log("buildBarChartDataToday");
     console.log(this.idealcalories);
     console.log(this.calories);
     console.log((100/this.idealcalories)*this.calories);

    data.push((100/this.idealcalories)*this.calories);
    data.push((100/this.idealcarbs)*this.carbs);
    data.push((100/this.idealfiber)*this.fiber);
    data.push((100/this.idealproteins)*this.proteins);
    data.push((100/this.idealfats)*this.fats);
//    data.push((100/this.idealsugar)*this.sugar);
 //   data.push((100/this.idealsfa)*this.sfa);
 //   data.push((100/this.idealsalt)*this.salt);

    this.bcDataset = [{data: data, label: 'nutrition %'}]; //,{data: idealdata,label: 'ingredients ideal' }];
    console.log(this.bcDataset);
    return this.bcDataset;
  }
  
  buildWeeklyBarChartData(): void {
    let wCarb = 0;
    let wFiber = 0;
    let wProteins = 0;
    let wFats = 0;
    let wKcals = 0;
 //   let wSugar = 0;
 //   let wSFA = 0;
 //   let wSalt = 0;
    
    let idealcarbsweekly: number = this.idealcarbs*7;
    let idealfiberweekly: number = this.idealfiber*7;
    let idealproteinsweekly: number = this.idealproteins*7;
    let idealfatsweekly: number = this.idealfats*7;
    let idealcaloriesweekly: number = this.idealcalories*7;
 //   let idealsugarweekly: number = this.idealsugar*7;
 //   let idealsfasweekly: number = this.idealsfa*7;
  //  let idealsaltweekly: number = this.idealsalt*7;

    let data: number[] = [];
    if(this.weeklyHistory) {
      this.weeklyHistory.forEach((day)=> {
        wCarb += day.carbs;
        wFiber += day.fiber;
        wProteins += day.proteins;
        wFats += day.fats;
        wKcals += day.kcal;
  //    wSugar += day.sugar;
  //    wSFA += day.saturated_fetty_acids;
  //    wSalt += day.salt;
        console.log(day);
      });
    }
    console.log("after forEach" );
    wCarb += this.todayCarb;
    wFiber += this.todayFiber;
    wProteins += this.todayProteins;
    wFats += this.todayFats;
    wKcals += this.todayCalories;

    data.push((100/idealcaloriesweekly)*wKcals);
    data.push((100/idealcarbsweekly)*wCarb);
    data.push((100/idealfiberweekly)*wFiber);
    data.push((100/idealproteinsweekly)*wProteins);
    data.push((100/idealfatsweekly)*wFats);;
 //   data.push(wSugar);;
 //   data.push(wSFA);;
 //   data.push(wSalt);;
    this.bcDatasetweek = [{data: data, label: 'nutrition %'}]; //,{data: idealdata,label: 'ingredients ideal' }];
    return this.bcDatasetweek;
  }



  buildLineChartLabel():Array<any> {
    let days: string[] = [];
    let aktMoment = moment(new Date().toISOString());
    aktMoment.add(-7, 'days');
    for(let i=-6;i<1;i++) {
      aktMoment.add(1, 'days');
      if(aktMoment.isoWeekday() != 7) {
        days.push(aktMoment.format('ddd'));
      }
    }
    return days;
  }

  buildLineChartData():Array<any> {
    let menues: any[] = this.userService.getHist();
    console.log(menues);
    let kcal: number[] = [];
    let aktMoment = moment(new Date().toISOString());
    aktMoment.add(-7, 'days');
    for(let i=-6;i<1;i++) {
      aktMoment.add(1, 'days');
      if(aktMoment.isoWeekday() != 7) {
        let inter = 0;
        let datum = aktMoment.format("DD.MM.YYYY");
        console.log(datum);
        menues.forEach(menu => {
          if(datum == menu.datum) {
            inter += menu.kcal;
          }
        })
        kcal.push(inter);
      }
    }
    console.log([{data: kcal, label: 'gram calories'}])
    return( [{data: kcal, label: 'gram calories'}]);
  }
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

 buildWaitingLineChartOptions(mode):any {
    switch(mode) {
      case 1:
        return  { 
                   legend: {labels:{fontColor:"white", fontSize: 18}},
                   scales: {
                      yAxes: [{
                        ticks: {
                           fontColor: "white",
                           fontSize: 18,
                           stepSize: 1,
                           beginAtZero:true
                        }
                      }],
                      xAxes: [{
                        ticks: {
                           fontColor: "white",
                           fontSize: 14,
                           stepSize: 1,
                           beginAtZero:true
                         }
                      }]
                    }
                  };
      case 2:
        return 
          {    };
    }
 }



 buildWaitingLineChartColors(mode):Array<any> {
    switch(mode) {
      case 1:
        return  [
    { // white
      backgroundColor: '#fff',
      borderColor: '#fff',
      pointBackgroundColor: '#fff',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    },
    { // white
      backgroundColor: '#fff',
      borderColor: '#fff',
      pointBackgroundColor: '#fff',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff'
    }
    
    ];
      case 2:
        return [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }];
    }
 }
 buildWaitingLineChartLabel(mode):Array<any> {
    switch(mode) {
      case 1:
        return  ['12:00','12:15','12:30','12:45','13:00','13:15','13:30','13:45','14:00'];
      case 2:
        return ['18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30']; 
    }
  }
  getTimeLine(mode:number) {
    switch(mode) {
      case 1:
        return [{h:11,m:45},{h:12,m:0},{h:12,m:15},{h:12,m:30},{h:12,m:45},{h:13,m:0},{h:13,m:15},{h:13,m:30},{h:13,m:45},{h:14,m:0}];     
      case 2:
        return [{h:18,m:30},{h:18,m:45},{h:19,m:0},{h:19,m:15},{h:19,m:30},{h:19,m:45},{h:20,m:0},{h:20,m:15},{h:20,m:30}]; 
    }
  }  
  buildWaitingLineChartData(visits: any[], mode: number ):Array<any> {
    let inter = this.getTimeLine(mode);
    let start = 0; // This optimising is only possible if the visits are ordered by time!!!
    console.log(inter.length);
    let data: number[] = [];
    for(let i=0;i<inter.length-1;i++) {
      data.push(0);
    }
    for(let visit of visits) {
//      console.log(visit);
      let a = visit.time.split(":",2);
      let akt = moment().set('hour', +a[0]).set('minute', +a[1]);
      for(let i=start;i<inter.length-1;i++) {
        let von = moment().set('hour', inter[i].h).set('minute', inter[i].m);
        let bis = moment().set('hour', inter[i+1].h).set('minute', inter[i+1].m);
//        console.log(akt.format("HH:mm") + " / " + von.format("HH:mm")+ " / " + bis.format("HH:mm") + " :: " + akt.isBetween(von, bis, 'minute','[)'));
        if(akt.isBetween(von, bis, 'minute','[)')) {
          data[i]++;
//        break;
        } else {
//          start++;  // This optimising is only possible if the visits are ordered by time!!!
        }
      }
    }
    if(mode == 1) {
      return ([{data:data,  label: 'Lunch'}]);
    } else {
      return ([{data:data,  label: 'Dinner'}]);
    }
  }
  // events
  
  public chartHovered(e:any):void {
    console.log(e);
  }

 recommendMenues( user) {
     this.events.subscribe(Unibz.UNIBZ_SUGESTION, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION);
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION_ERR);
     
      this.recoms = data;
      switch(+this.user.mealtyp) {
   
        case 1: //primo
          this.fits0 = this.recoms.firsts[0];
          this.fits1 = this.recoms.firsts[1];
          this.fits2 = this.recoms.firsts[2];
          break;        
        case 2: //secundo
          this.fits0 = this.recoms.seconds[0];
          this.fits1 = this.recoms.seconds[1];
          this.fits2 = this.recoms.seconds[2];
          break;        
        case 3: //full
          this.fits0 = this.recoms.fulls[0];
          this.fits1 = this.recoms.fulls[1];
          this.fits2 = this.recoms.fulls[2];
          break;        
      }
      
    });
    this.events.subscribe(Unibz.UNIBZ_SUGESTION_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION);
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION_ERR);
      let alert = this.alertCtrl.create({
        title: "Error getting recoms",
        message: err._body,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      return;
    });


   let wanted: string = "L";
    if(this.mealtime != 'lunch') {
      wanted = "D";
    }
  
    let menu: string = "mensa";    
    if(this.mealtime == 'lunch') {
      menu = UserModel.MENU_BZ[+this.user.menutypL];
    } else {
      menu = UserModel.MENU_BZ[+this.user.menutypD];
    }
    if(menu == "") {
      menu = "mensa";      
    }
    let other = "";
    if(this.mealtime == 'lunch') {
      other = UserModel.MENU_BZ[+this.user.menutypD];
    } else {
      other = UserModel.MENU_BZ[+this.user.menutypL];
    }
    if(other == "") {
      other = "mensa";
    }
  
   this.unibz.getRecommends(this.user,wanted, menu, other);
   
  }
/*
  doInput() {
    let inputModal = this.modalCtrl.create(ModalInput, {  });
    inputModal.onDidDismiss(data => {
      //this.setStates("")
      console.log("doInput: " + data);
      if(data.length > 0) {

//        this.data = data;
//        this.chat.yesRunState = 5;  
//        this.cont0();
        return;
      }
//      this.chat.noRunState = 5;  
//      this.cont1();
    });
    inputModal.present();
  }
*/

// Reakt on inputfield
  submitI(e){
    console.log("submitI; " + e);
  }
  changeI(e){
 //   this.cont0();
  }
  focusI(e){
    console.log("focusI; " + e);
  }
  skip() {
    console.log("skip or Ok");
    this.cont1();
  }
  blur() {
    console.log("blur ");
//    this.cont0();
  }

}