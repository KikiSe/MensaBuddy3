import { Component } from '@angular/core';
import { Platform, Events,ModalController } from 'ionic-angular';

import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { SwitchService }     from '../../services/switchservice';

import { Tags } from '../../interfaces/tags';

import { UserModel } from '../../models/user-model';
import { Data } from '../../providers/userdata';
import { Sergelution } from '../../providers/sergelution';
import { Unibz } from '../../providers/unibz';
import { MiscData }     from '../../providers/miscdata';
import { Modalinfo }     from '../modalinfo/modalinfo';
import { ModalSurvey }     from '../modalsurvey/modalsurvey';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers: [Unibz]
})
export class SettingsPage {
  public genders: string[] = []; 
  public activities: string[] = []; 
  public breakfasts: string[] = []; 
  public user: UserModel = new UserModel();
  public showSurvey:boolean = true;

  constructor(
     public navCtrl: NavController, 
     private switchService: SwitchService,
     private alertCtrl: AlertController,
     public platform: Platform,
     public sergelution: Sergelution,
     public unibz: Unibz,
     public userService: Data,
     public misc: MiscData,
     public events: Events,
     public modalCtrl: ModalController,
     ) { 
    //   this.ionViewDidEnter();
        console.log("SettingsPage constructor");
        for(let i = 0, j=UserModel.GENDERS.length;i<j;i++) {
          this.genders.push(UserModel.GENDERS[i]);
        } 
        for(let i = 0, j=UserModel.ACTIVITIES.length;i<j;i++) {
          this.activities.push(UserModel.ACTIVITIES[i]);
        } 
        for(let i = 0, j=UserModel.BREAKFASTS.length;i<j;i++) {
          this.breakfasts.push(UserModel.BREAKFASTS[i]);
        } 
      } 

  ionViewDidEnter() {
    this.events.subscribe(Tags.USERDATA_READY, (data) => {
      this.events.unsubscribe(Tags.USERDATA_READY);
      console.log("ionViewDidEnter() first time data received");
      console.log(data);
      this.user = data;
 
      this.events.subscribe(Sergelution.SHOWSURVEY_READY, show => {
        this.events.unsubscribe(Sergelution.SHOWSURVEY_READY);
        this.events.unsubscribe(Sergelution.SHOWSURVEY_READY_ERR);
        console.log(show);
        this.showSurvey = (show.showIt == 1);
        console.log("this.showSurvey:" + this.showSurvey);
      });
      this.events.subscribe(Sergelution.SHOWSURVEY_READY_ERR, err => {
        this.events.unsubscribe(Sergelution.SHOWSURVEY_READY);
        this.events.unsubscribe(Sergelution.SHOWSURVEY_READY_ERR);
        this.showSurvey = true;
      });
      this.sergelution.getShowSurvey(this.user);      
      this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSENTER]);
      this.events.subscribe(Tags.USERDATA_READY, (data) => {
        console.log("ionViewDidEnter() else time data received");
        console.log(data);
        this.user = data;
      });
    });
    this.userService.prepData();
    
  }
  ionViewDidLeave() {
    this.sergelution.postUserData(this.user);
//    this.events.unsubscribe(Tags.USERDATA_READY);
//    console.log("ionViewDidLeave()");
  }

  switch() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSWITCHTOCHAT]);
    this.events.unsubscribe(Tags.USERDATA_READY);    
    this.misc.setLastTab(this.navCtrl.parent.getSelected().index);
    this.switchService.switchTo("chat");
  }
    
  onNameChange() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGENAME]);
    this.userService.save(this.user);
  }
  onAgeChange() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEAGE]);
    this.userService.save(this.user);
  }
  onWeightChange() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEWEIGHT]);
    this.userService.save(this.user);
  }
  onHeightChange() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEHEIGHT]);
    this.userService.save(this.user);
  }
  onSnackChange() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGESNACKS]);
    this.userService.save(this.user);
  }

  changeGender() : void {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEGENDER] + 1);
    console.log("changeGender()");
    this.userService.save(this.user);
  }
  cancelGender() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEGENDER] + 0);
    console.log("cancelGender()");    
  }

  changeActivity() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEACTITYP] + 1);
    console.log("changeActivity()");    
    this.userService.save(this.user);
  }
  cancelActivity() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEACTITYP] + 0);
    console.log("cancelActivity()");    
  }

  changeBreakfast() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEBREAKFAST] + 1);
    console.log("changeActivity()");    
    this.userService.save(this.user);
  }
  cancelBreakfast() {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSCHANGEBREAKFAST] + 0);
    console.log("cancelActivity()");    
  }
  

  saveSettings(event) {
    this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSSAVESETTINGSBUTTON]);
    if(this.userService.isAllSet(this.user)) {

//      this.user.logged = true;

      if(this.user.logged) {
        console.log("changeSettings" );
        console.log(event );
        this.events.subscribe(Unibz.UNIBZ_PROFILE_PUT, (data) => {
          this.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT);
          this.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT_ERR);
          console.log("UNIBZ_PROFILE_PUT" );
          console.log(data);
          this.events.subscribe(Tags.USERDATA_REG_READY, (data) => {
            this.events.unsubscribe(Tags.USERDATA_REG_READY);
            this.showSurvey = false;
            let alert = this.alertCtrl.create({
              title: "Change Profile settings",
              message: 'Your Profile is successfully changed!',
              buttons: [{text: 'Ok',role: 'cancel',}]
            });
            alert.present();
            return;          
          });
     //     this.user.logged = true;
          this.userService.registrationSave(this.user);
          this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSSAVESETTINGSBUTTON]+1);
        });
        this.events.subscribe(Unibz.UNIBZ_PROFILE_PUT_ERR, (err) => {
          this.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT);
          this.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT_ERR);
          console.log("UNIBZ_PROFILE_PUT_ERR" );
          console.log(err);
          let alert = this.alertCtrl.create({
            title: "Change Profile settings error",
            message: err._body,
            buttons: [{text: 'Back',role: 'cancel',}]
          });
          alert.present();
        });
        this.unibz.putProfile(this.user);
      } else {
        console.log("saveSettings" );
        console.log(event );
        this.events.subscribe(Unibz.UNIBZ_REGISTRATION, (data) => {
          this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION);
          this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION_ERR);
          console.log("UNIBZ_REGISTRATION" );
          console.log(data);
          this.events.subscribe(Tags.USERDATA_REG_READY, (data) => {
            this.events.unsubscribe(Tags.USERDATA_REG_READY);
            let alert = this.alertCtrl.create({
              title: "Profile settings",
              message: 'Your profile is successfully registered!',
              buttons: [{text: 'Ok',role: 'cancel',}]
            });
            alert.present();
            return;          
          });
          this.user.email = data.username;
          this.user.logged = true;
          this.userService.registrationSave(this.user);
          this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSSAVESETTINGSBUTTON]+0);
        });
        this.events.subscribe(Unibz.UNIBZ_REGISTRATION_ERR, (err) => {
          this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION);
          this.events.unsubscribe(Unibz.UNIBZ_REGISTRATION_ERR);
          console.log("UNIBZ_REGISTRATION_ERR" );
          console.log(err);
          let alert = this.alertCtrl.create({
            title: "Profile settings error",
            message: err._body,
            buttons: [{text: 'Back',role: 'cancel',}]
          });
          alert.present();
        });
   //     this.user.email = "u" + this.user.id;
   //     this.user.password = this.user.email;
        console.log(this.user);
        this.unibz.doRegister(this.user);
      }
    } else {
      this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSPROFILINFOMISSING]);
      let alert = this.alertCtrl.create({
        title: "Profile settings",
        message: 'Please fullfill your settings to get recommendations!',
        buttons: [{text: 'Back',role: 'cancel',}]
      });
      alert.present();
    }
  }

  presentInfoModal() {
    console.log("infomodal");
      let infoModal;
       infoModal = this.modalCtrl.create(Modalinfo);
       infoModal.present();
       this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSGETMOREINFO]);
   
  }

  presentSurveyModal() {
    console.log("surveymodal");
      let surveyModal;
       surveyModal = this.modalCtrl.create(ModalSurvey);
       surveyModal.present();
       this.sergelution.postUserAction(this.user, Tags.SETTINGS[Tags.SETTINGSDOSURVEY]);
   
  }

}


