import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { Sergelution } from '../../providers/sergelution';
import { Data }        from '../../providers/userdata';
import { MiscData }     from '../../providers/miscdata';
import { Datum } from '../../providers/datum';

import * as moment from 'moment';

@Component({
  selector: 'modaldaysfreq-page',
  templateUrl: 'daysfreq.html'
})
export class ModalDaysFreq {

   constructor(public viewCtrl: ViewController, 
              public params: NavParams,
              public sergelution: Sergelution,
              public userData: Data,
              public datum: Datum,
              public misc: MiscData
              ) {
                let inter = params.get("today");
                console.log(inter);

                this.today = inter.moment.format("MMM DD, YYYY");
                this.visits = params.get("visits");
                this.buildLunchLineChart(this.visits);
                this.buildDinnerLineChart(this.visits);
  } 

  today: string ;
  visits:  any[] ;
  public lcHeader: string = "";
  public lcDataset: any = [{data: [0,0,0,0,0,0,0,0,0], label: ""}];
  public lcLabels: any = ['12:00','12:15','12:30','12:45','13:00','13:15','13:30','13:45','14:00'];
  public lcOptions: any = { responsive: true };
 public lcColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
 public lcLegend:boolean = false;
 public lcType = 'bar'; 

  public lcDinHeader: string = "";
  public lcDinDataset: any = [{data: [0,0,0,0,0,0,0,0], label: ""}];
  public lcDinLabels: any = ['18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30'];
  public lcDinOptions: any = { responsive: true };
 public lcDinColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
 public lcDinLegend:boolean = false;
 public lcDinType = 'bar'; 


  buildLunchLineChart(visits: any[]) {
    let inter = [{h:11,m:45},{h:12,m:0},{h:12,m:15},{h:12,m:30},{h:12,m:45},{h:13,m:0},{h:13,m:15},{h:13,m:30},{h:13,m:45},{h:14,m:0}];
    this.lcDataset= [{data: this.buildTheData(visits, inter), label: 'visiters'}];
  }
  buildDinnerLineChart(visits: any[]) {
    let inter = [{h:18,m:30},{h:18,m:45},{h:19,m:0},{h:19,m:15},{h:19,m:30},{h:19,m:45},{h:20,m:0},{h:20,m:15},{h:20,m:30}];
    this.lcDinDataset= [{data: this.buildTheData(visits, inter), label: 'visiters'}];
  }

  buildTheData(visits: any[], inter: any[]): number[] {
    let start = 0; // This optimising is only possible if the visits are ordered by time!!!
    console.log(inter.length);
    let data: number[] = [];
    for(let i=0;i<inter.length-1;i++) {
      data.push(0);
    }
    for(let visit of visits) {
      let a = visit.time.split(":",2);
      let akt = moment().set('hour', +a[0]).set('minute', +a[1]);
      for(let i=start;i<inter.length-1;i++) {
        let von = moment().set('hour', inter[i].h).set('minute', inter[i].m);
        let bis = moment().set('hour', inter[i+1].h).set('minute', inter[i+1].m);
//        console.log(akt.format("HH:mm") + " / " + von.format("HH:mm")+ " / " + bis.format("HH:mm") + " :: " + akt.isBetween(von, bis, 'minute','[)'));
        if(akt.isBetween(von, bis, 'minute','[)')) {
          data[i]++;
 //         break;
        } else {
 //         start++;  // This optimising is only possible if the visits are ordered by time!!!
        }
      }
    }
    return data;
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

  dismiss() {
      this.viewCtrl.dismiss();
  }
}
