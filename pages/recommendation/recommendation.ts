import { Component } from '@angular/core';
import { NavController, NavParams, Platform, Events, AlertController,ToastController } from 'ionic-angular';
import { MenueModel } from '../../models/menue-model';
import { UserModel } from '../../models/user-model';
import { SwitchService }     from '../../services/switchservice';
import { Sergelution } from '../../providers/sergelution';
import { Unibz } from '../../providers/unibz';
import { Data } from '../../providers/userdata';

import { MiscData }     from '../../providers/miscdata';
import { Recommendation } from '../../providers/recommendation';
import { Datum } from '../../providers/datum';

import { Tags } from '../../interfaces/tags';
import * as moment from 'moment';
//import { ItemSliding } from 'ionic-angular';

@Component({
  selector: 'page-recommendation',
  templateUrl: 'recommendation.html',
  providers: [Unibz]
})
export class RecommendationPage {
  ioncard: boolean = false;
  offset: number = 0;
  user: any = new UserModel();
  today: any;
  kind: string[] = ["first", "second", "side"];
  firstmenues : MenueModel[];
  secondmenues : MenueModel[];
  sidemenues : MenueModel[];
  recoms: any;
  fits0: any;
  fits1: any;
  fits2: any;
  fits3: any;
  fits4: any;
  fits5: any;
  hide0: boolean = false;
  hide1: boolean = false;
  hide2: boolean = false;
  hide3: boolean = false;
  hide4: boolean = false;
  hide5: boolean = false;

  hideImg: boolean = false;

  hideRecommendation: boolean = false;
  recommendationDay: string = 'today';
  recomMoment: any;

  history: any[] = [];
  eaten: any[] = [];
  eatensalt: any;
  eatensfa: any;
  eatensugars: any;
  recipes: any = [];
  menusugars: any;
  menusfa: any;
  menusalt: any;
  dailyneedsugar: any;
  dailyneedsfa: any;
  dailyneedsalt: any;

  mealtime: string ="lunch";
  buttonText: string = "I will eat this!";
  buttonFunc: boolean = false;

  constructor(
    public navCtrl: NavController, 
    private toastCtrl: ToastController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public sergelution: Sergelution,
    public unibz: Unibz,
    public userService: Data,
    public datum: Datum,
    public misc: MiscData,
    public platform: Platform,
    public events: Events,
    public switchService: SwitchService
    
  ) {
    
  }

  checkForDish(mealtime: string): boolean {
    let retval = false;
    if(this.eaten) {
      this.eaten.forEach( (dish) => {
        console.log(dish);
        console.log(dish.meal == mealtime);
        if(dish.meal == mealtime) {
          retval = true;
        }
      });
    }
    return retval;
  }

ionViewDidLoad() {
  console.log('ionViewDidLoad RecommendationPage');
  this.events.subscribe(Tags.USERDATA_READY, (user) => {
    this.events.unsubscribe(Tags.USERDATA_READY);
    console.log("ionViewDidEnter() data received");
    console.log(user);
    this.user = user;

 //   this.showToastWithCloseButton();
  
    if(this.misc.getRecomToast()) {
      this.showAlertWithDescription();
    }

    this.events.subscribe(Unibz.UNIBZ_EATENDISHES, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
      console.log("getEatenDishes");
      console.log(data);
      this.eaten = data;
    });
    this.events.subscribe(Unibz.UNIBZ_EATENDISHES_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
      this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
      let alert = this.alertCtrl.create({
          title: "Error during " + Unibz.UNIBZ_EATENDISHES,
          message: "Err: " + err,
          buttons: [{ text: 'Ok', role: 'cancel', handler: ( (data) => { alert.dismiss(); return;}) }]
      });
     alert.present();
    });
    if(this.user.logged) {
      this.unibz.getEatenDishes(user, moment().format("YYYY-MM-DD"));
    }  else {
      let alert = this.alertCtrl.create({
          title: "Profile missing",
          message: "Please complete your Profile",
          buttons: [{ text: 'Ok', role: 'cancel', handler: ( (data) => { alert.dismiss(); return;}) }]
      });
     alert.present();
    }
  });
  this.userService.prepData();
  this.recommendationDay = this.misc.getRecomDay();
}

ionViewDidEnter(){
  this.events.subscribe(Tags.USERDATA_READY, (user) => {
    this.events.unsubscribe(Tags.USERDATA_READY);
    console.log("ionViewDidEnter() first data received");
    console.log(user);
    this.user = user;
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOENTER]);
    this.events.subscribe(Tags.USERDATA_READY, (user) => {
      console.log("ionViewDidEnter() next data received");
      this.user = user;
      this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOENTER]);
    });
  });
  this.userService.prepData();
}

/*showToastWithCloseButton() {
    let toast = this.toastCtrl.create({
      message: 'Please choose the supposedly kind of lunch/ dinner for today and choose if you like a primo, secondo or full menu',
      showCloseButton: true,
      position: "top",
      closeButtonText: "ok",
    });
    toast.onDidDismiss(() => {
      this.misc.setRecomToast(false);  
      console.log('Dismissed toast');
      this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOTOASTWHATTODO]);
    });
    toast.present();
}*/
showAlertWithDescription() {

    let alert = this.alertCtrl.create({
        title: "How to get recommendations!",
        subTitle: "To calculate recommendations you have to tell us two more information.",
        message: "To get recommendations for Lunch, tell us your type of dinner you will eat."+ "<br>" + "To get recommendations for Dinner, tell us what type of lunch you already ate."+"<br>"+"Then choose the Mensa Menu Size. If you want a Primo, Secondo or Full Menu!",
        buttons: [{ text: "Don't show again", handler:() =>{this.misc.setRecomToast(false),this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOTOASTWHATTODO]+1);}, role: 'cancel'},  
        { text: 'Okay',handler:() =>{this.misc.setRecomToast(true),this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOTOASTWHATTODO]+0);}, role: 'cancel' }]
      });
      alert.present();
}


  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.events.unsubscribe(Tags.USERDATA_READY);
    this.misc.setRecomDay(this.recommendationDay);
    console.log("recommendation: onDestroy");
  }

  switch() {
      this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOSWITCHTOCHAT]);
      this.misc.setLastTab(this.navCtrl.parent.getSelected().index);
      this.switchService.switchTo("chat");
  }

  recommend(){
    this.hideRecommendation = false;
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECORECOMMENDATIONBUTTON]);
    let msg = "";
    if(! this.user.logged) {
      msg ='Please complete your profile to get recommendations!'
     
      this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECORECOMMENDATIONTOASTBECAUSEMISSINGPROFIL]);
    }
    
    if(this.mealtime == "lunch"){
      if( this.user.menutypD == 0 || this.user.mealtyp == 0 ){
        msg = 'Dinner or Meals Type is missing';
      }
     }else {
      if( this.user.menutypL == 0 || this.user.mealtyp == 0 ){
        msg = 'Lunch or Meal Type is missing!';
      }
    }

    if(msg.length != 0){
      const toast = this.toastCtrl.create({
        message: msg,
        duration: 3000,
        position: "middle",
    });
    toast.present();
  
  } else {
      this.updateEaten();
      this.buildRecommendation();
      this.hideRecommendation = true;
    }
  }

  updateEaten() {
      this.buttonFunc = false;
      if(this.mealtime == "lunch"){
        this.buttonFunc = this.checkForDish("L");
      } else {
        this.buttonFunc = this.checkForDish("D");
      }
      if(this.buttonFunc) {
        this.receptPartIndex = 1;
        this.buttonText = "Already eaten!";
      } else {
        this.receptPartIndex = 0;
        this.buttonText = "I will eat this!";
      }
      console.log("this.buttonFunc: " + this.buttonFunc);
      console.log("this.receptPartIndex: " + this.receptPartIndex);
      console.log("this.hideRecommendation = true")
  }

  receptMsg: any[] = [];
  receptMsgPart: string = "";
  receptPart: any[] = [{text: "Add to History", handler: () => { this.addToHistory();}}, {}];
  receptIndex: number;
  receptPartIndex: number = 0;

  clickCard(index: number){
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOCLICKEDONRECOCARD]+index);
    this.showRecept(index);
  }
  showRecept(index: number) {
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOIWILLEATTHIS]);
    this.receptIndex = index;
    let prompt = this.alertCtrl.create({
      title: "Add to History",
      message: "You will eat this? Then add this menu to your history",
      buttons: [
        { text: 'Back', role: 'cancel', handler:() => {this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOIWILLEATTHIS]+0);}},
         this.receptPart[this.receptPartIndex],
        ]
    });
    prompt.present();
  }

  showInfo(index: number) {
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOSHOWINFO]);
    this.receptIndex = index;
    let prompt = this.alertCtrl.create({
      title: "More nutritional Information",
      message: this.receptMsg[this.receptIndex],
      buttons: [
        { text: 'Back', role: 'cancel', handler:() => {this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOSHOWINFO]+0);}},
          this.receptPart[this.receptPartIndex],
        ]
    });
    prompt.present();
  }


  addToHistory() {
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOIWILLEATTHIS]+1);
    let mealtyp = "L";
    if(this.mealtime != 'lunch') {
      mealtyp = 'D';
    }
    let dishes = [];
    console.log(this.fits0[0][0]);
    switch(this.receptIndex) {
      case 0:
      dishes.push(this.fits0[0][0].name);
      if(this.fits0[0][1]) {
        dishes.push(this.fits0[0][1].name);
      }
      if(this.fits0[0][2]) {
        dishes.push(this.fits0[0][2].name);
      }
      break;
      case 1:
      dishes.push(this.fits1[0][0].name);
      if(this.fits1[0][1] != undefined) dishes.push(this.fits1[0][1].name);
      if(this.fits1[0][2] != undefined) dishes.push(this.fits1[0][2].name);
      break;
      case 2:
      dishes.push(this.fits2[0][0].name);
      if(this.fits2[0][1] != undefined) dishes.push(this.fits2[0][1].name);
      if(this.fits2[0][2] != undefined) dishes.push(this.fits2[0][2].name);
      break;
      case 3:
      dishes.push(this.fits3[0][0].name);
      if(this.fits3[0][1] != undefined) dishes.push(this.fits3[0][1].name);
      if(this.fits3[0][2] != undefined) dishes.push(this.fits3[0][2].name);
      break;
      case 4:
      dishes.push(this.fits3[0][0].name);
      if(this.fits4[0][1] != undefined) dishes.push(this.fits4[0][1].name);
      if(this.fits4[0][2] != undefined) dishes.push(this.fits4[0][2].name);
      break;
      case 5:
      dishes.push(this.fits5[0][0].name);
      if(this.fits5[0][1] != undefined) dishes.push(this.fits5[0][1].name);
      if(this.fits5[0][2] != undefined) dishes.push(this.fits5[0][2].name);
      break;
    }
    this.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
      console.log("update");
      console.log(data);
      this.events.subscribe(Unibz.UNIBZ_EATENDISHES, (data) => {
        this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
        this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
        console.log("getEatenDishes");
        console.log(data);
        this.eaten = data;
        this.updateEaten();
      });
       this.events.subscribe(Unibz.UNIBZ_EATENDISHES_ERR, (err) => {
        this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
        this.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
        let alert2 = this.alertCtrl.create({
          title: "Error",
          message: "Sorry something went wrong.. "+ err,
          buttons: [{ text: 'Back', role: 'cancel'}]
        });
        alert2.present();
      });
      this.unibz.getEatenDishes(this.user, moment().format("YYYY-MM-DD"));

      let msg = "calories: " + data.kcal + 'g <br />' + 
                "carbs: " + data.carbs + 'g <br />' + 
                "sugars: " + data.sugars + 'g <br />' + 
                "proteins: " + data.proteins + 'g <br />' + 
                "fiber: " + data.fiber +'g <br />' + 
                "fats: " + data.fats + 'g <br />' + 
                "saturated_fatty_acids: " + data.saturated_fatty_acids + 'g <br />' + 
                "salt: " + data.salt + 'g';
      let alert = this.alertCtrl.create({
        title: "Your History has been updated",
        message: msg,
        buttons: [{ text: 'Ok', role: 'cancel' }]
      });
      alert.present();
    });
    
    this.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
      this.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
      let alert2 = this.alertCtrl.create({
        title: "Error",
        message: "Sorry something went wrong.. "+ err,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert2.present();
    });
    this.unibz.updateUserHistory(this.user, mealtyp, dishes);
  }

  changeMealTime(index: number){
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOCHANGEMENUTIME] +index);
//    this.userService.save(this.user);
    console.log("changeMealTime("+index+")");
    this.hideRecommendation = false;
//    console.log(this.user);
    
  }

  changeMealTyp(){
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOCHANGEMEALTYP]);
    this.userService.save(this.user);
    console.log("changeMealTyp()");
    console.log(this.user);
    
  }
  changeMenuTypL(){
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOCHANGEMENUTYPL]);
    this.userService.save(this.user);
    console.log("changeMealTyp()");
    console.log(this.user);

  }
  changeMenuTypD(){
    this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOCHANGEMENUTYPD]);
    this.userService.save(this.user);
    console.log("changeMealTyp()");
    console.log(this.user);
  }
  changedRecoDay(day: string){
     this.recommendationDay = day;
  }

buildRecommendation() {
    this.events.subscribe(Unibz.UNIBZ_SUGESTION, (data) => {
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION);
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION_ERR);

      this.recoms = data;
      this.fits0=undefined;
      this.fits1=undefined;
      this.fits2=undefined;
      this.fits3=undefined;
      this.fits4=undefined;
      this.fits5=undefined;
      this.hide1=false;
      this.hide2=false;
      this.hide3=false;
      this.hide4=false;
      this.hide5=false;
      if(this.recoms.fulls.length == 0) {
          this.ioncard = true;
          return;
      }
      switch(+this.user.mealtyp) {
        case 1: //primo
          console.log("anzahl primos");
          console.log(this.recoms.firsts.length);
          for(let i=0;i<this.recoms.firsts.length;i++) {
            console.log(this.recoms.firsts[i][0][0].name);
          }
          if(this.recoms.firsts.length>6) {
            this.getTheRecoms(this.recoms.firsts);
          } else {
            this.getTheRecomsWoRand(this.recoms.firsts);
          }
          break;        
        case 2: //secundo
          console.log("anzahl secundos");
          console.log(this.recoms.seconds.length);
          for(let i=0;i<this.recoms.seconds.length;i++) {
            console.log(this.recoms.seconds[i][0][0].name);
          }
          if(this.recoms.seconds.length>6) {
            this.getTheRecoms(this.recoms.seconds);
          } else {
            this.getTheRecomsWoRand(this.recoms.seconds);
          }
          break;        
        case 3: //full
          console.log("anzahl fulls");
          console.log(this.recoms.fulls.length);
          for(let i=0;i<this.recoms.fulls.length;i++) {
            console.log(this.recoms.fulls[i][0][0].name);
          }
          if(this.recoms.fulls.length>6) {
            this.getTheRecoms(this.recoms.fulls);
          } else {
            this.getTheRecomsWoRand(this.recoms.fulls);
          }
          break;        
      }
      this.receptMsg = [];
      this.buildWarnings(this.fits0);
      
   
      this.buildWarnings(this.fits0);

      if(! this.fits1) {
        this.hide1=true;
      } else {
        this.buildWarnings(this.fits1);
      }

     if(! this.fits2) {
        this.hide2=true;
      } else {
        this.buildWarnings(this.fits2);
      }

     if(! this.fits3) {
        this.hide3=true;
      } else {
        this.buildWarnings(this.fits3);
      }

     if(! this.fits4) {
        this.hide4=true;
      } else {
        this.buildWarnings(this.fits4);
      }

     if(! this.fits5) {
        this.hide5=true;
      } else {
        this.buildWarnings(this.fits5);
      }

    });
    this.events.subscribe(Unibz.UNIBZ_SUGESTION_ERR, (err) => {
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION);
      this.events.unsubscribe(Unibz.UNIBZ_SUGESTION_ERR);
      let alert = this.alertCtrl.create({
        title: "Error getting recoms",
        message: err._body,
        buttons: [{ text: 'Back', role: 'cancel'}]
      });
      alert.present();
      return;
    });
    let wanted: string = "L";
    if(this.mealtime != 'lunch') {
      wanted = "D";
    }
    let menu: string = "mensa";    
    if(this.mealtime == 'lunch') {
      menu = UserModel.MENU_BZ[+this.user.menutypL];
    } else {
      menu = UserModel.MENU_BZ[+this.user.menutypD];
    }
    if(menu == "") {
      menu = "mensa";      
    }
    let other = "";
    if(this.mealtime == 'lunch') {
      other = UserModel.MENU_BZ[+this.user.menutypD];
    } else {
      other = UserModel.MENU_BZ[+this.user.menutypL];
    }
    if(other == "") {
      other = "mensa";
    }
    this.unibz.getRecommends(this.user,wanted, menu, other);
  }

  getTheRecoms(recs) {
    let was = [];
    let anz = recs.length;
    for(let i=0;i<anz;i++) {
      was.push(i);
    }

    let pos = Math.floor(Math.random()*anz);
    this.fits0 = recs[+was[+pos]];
    let index = was.indexOf(pos,1);
    was.splice(index,1);

    anz--;
    pos = Math.floor(Math.random()*anz);
    this.fits1 = recs[+was[+pos]];
    index = was.indexOf(pos,1);
    was.splice(index,1);

    anz--;
    pos = Math.floor(Math.random()*anz);
    this.fits2 = recs[+was[+pos]];
    index = was.indexOf(pos,1);
    was.splice(index,1);

    anz--;
    pos = Math.floor(Math.random()*anz);
    this.fits3 = recs[+was[+pos]];
    index = was.indexOf(pos,1);
    was.splice(index,1);

    anz--;
    pos = Math.floor(Math.random()*anz);
    this.fits4 = recs[+was[+pos]];
    index = was.indexOf(pos,1);
    was.splice(index,1);

    anz--;
    pos = Math.floor(Math.random()*anz);
    this.fits5 = recs[+was[+pos]];
    index = was.indexOf(pos,1);
    was.splice(index,1);
  }
  getTheRecomsWoRand(recs) {
    this.fits0 = recs[0];
    if(recs.length>1)
      this.fits1 = recs[1];
    if(recs.length>2)
      this.fits2 = recs[2];
    if(recs.length>3)
      this.fits3 = recs[3];
    if(recs.length>4)
      this.fits4 = recs[4];
    if(recs.length>5)
      this.fits4 = recs[5];
  }

  buildWarnings(fit) {
    console.log("eatensfa "+ this.eatensfa);
    this.menusalt = fit[2].salt_in_menu;
    this.menusugars = fit[2].sugars_in_menu;
    this.menusfa = fit[2].sfa_in_menu;
    console.log("menusfa "+ this.menusfa);
    this.dailyneedsfa = this.fits0[2].sfa_daily_limit.toFixed(1);
      console.log("dailysfa "+ this.dailyneedsfa);
      this.dailyneedsugar = this.fits0[2].sugars_daily_limit.toFixed(1);
      this.dailyneedsalt = this.fits0[2].salt_daily_limit.toFixed(1);


      if(!this.fits0[2].sfa_eaten_today){
        this.eatensfa = 0;
      }else{
         this.eatensfa = this.fits0[2].sfa_eaten_today.toFixed(1);
      }
      if(!this.fits0[2].sugars_eaten_today){
        this.eatensugars = 0;
      }else{
         this.eatensugars = this.fits0[2].sugars_eaten_today.toFixed(1);
      }
      if(!this.fits0[2].salt_eaten_today){
        this.eatensalt = 0;
      }else{
        this.eatensalt = this.fits0[2].salt_eaten_today.toFixed(1);
      }
    
    let menusugarstext =  this.menusugars;
    let menusalttext =  this.menusalt;
    let menusfatext =  this.menusfa;

    if(+this.menusugars > +this.dailyneedsugar){
      this.menusugars = this.menusugars.toFixed(1);
      menusugarstext = "<strong><font color='#FF6F00' >"  +  this.menusugars  +  "</font></strong>";
    }
    if(+this.menusalt > +this.dailyneedsalt){
      this.menusalt = this.menusalt.toFixed(1);
      menusalttext = "<strong><font color='#FF6F00' >"  +  this.menusalt +  "</font></strong>";
    }
    if(+this.menusfa > +this.dailyneedsfa){
      this.menusfa = this.menusfa.toFixed(1);
      menusfatext = "<strong><font color='#FF6F00'>"  +  this.menusfa  +  "</font></strong>";
      console.log(this.menusfa + " > " + this.dailyneedsfa);
    }
    this.receptMsg.push("Limiting the intake of <b>sugars</b>, <b>saturated fatty acids</b> and <b>salt</b> lowers the risk of food-related diseases" + "<br>"
            + "<br>"
            + "Your daily limits: "
            + "<br>"
            + "<b>sugar:  </b>" + this.dailyneedsugar + " gr <br>"
            + "<b>saturated fatty acids: </b>"  + this.dailyneedsfa + "  gr <br>"
            + "<b>salt: </b>"  + this.dailyneedsalt + " gr <br>"
            +  "<br>"
            + "This menu provides: "
            + "<br>"
            + "<b>sugar: </b>" + menusugarstext + " gr"
            + "<br>"
            + "<b>saturated fatty acids: </b>"  + menusfatext+ " gr"
            + "<br>"
            + "<b>salt: </b>"  + menusalttext + " gr <br>"
            +  "<br>"
            + "You ate already: "
            + "<br>"
            + "<b>sugars: </b>" + this.eatensugars + " gr"
            + "<br>"
            + "<b>saturated fatty acids: </b>"  + this.eatensfa+ " gr"
            + "<br>"
            + "<b>salt: </b>"  + this.eatensalt + " gr"
    );
  }

}
