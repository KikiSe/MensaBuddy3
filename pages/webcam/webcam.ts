import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { SwitchService }     from '../../services/switchservice';
import { MiscData }     from '../../providers/miscdata';
import { ModalController } from 'ionic-angular';
import { Events } from 'ionic-angular';

import * as moment from 'moment';

import { Tags } from '../../interfaces/tags';

import { UserModel } from '../../models/user-model';
import { AlertController } from 'ionic-angular';
import { Data } from '../../providers/userdata';
import { Sergelution } from '../../providers/sergelution';
import { TimeTicker } from '../../providers/akttime';

import { ModalDaysFreq } from '../daysfreq/daysfreq';


@Component({
  selector: 'page-webcam',
  templateUrl: 'webcam.html'
})
export class WebcamPage {

  public thisWeek: any[] ;
  public backOne: any[] ;
  public backTwo: any[] ;
  
  public theWeek: string[] ;
  public theDay: string;
  public theTime: string;
  public theMensaOpening: string;
  public user: UserModel;
  hideCheckInbutton: boolean = false;
  hideVideoMensa: boolean = false;

  constructor(
    public navCtrl: NavController, 
    private switchService: SwitchService,
    public tt: TimeTicker,
    public modalCtrl: ModalController,
    public misc: MiscData,
    public alertCtrl: AlertController,
    public userData: Data,
    public sergelution: Sergelution,
    public events: Events,
    ) { 
      let aktMoment = moment(new Date().toISOString());
      this.theDay = aktMoment.format("dddd");
      this.theTime = aktMoment.format("HH:mm");
      this.buildDays();
      this.buildMensaOpen();
      this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.user = data;
      });
    this.userData.prepData();
      this.events.subscribe('aktTime:min',(min) => {
        this.theTime = min;
      });
  }

  ionViewDidEnter(){
     this.events.subscribe(Tags.USERDATA_READY,(data) => {
      this.user = data;
      this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMENTER]);
    });
    this.userData.prepData();
    if(this.misc.getWaitingAlert()) {
      this.showAlertWithDescription();
    }
  }
    
  ngOnDestroy() {
    this.events.unsubscribe('aktTime:min');
    this.events.unsubscribe('userData:ready');
  }
    
  switch() {
    this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMSWITCHTOCHAT]);
    this.misc.setLastTab(this.navCtrl.parent.getSelected().index);
    this.switchService.switchTo("chat");      
  }

  showAlertWithDescription() {
    let alert = this.alertCtrl.create({
        title: "Waiting Time",
        subTitle: "Help us to provide the Waiting Time for the mensa!",
        message: "By telling us if you go to mensa now or later, we can calculate how many students will wait infornt of the mensa! <br> You can check the waiting time for the last three weeks for each day in the calendar below.",
         buttons: [{ text: "Don't show again", handler:() =>{this.misc.setWaitingAlert(false),this.sergelution.postUserAction(this.user, Tags.RECO[Tags.RECOTOASTWHATTODO]);}, role: 'cancel'},  
        { text: 'Okay',handler:() =>{this.misc.setWaitingAlert(true)}, role: 'cancel' }]
      });
      alert.present();
}

  showCheckIn() {
    this.events.subscribe(Tags.MENSAOPEN_TIME ,(open) => {
      this.events.unsubscribe(Tags.MENSAOPEN_TIME);
      this.buildMensaOpen();
      console.log("showCheckIn: " + open);
      let user = this.user;
      console.log(user);
      if(open) { // open
//        this.alertOk(); // for debugging on non open times

      /* from here */
        if(this.tt.isLunchTime(moment())) {
          console.log(user.iGoLunch);
          if(user.iGoLunch && moment(user.iGoLunch).isSame(moment(), 'day')) {
         
            this.alertNo("Thank you", 'We already have your information when you go for todays Lunch!');
          } else {
            this.alertOk();
            
          }
        }  else if (this.tt.isDinnerTime(moment())) {
          console.log(user.iGoDinner);
          if(user.iGoDinner  && moment(user.iGoDinner).isSame(moment(), 'day')) {
            
            this.alertNo("Thank you", 'We already have your information when you go for todays Dinner!');
          } else {
            this.alertOk();
          }
        }
        /* to here */
      } else {
        this.alertNo("Mensa closed", 'Sorry, the doors are closed! No food this time today!');
        this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMCHECKINNOWBUTCLOSED]);
      }
    });
    this.tt.isMensaOpen();
  }

  showCheckInTimer() {
    this.events.subscribe(Tags.MENSAOPEN_TIME ,(open) => {
      this.events.unsubscribe(Tags.MENSAOPEN_TIME);
        this.buildMensaOpen();
    let user = this.user;
    console.log(user);
    this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMCHECKINLATER]);
    // this.alertTimer(); only for development
   /* from here  for development disable */
     if(open) { // open
       
    if(user.iGoLunch && moment(user.iGoLunch).isSame(moment(), 'day')) {
            this.alertNo("Thank you", 'We already have your information when you go for todays Lunch!');
            
          } else {
            this.alertTimer();
          }
    if (this.tt.isDinnerTime(moment())) {
          console.log(user.iGoDinner);
          if(user.iGoDinner  && moment(user.iGoDinner).isSame(moment(), 'day')) {
            this.alertNo("Thank you", 'We already have your information when you go for todays Dinner!');
           
          } else {
            this.alertTimer();
          }
        }
 /* to here */
     }
      else {
        this.alertNo("Mensa closed", 'Sorry, the doors are closed!');
       this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMCHECKINLATERBUTCLOSED]);
      }
    });
    this.tt.isMensaOpen();
}

  alertNo(text1, text2) {
    let alert = this.alertCtrl.create({
      title: text1,
      subTitle: text2,
      buttons: [{ text: 'OK', role: 'cancel' }]
    });
    alert.present();
  }
  

  alertTimer() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Choose Time');

    let von = 0;
    if(this.tt.isAfterLunchTime(moment())){
      von = UserModel.MENSATIMEDSTART;
    }
    for(let i=von; i<UserModel.MENSATIMELD.length; i++){
      alert.addInput({type: 'radio', label: UserModel.MENSATIMELD[i], value: ''+i});
    }
    
    alert.addButton({
      text: 'Cancel',
      handler: () =>{
        this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMCHECKINLATERCANCEL]);
      }
    });
    alert.addButton({
      text: 'Save',
      handler: data => {
        console.log("alertTimer");
        console.log(data);
           this.events.subscribe(Tags.IGONOW_READY, (akt) => {
              this.events.unsubscribe(Tags.IGONOW_READY);
              if(this.tt.isLunchTime(akt)) {
                console.log("save iGoLunch");
                let data = this.user;
                data.iGoLunch = akt;
                this.userData.save(data);
                this.hideCheckInbutton = false;
              } else if(this.tt.isDinnerTime(akt)) {
                console.log("save iGoDinner");
                let data = this.user;
                data.iGoDinner = akt;
                this.userData.save(data);
                this.hideCheckInbutton = false;
              }
            });

            let aktMoment = moment().set('hour', this.misc.hourMinutes[data].hour).set('minute', this.misc.hourMinutes[data].minute);
            this.sergelution.postAccessDate(this.user.id, aktMoment);
            this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMCHECKINLATERSAVED]);
      }
    });
     alert.present();
  }

  alertOk() {
      let alert = this.alertCtrl.create({
      title: 'Check In',
      subTitle: 'Now you are telling us you will go to Mensa now. <br /> We need this info to give you better Predciton how many people are on their way! ;)',
      buttons: [
        {
          text: "YES",
          handler: () => {
            this.events.subscribe(Tags.IGONOW_READY, (akt) => {
              this.events.unsubscribe(Tags.IGONOW_READY);
              if(this.tt.isLunchTime(akt)) {
                console.log("save iGoLunch");
                let data = this.user;
                data.iGoLunch = akt;
                this.userData.save(data);
                this.hideCheckInbutton = false;
              } else if(this.tt.isDinnerTime(akt)) {
                console.log("save iGoDinner");
                let data = this.user;
                data.iGoDinner = akt;
                this.userData.save(data);
                this.hideCheckInbutton = false;
              }
            });
            this.sergelution.postAccessDate(this.user.id, moment());
            this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMCHECKINNOW]);
        }
      },
      {
        text: 'NO',
        role: 'cancel'
      }
      ]
    });
    alert.present();
  }
  
  buildDays(): void {
    this.theWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    this.thisWeek = [];
    this.backOne = [];
    this.backTwo = [];
    let aktMoment = moment();
    let aktDay = aktMoment.isoWeekday();
    let theMoment = aktMoment.clone().subtract(aktDay-1, 'days');
    let last1 = theMoment.clone().add(-7,'days');
    let last2 = last1.clone().add(-7,'days');
    
    let color = 'visitOk';
    let colorOk = 'visitOk';
 
    for(let i=0;i<6;i++) {
      let datum = {moment: theMoment.clone(), day: theMoment.format('DD'), hostDay: theMoment.format('YYYY.MM.DD'), color: color};
      this.thisWeek.push(datum);
      datum = {moment: last1.clone(), day: last1.format('DD'), hostDay: last1.format('YYYY.MM.DD'), color: colorOk};
      this.backOne.push(datum);
      datum = {moment: last2.clone(), day: last2.format('DD'), hostDay: last2.format('YYYY.MM.DD'), color: colorOk};
      this.backTwo.push(datum);
      if(theMoment.isoWeekday() == aktDay) {
        color = 'visitNo';
      }
      theMoment.add(1, 'days');
      last1.add(1, 'days');
      last2.add(1, 'days');
    }
   }

   buildMensaOpen(){
    this.events.subscribe(Tags.MENSAOPEN_TIME,(open) => {
      this.events.unsubscribe(Tags.MENSAOPEN_TIME);
      console.log("BuildMensaOpen called");
      if (open){
        this.theMensaOpening = "Mensa is open";
        this.hideVideoMensa = true;
        console.log(this.theMensaOpening);
      }else{
        this.theMensaOpening = "Mensa is closed";
        console.log(this.theMensaOpening);
        this.hideVideoMensa = false;
      }
    });
    this.tt.isMensaOpen();
  }

  showDay(theday) {
    this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMSHOWDAY]+theday);
    console.log(theday);
    if(theday.color == 'visitNo') {
      return;
    }
    this.events.subscribe(Tags.MENSAUSE_READY,(day) => {
      this.events.unsubscribe(Tags.MENSAUSE_READY);
      let modal = this.modalCtrl.create(ModalDaysFreq, { today: theday, visits: day });
      modal.onDidDismiss(() => {
        this.sergelution.postUserAction(this.user, Tags.WEBCAM[Tags.WEBCAMSHOWDAYCANCEL]);
        return;
      });
      modal.present();
    });
    this.sergelution.callForUses(theday.hostDay);
  }

  

}
