import { Component } from '@angular/core';
import { Events } from 'ionic-angular';

import { NavController } from 'ionic-angular';

import { ChatModel } from '../../models/chat-model';

import { Sergelution } from '../../providers/sergelution';
import { Unibz } from '../../providers/unibz';
import { MiscData } from '../../providers/miscdata';
import { Recommendation } from '../../providers/recommendation';


import * as moment from 'moment';
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  chats;
  today: any;

  constructor(public navCtrl: NavController,        
        public events: Events,
        public misc: MiscData,
        public sergelution: Sergelution,
        public unibz: Unibz,
) {
    this.today = new Date().toISOString();
    this.chats = [];
//    this.chats.push({text:[{text: "hallo"}, {text:"ballo"}, {text:"trallo"}], bot: true, multi:true});
    let a = {text:[{t:"hallo", rclass:"rgreen"}, {t:"ballo", rclass:"ryellow"},{t:"trallo", rclass:"rred"}], bot: true, multi:true};

    this.chats.push(a);
    this.chats.push({text:[{t:"hallo", rclass:"rgreen"}, {t:"ballo", rclass:"ryellow"},{t:"trallo", rclass:"rred"}], bot: true, multi:true});
    let sb = [{text: 'hehe', rclass: 'green_reco'}, {text: 'nene', rclass: 'red_reco'}]
    this.chats.push(new ChatModel(sb, ChatModel.BOT, ChatModel.MULTI, "NaNu"));
    
    this.chats.push(new ChatModel("hallo1dddd", ChatModel.BOT, ChatModel.SINGLE));
    this.chats.push({text: "hallo2", bot: true, multi:false});
    this.chats.push({text: "hallo3", bot: true, multi:false});
    this.chats.push({text: "hallo4", bot: true, multi:false});
    this.chats.push({text: "hallo5", bot: true, multi:false});
  }

  buildChat(menues, header) {
    let sb: any[] = [];
    for(let menu of menues) {
      let it = {t: menu.name , rclass: menu.rec_class}
      sb.push(it);
    }
    let a = {text: sb, bot: true, multi:true, head: header};

    console.log(a);
    this.chats.push(a);
  }


  tryDisplay() {
    console.log("tryDisplay"); 
      this.events.subscribe('menuesL:ready',(first, second, side) => {
         this.events.unsubscribe('menuesL:ready');
         console.log("event");
         console.log(first);
          Recommendation.recommendIt(first, null);
          Recommendation.recommendIt(second, null);
          Recommendation.recommendIt(side, null);

        this.chats = [];
         this.buildChat(first, "First course");
         this.buildChat(second, "Second course");
         this.buildChat(side, "Sides");
//         this.doCheckbox(first, second, side, "Dinner on " + d);
      });
      this.unibz.setToday(moment(this.today).format("YYYY-MM-DD"));
  }


}
