import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import { UserModel } from '../models/user-model';
import { MenueModel } from '../models/menue-model';

import { Sergelution } from '../providers/sergelution';

import { Tags } from '../interfaces/tags';

 
@Injectable()
export class Data {
  userModel: UserModel;
  userHist: MenueModel[];

  constructor(public storage: Storage, public events: Events, public sergelution: Sergelution) {
    console.log('Hello UserData Provider');
  }

  private assureId(): void {
    this.storage.get('userdata').then((userdata) => {
      console.log("assureId");
      console.log(userdata);
      if(userdata) {
        let sd = JSON.parse(userdata)[0];
        this.userModel = new UserModel(sd.name, sd.password, sd.email, sd.weight, sd.size, sd.id, sd.age, sd.activity, sd.breakfasttyp, sd.snacks, sd.gender, sd.logged, sd.iGoLunch, sd.iGoDinner, sd.mealtyp, sd.menutypL, sd.menutypD);
      } else {
        this.userModel = new UserModel();
        console.log("new usermodel");
      }
      if(this.userModel.id == -1) {
        this.events.subscribe(Tags.USERID_READY, (id) => {
          this.events.unsubscribe(Tags.USERID_READY);
          this.saveId(id);
        });
        this.sergelution.getUserId();
      } else {
        console.log("publish " + Tags.USERDATA_READY);
        this.events.publish(Tags.USERDATA_READY, this.userModel);
      }
   });
  // this.prepHist();
  // this.prepCheck();
  }

  prepData(): void {
    this.storage.get('userdata').then((userdata) => {
      console.log("prepData");
      console.log(userdata);
      if(userdata) {
        let sd = JSON.parse(userdata)[0];
        this.userModel = new UserModel(sd.name, sd.password, sd.email, sd.weight, sd.size, sd.id, sd.age, sd.activity, sd.breakfasttyp, sd.snacks, sd.gender, sd.logged, sd.iGoLunch, sd.iGoDinner, sd.mealtyp, sd.menutypL, sd.menutypD);
        console.log("publish " + Tags.USERDATA_READY);
        this.events.publish(Tags.USERDATA_READY, this.userModel);
      } else {
        this.userModel = new UserModel();
        this.events.subscribe(Tags.USERID_READY, (id) => {
          console.log(Tags.USERID_READY + id);
          this.events.unsubscribe(Tags.USERID_READY);
          this.saveId(id);
        });
        this.sergelution.getUserId();
      }
   });
  }

  prepHist(): void {
//    this.storage.remove('userhist');
//    this.storage.get('userhist').then((userhists) => {
//      this.userHist = [];
 //     if(userhists!=null)  {
 //       let sd = JSON.parse(userhists);
 //       sd.forEach((userhist) => {
 //         this.userHist.push(userhist);
 //       });
 //     }
 //     this.events.publish(Tags.USERHIST_READY, this.userHist);
 //  });
  }

  getData() : UserModel {
    return this.userModel;
  }
  getHist() : MenueModel[] {
    return this.userHist;
  }
  getCheck() : void {
  //  return this.userCheck;
  }

  compareUserModel(b: UserModel) {
    let a = this.userModel;
    if(a.activity != b.activity) return true;
    if(a.breakfasttyp != b.breakfasttyp) return true;
    if(a.age != b.age) return true;
    if(a.email != b.email) return true;
    if(a.gender != b.gender) return true;
    if(a.iGoDinner != b.iGoDinner) return true;
    if(a.iGoLunch != b.iGoLunch) return true;
    if(a.id != b.id) return true;
    if(a.logged != b.logged) return true;
    if(a.menutypL != b.menutypL) return true;
    if(a.menutypD != b.menutypD) return true;
    if(a.name != b.name) return true;
    if(a.password != b.password) return true;
    if(a.snacks != b.snacks) return true;
    return false;   
  }

  saveId(id: number) {
    this.userModel.id = id;
    let saveData = [{
      name: this.userModel.name,
      password: this.userModel.password,
      email: this.userModel.email,
      weight: this.userModel.weight,
      size: this.userModel.size,
      id: id,
      age: this.userModel.age,
      activity: this.userModel.activity,
      breakfasttyp: this.userModel.breakfasttyp,
      snacks: this.userModel.snacks,
      gender: this.userModel.gender,
      logged: this.userModel.logged,
      iGoLunch: this.userModel.iGoLunch,
      iGoDinner: this.userModel.iGoDinner,
      mealtyp: this.userModel.mealtyp,
      menutypL: this.userModel.menutypL,
      menutypD: this.userModel.menutypD,
    }];
    let newData = JSON.stringify(saveData);
    console.log(newData);
    this.storage.set('userdata', newData).then((val) => {
      this.prepPurData(val);
    });
  }

  reset()  {
    this.storage.clear();
  }

  save(data : UserModel): void {
    this.userModel = data;
    let saveData = [{
      name: data.name,
      password: data.password,
      email: data.email,
      weight: data.weight,
      size: data.size,
      id: data.id,
      age: data.age,
      activity: data.activity,
      breakfasttyp: data.breakfasttyp,
      snacks: data.snacks,
      gender: data.gender,
      logged: data.logged,
      iGoLunch: data.iGoLunch,
      iGoDinner: data.iGoDinner,
      mealtyp: data.mealtyp,
      menutypL: data.menutypL,
      menutypD: data.menutypD,
    }];
    let newData = JSON.stringify(saveData);
    this.storage.set('userdata', newData).then((val) => {
      this.prepPurData(val);
    });
  }
  prepPurData(val: any): void {
      let sd = JSON.parse(val);
      console.log("prepPurData");
      console.log(sd);
      this.userModel = new UserModel(sd[0].name, sd[0].password, sd[0].email, sd[0].weight, sd[0].size, sd[0].id, sd[0].age, sd[0].activity, sd[0].breakfasttyp, sd[0].snacks, sd[0].gender, sd[0].logged, sd[0].iGoLunch, sd[0].iGoDinner, sd[0].mealtyp, sd[0].menutypL, sd[0].menutypD);
      console.log(this.userModel);
      this.events.publish(Tags.USERDATA_READY, this.userModel);
      console.log(Tags.USERDATA_READY + " published");
   
  }

  registrationSave(data : UserModel): void {
    this.userModel = data;
    let saveData = [{
      name: data.name,
      password: data.password,
      email: data.email,
      weight: data.weight,
      size: data.size,
      id: data.id,
      age: data.age,
      activity: data.activity,
      breakfasttyp: data.breakfasttyp,
      snacks: data.snacks,
      gender: data.gender,
      logged: data.logged,
      iGoLunch: data.iGoLunch,
      iGoDinner: data.iGoDinner,
      mealtyp: data.mealtyp,
      menutypL: data.menutypL,
      menutypD: data.menutypD,
    }];
    let newData = JSON.stringify(saveData);
    this.storage.set('userdata', newData).then((val) => {
      console.log("registrationSave");
      console.log(val);
      this.prepRegistrationSave(val);
    });
  }
  prepRegistrationSave(val): void {
    this.storage.get('userdata').then((userdata) => {
      console.log("prepPurData");
      console.log(userdata);
      if(userdata) {
        let sd = JSON.parse(userdata)[0];
        console.log(sd);
        this.userModel = new UserModel(sd.name, sd.password, sd.email, sd.weight, sd.size, sd.id, sd.age, sd.activity, sd.breakfasttyp, sd.snacks, sd.gender, sd.logged, sd.iGoLunch, sd.iGoDinner, sd.mealtyp, sd.menutypL, sd.menutypD);
        console.log(this.userModel);
      } else {
        this.userModel = new UserModel();
      }
      this.events.publish(Tags.USERDATA_REG_READY, this.userModel);
      console.log(Tags.USERDATA_REG_READY + " published");
   });
  }





  saveHist() {
//    let saveData = JSON.stringify(this.userHist);
 //   this.storage.set('userhist', saveData).then((val) => {
 //     this.prepHist();
 //   });
  }

  addToHist(dat: MenueModel): void {
 //   let add: boolean = true;
//    this.userHist.forEach((menue) => {
 //     if(menue.id == dat.id) {
 //       add = false;
//      }
 //   });
 //   if(add) {
 //     this.userHist.push(dat);
 ////     this.userHist.sort( ( obj1 :MenueModel, obj2: MenueModel) => {
 //         if(obj1.datum < obj2.datum) {
  //            return -1;
 //         }
  //        if(obj1.datum > obj2.datum) {
 //             return 1;
  //        }
 //         return 0;
 //     });
 //     this.saveHist();
 //   }
  }


  isAllSet(dat: UserModel): boolean {
    let isOk = true;
    if(dat.activity == 0) isOk = false;
    if(dat.weight == 0) isOk = false;
    if(dat.size == 0) isOk = false;
    if(dat.age == 0) isOk = false;
    if(dat.breakfasttyp == 0) isOk = false;
    if(dat.gender == 0) isOk = false;

 //   if(isOk) {
 //     if(! dat.logged) {

 //     }
//
  //  }

// Check this lokally, because the user can change it in the recommendation page
//    if(dat.mealtyp == 0) isOk = false;
//    if(dat.menutypL == 0) isOk = false;
//    if(dat.menutypD == 0) isOk = false;
    return isOk;
  }
}