import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import * as moment from 'moment';

@Injectable()
export class Datum {
 
  constructor() {
    console.log('Hello Datum Provider');
  }

  getGermanDate(): string {
    return this.getGermanDatePlus(0);
  }
  getGermanDatePlus(days : number): string {
    return this.buildGermanDate(moment().add(days, 'days'));
  }
  
  buildGermanDate(akt): string {
    let dsz = akt.format("YYYY-MM-DD");
    return dsz;
  }

  getMenuDay(): string {
    return this.getMenueDayPlus(0);
  }
  getMenueDayPlus(days : number): string {
    return this.buildMenueDay(moment().add(days, 'days'));
  }
  buildMenueDay(akt): string {
    return akt.format("MMM");
  }

  getMenueDate(): string {
    return this.getMenueDatePlus(0);
  }
  getMenueDatePlus(days : number): string {
    return this.buildMenueDate(moment().add(days, 'days'));
  }
  buildMenueDate(akt): string {
    return akt.format("dddd MMM DD");
  }

}
