import {ResponseOptions, Response} from '@angular/http';

import { Events, LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MenueModel } from '../models/menue-model';
import { UserModel } from '../models/user-model';
import { Tags } from '../interfaces/tags';
import { Data } from '../providers/userdata';
import { FlurryService } from '../providers/flurryservice';

import * as moment from 'moment';

@Injectable() 
export class Unibz {
  public static readonly url:string = "http://foodrs.inf.unibz.it:8000/";
  public static readonly proxy: string = ""; // https://cors-anywhere.herokuapp.com/";

  //public static readonly urlDebugUsed:boolean = true;
  //public static readonly urlDebug:string = "http://bagtag.sergelution.de/unibz.php/";

  public static readonly UNIBZ_RECIPES: string = 'unibz:recipes';
  public static readonly UNIBZ_RECIPES_ERR: string = 'unibz:recipes:err';
  public static readonly UNIBZ_REGISTRATION: string = 'unibz:registration';
  public static readonly UNIBZ_REGISTRATION_ERR: string = 'unibz:registration:err';
  public static readonly UNIBZ_PROFILE_PUT: string = 'unibz:profile:put';
  public static readonly UNIBZ_PROFILE_PUT_ERR: string = 'unibz:profile:put:err';
  public static readonly UNIBZ_SUGESTION: string = 'unibz:sugestion';
  public static readonly UNIBZ_SUGESTION_ERR: string = 'unibz:sugestion:err';
  public static readonly UNIBZ_DAILYNEEDS: string = 'unibz:dailyneeds';
  public static readonly UNIBZ_DAILYNEEDS_ERR: string = 'unibz:dailyneeds:err';
  public static readonly UNIBZ_UPDATEUSERHISTORY: string = 'unibz:updateuserhistory';
  public static readonly UNIBZ_UPDATEUSERHISTORY_ERR: string = 'unibz:updateuserhistory:err';
  public static readonly UNIBZ_NUTRITIONAL_HISTORY: string = 'unibz:nutritionalhistory';
  public static readonly UNIBZ_NUTRITIONAL_HISTORY_ERR: string = 'unibz:nutritionalhistory:err';
  public static readonly UNIBZ_NUTRITIONAL_HISTORYWEEKLY: string = 'unibz:nutritionalhistory:weekly';
  public static readonly UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR: string = 'unibz:nutritionalhistory:weekly:err';
  public static readonly UNIBZ_EATENDISHES: string = 'unibz:eatendishes';
  public static readonly UNIBZ_EATENDISHES_ERR: string = 'unibz:eatendishes:err';


    meal: String = "L";
    user: any;
    username: String = "kiki";
    password: string = "kiki";
    authdata: string = "";
    today: string = "2017-05-01";
    firstD: MenueModel[] = [];
    secondD: MenueModel[] = [];
    sideD: MenueModel[] = [];
    firstL: MenueModel[] = [];
    secondL: MenueModel[] = [];
    sideL: MenueModel[] = [];

    loading: any;
    loaded: boolean = false;
    inLoading: boolean  = false;

  constructor(public http: Http, 
              public events: Events,
              public loadingCtrl: LoadingController,
              public flurryService: FlurryService,
              public userService: Data) { 
                events.subscribe(Tags.USERDATA_READY, (user) => {
                  events.unsubscribe(Tags.USERDATA_READY);
                  this.user = user;  
                  this.username = user.email;
                  this.password = user.password;
                  console.log(user);  
                  //userService.save(user);
                });
                userService.prepData();
              }

  getRecipes(date) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'daily-plan/?date=' + date ;
    console.log("using: " + url);
    this.inLoading = true;
    this.http.get(url).map(res => res.json()).subscribe( (data) => {
        this.inLoading = false;
        this.loaded = true;
        console.log("unibz.getRecipes()");
        console.log(data);
        this.buildMenues(data);
        this.loading.dismiss();
        this.events.publish(Tags.MENUESL_READY, this.firstL, this.secondL, this.sideL);
        this.events.publish(Tags.MENUESD_READY, this.firstD, this.secondD, this.sideD);
        this.events.publish(Tags.MENUESLD_READY,  this.firstL, this.secondL, this.sideL, this.firstD, this.secondD, this.sideD);
    }, (error) => {
        this.inLoading = false;
        console.log("unibz.getRecipes() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_RECIPES_ERR, error);
    });
  }

  private buildMenues(data: any): void {
    //if(!this.loaded) {
    //    this.getRecipes(this.today);;
    //}
    this.firstL = [];
    this.firstD = [];
    this.secondL = [];
    this.secondD = [];
    this.sideL = [];
    this.sideD = [];
    for(let i = 0, j = data.length; i < j; i++) {
        let dat = data[i];
        let model = new MenueModel(
            Number(dat.id), 
            dat.datum, 
            dat.kind, 
            dat.dish, 
            dat.name, 
            Number(dat.portion_grams), 
            Number(dat.kcal), 
            Number(dat.carbs), 
            Number(dat.fiber), 
            Number(dat.sugars), 
            Number(dat.proteins), 
            Number(dat.fats), 
            Number(dat.salt), 
            Number(dat.saturated_fatty_acids), 
            500.50,
            'green-reco',
            '0',
            'https://unibz.markas.info/images/piatti/' + dat.img);
        if(dat.dish == 'F') {
            if(dat.kind == 'L') {
                this.firstL.push(model);
            } else {
                this.firstD.push(model);
            }
        }
        if(dat.dish=='S') {
            if(dat.kind == 'L') {
                this.secondL.push(model);
            } else {
                this.secondD.push(model);
            }
        }
        if(dat.dish=='SD') {
            if(dat.kind == 'L') {
                this.sideL.push(model);
            } else {
                this.sideD.push(model);
            }
        }
    }
  }
  

  setToday(date: string) {
      console.log(this.today + " :: " + date + " / " + this.loaded);
      if(this.today==date && this.loaded) {
        this.events.publish(Tags.MENUESL_READY, this.firstL, this.secondL, this.sideL);
        this.events.publish(Tags.MENUESD_READY, this.firstD, this.secondD, this.sideD);
        this.events.publish(Tags.MENUESLD_READY,  this.firstL, this.secondL, this.sideL, this.firstD, this.secondD, this.sideD);
        return;
      }
      if(this.inLoading) {
        return;
      }
      this.loaded = false;
      this.today = date;
      this.getRecipes(this.today);
  }

//  fetchData(): void {
//    this.getRecipes(this.today);
//  }

  doRegister(user: any) {
    user.email = "v" + user.id;
    user.password = user.email;
    this.user = user;
    console.log(this.user);
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let data: any = {
      'username': this.user.email,
      'password': this.user.password,
      'age' : +this.user.age,
      'gender': UserModel.GENDERS_BZ[+this.user.gender],
      'height': +this.user.size,
      'weight': +this.user.weight,
      'laf': +this.user.activity,
      'breakfast': UserModel.BREAKFASTS_BZ[+this.user.breakfasttyp],
      'snacks': +this.user.snacks,
    };
    console.log("doRegister");
    console.log(user);
    console.log(data);

    let head = new Headers();
    head.append('Content-Type', 'application/json');

    let url = Unibz.proxy + Unibz.url + 'registration/';
    console.log("using: " + url);
    this.http.post(url, data, {headers: head}).subscribe((retdata) => {
        this.loaded = true;
        console.log("unibz.doRegister()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_REGISTRATION, data);
    }, (error) => {
        console.log("unibz.doRegister() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_REGISTRATION_ERR, error);
    });
  }

  getProfile(user) {
    this.user = user;
    this.buildBasicToken();
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'profile/' + this.username ;
    console.log("using: " + url);
//    let head = new Headers();
//    head.append( 'Authorization', ' Basic ' + this.authdata);
//    console.log(head);
//    this.http.get(url, {headers: head} ).map(res => res.json()).subscribe( (data) => {
    this.http.get(url).map(res => res.json()).subscribe( (data) => {
        this.loaded = true;
        console.log("unibz.getProfile()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish('unibz_profile_get', data);
    }, (error) => {
        console.log("unibz.getProfile() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
    });
  }

  putProfile(user: any) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.user = user;
    this.buildBasicToken();
    let data: any = {
      'username': this.user.email,
      'password': this.user.password,
      'age' : +this.user.age,
      'gender': UserModel.GENDERS_BZ[+this.user.gender],
      'height': +this.user.size,
      'weight': +this.user.weight,
      'laf': +this.user.activity,
      'breakfast': UserModel.BREAKFASTS_BZ[+this.user.breakfasttyp],
      'snacks': this.user.snacks,
    };
    console.log("putProfile");
    console.log(data);
  //  let head = new Headers();
  //  head.append( 'Authorization', ' Basic ' + this.authdata);
    let url = Unibz.proxy + Unibz.url + 'profile/' + this.username + "/";
    console.log("using: " + url);
    this.http.put(url, data).subscribe((data) => {
        this.loaded = true;
        console.log("unibz.putProfile()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_PROFILE_PUT, data);
    }, (error) => {
        console.log("unibz.putProfile() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_PROFILE_PUT_ERR, error);
    });
  }

  updateUserHistory(user, meal: String, dishes: any[], datum : string = moment().format("YYYY-MM-DD")) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.user = user;
    this.buildBasicToken();
    console.log("updateUserHistory: dishes");
    console.log(dishes);
    let data: any = {
      "dishes": dishes,
      "meal": meal, 
      "date" : datum
    }; 
    console.log(data);
  //  let head = new Headers();
  //  head.append( 'Authorization', ' Basic ' + this.authdata);
    //head.append('Content-Type', 'application/json');
    let url = Unibz.proxy + Unibz.url + 'update-user-history/' + this.username + "/";
    console.log("using: " + url);
    this.http.post(url, data).map(res => res.json()).subscribe((data) => {
        this.loaded = true;
        console.log("unibz.updateUserHistory()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_UPDATEUSERHISTORY, data);
    }, (error) => {
        console.log("unibz.updateUserHistory() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR, error);
    });
  }

  getDailyNeeds(user: any) {
    this.user = user;
    this.buildBasicToken();
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'daily-needs/' + this.username + "/" ;
    console.log("using: " + url);
//    let head = new Headers();
//    head.append( 'Authorization', ' Basic ' + this.authdata);
//    console.log(head);
    this.http.get(url).map(res => res.json()).subscribe( (data) => {
        this.loaded = true;
        console.log("unibz.getdaily-needs()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_DAILYNEEDS, data);

    }, (error) => {
        console.log("unibz.getdaily-needs() err");
        console.log("Oooopsala!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_DAILYNEEDS_ERR, error);
    });
  }

  getNutritionalHistory(user: any, day: any = "all") {
    this.user = user;
    this.buildBasicToken();
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'nutritional-history/' + this.username + "/?date=" + day;
    console.log("using: " + url);
  //  let head = new Headers();
  //  head.append( 'Authorization', ' Basic ' + this.authdata);
  //  console.log(head);
    this.http.get(url ).map(res => res.json()).subscribe( (data) => {
        this.loaded = true;
        console.log("unibz.nutritional-history()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_NUTRITIONAL_HISTORY, data);

    }, (error) => {
        console.log("unibz.getnutritional-his() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_NUTRITIONAL_HISTORY_ERR, error);
    });
  }

  getNutritionalHistoryWeek(user: any) {
    this.user = user;
    this.buildBasicToken();
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'nutritional-history/' + this.username + "/?date=all";
    console.log("using: " + url);
//    let head = new Headers();
//    head.append( 'Authorization', ' Basic ' + this.authdata);
//    console.log(head);
    this.http.get(url).map(res => res.json()).subscribe( (data) => {
      let history: any[] = [];
      let days:string[] = [];
      for(let index:number = 0;index<7;index++){
        days.push(moment().add(-index, "d").format("YYYY-MM-DD"));
      }
      data.forEach( (item) => {
        days.forEach( (day) => {
          if(item.date == day) {
            history.push(item);
          }
        });
      });
      console.log("unibz.nutritional-history()");
      console.log(data);
      this.loading.dismiss();
      this.events.publish(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY, data);

    }, (error) => {
        console.log("unibz.getdaily-needs() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_NUTRITIONAL_HISTORYWEEKLY_ERR, error);
    });
  }


   getEatenDishes(user, day: any = "all") {
    this.user = user;
    this.buildBasicToken();
    
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'eaten-dishes/' + this.username + "/?date=" + day;
    console.log("using: " + url);
//    let head = new Headers();
//    head.append( 'Authorization', ' Basic ' + this.authdata);
//    console.log(head);
    this.http.get(url).map(res => res.json()).subscribe( (data) => {
        this.loaded = true;
        console.log(Unibz.UNIBZ_EATENDISHES);
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_EATENDISHES, data);

    }, (error) => {
        console.log(Unibz.UNIBZ_EATENDISHES_ERR);
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_EATENDISHES_ERR, error);
    });
  }

  getRecommends(user, wanted: string, meal: string, other: string) {
    this.user = user;
    this.buildBasicToken();
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Unibz.proxy + Unibz.url + 'suggestion/' + this.username + "/?wanted_meal=" + wanted;
    url += "&other_meal=";  
    url += other;  
    console.log("using: " + url);
//    let head = new Headers();
//    head.append( 'Authorization', ' Basic ' + this.authdata);
    this.http.get(url).map(res => res.json()).subscribe( (data) => {
        this.loaded = true;
        console.log("unibz.getRecommends()");
        console.log(data);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_SUGESTION, data);
    }, (error) => {
        console.log("unibz.getRecommends() err");
        console.log("Oooops!");
        console.log(error);
        this.loading.dismiss();
        this.events.publish(Unibz.UNIBZ_SUGESTION_ERR, error);
    });
  }


  buildBasicToken() {
    this.username = this.user.email;
    this.password = this.user.password;
 //   this.authdata = this.encode64(this.username + ':' + this.password);
  }

  keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  encode64(input: string) {
    let output = "";
    let chr1, chr2, chr3: any = "";
    let enc1, enc2, enc3, enc4: any = "";
    let i = 0;
    do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);
      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
  
      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }
      output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);
    return output;
  }
  
  decode64(input) {
    let output = "";
    let chr1, chr2, chr3: any = "";
    let enc1, enc2, enc3, enc4:any = "";
    let i = 0;
    // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
    let base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
      window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    do {
      enc1 = this.keyStr.indexOf(input.charAt(i++));
      enc2 = this.keyStr.indexOf(input.charAt(i++));
      enc3 = this.keyStr.indexOf(input.charAt(i++));
      enc4 = this.keyStr.indexOf(input.charAt(i++));
  
      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;
  
      output = output + String.fromCharCode(chr1);
      if (enc3 != 64) {
        output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
        output = output + String.fromCharCode(chr3);
      }
  
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
  
    } while (i < input.length);
    return output;
  }
}
