import { Events, LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MenueModel } from '../models/menue-model';
import { Tags } from '../interfaces/tags';
//import { Data } from '../providers/userdata';
import { FlurryService } from '../providers/flurryservice';

import * as moment from 'moment';

@Injectable() 
export class Sergelution {
   public static readonly SURVEY_READY:string = "survey:ready";
   public static readonly SURVEY_READY_ERR:string = "survey:ready:err";
   public static readonly SHOWSURVEY_READY:string = "showsurvey:ready";
   public static readonly SHOWSURVEY_READY_ERR:string = "showsurvey:ready:err";
 
   public static readonly url:string = "http://bagtag.sergelution.de/menuerec.php/";
//    public static readonly url:string = "http://192.168.178.49/BagTag/menuerec.php/";
//    public static readonly url:string = "http://88.215.92.3/BagTag/menuerec.php/";
   
    today: string = "03.02.2017";
    loaded: any = false;
    firstD: MenueModel[] = [];
    secondD: MenueModel[] = [];
    sideD: MenueModel[] = [];
    firstL: MenueModel[] = [];
    secondL: MenueModel[] = [];
    sideL: MenueModel[] = [];

    loading: any;

  constructor(public http: Http, 
              public events: Events,
              public loadingCtrl: LoadingController,
              public flurryService: FlurryService) { }

  setToday(date: string) {
      console.log(this.today + " :: " + date + " / " + this.loaded);
      if(this.today==date && this.loaded) {
        this.events.publish(Tags.MENUESL_READY, this.firstL, this.secondL, this.sideL);
        this.events.publish(Tags.MENUESD_READY, this.firstD, this.secondD, this.sideD);
        this.events.publish(Tags.MENUESLD_READY,  this.firstL, this.secondL, this.sideL, this.firstD, this.secondD, this.sideD);
        return;
      }
      this.loaded = false;
      this.today = date;
      this.fetchData(); 
  }

  fetchPic(names: string): void {
//    this.loading = this.loadingCtrl.create();
//    this.loading.present();
    let url = Sergelution.url + 'getpic/' + names;
    console.log("using: " + url);
    this.http.get(url).map(res => res.json()).subscribe(data => {
        console.log("fetchPic");
        console.log(data);
        this.buildMenues(data);
        this.events.publish("pic:ready", data);
    });
  }


  fetchData(): void {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Sergelution.url + 'menue/' + this.today;
    console.log("using: " + url);
    this.http.get(url).map(res => res.json()).subscribe(data => {
        console.log("fetchData");
        console.log(this.today);
        console.log(data);
        this.loaded = true;
        this.buildMenues(data);
        this.loading.dismiss();
        this.events.publish(Tags.MENUESL_READY, this.firstL, this.secondL, this.sideL);
        this.events.publish(Tags.MENUESD_READY, this.firstD, this.secondD, this.sideD);
        this.events.publish(Tags.MENUESLD_READY,  this.firstL, this.secondL, this.sideL, this.firstD, this.secondD, this.sideD);
      
    });
  }

  fetchNextData(): void {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Sergelution.url + 'menue/' + this.today;
    console.log("using: " + url);
    this.http.get(url).map(res => res.json()).subscribe(data => {
        this.loaded = true;
        this.buildMenues(data);
        this.loading.dismiss();
        this.events.publish(Tags.MENUESL_READY, this.firstL, this.secondL, this.sideL);
        this.events.publish(Tags.MENUESD_READY, this.firstD, this.secondD, this.sideD);
        this.events.publish(Tags.MENUESLD_READY,  this.firstL, this.secondL, this.sideL, this.firstD, this.secondD, this.sideD);
        
    });
  }

  inGettingUserId: boolean = false;  
  getUserId(): void {
    if(this.inGettingUserId) return;
    this.inGettingUserId = true;
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Sergelution.url + 'getuserid';
    console.log("using: " + url);
    this.http.get(url).map(res => res.json()).subscribe(userId => {
        this.inGettingUserId = false;
        this.loaded = true;
        console.log("getUserId()");
        console.log(userId);
        console.log(userId.id);
        this.loading.dismiss();
        this.events.publish(Tags.USERID_READY, userId.id);
        
    });
  }

  getShowSurvey(user): void {
    console.log(user);
    console.log(user.id);
    let url = Sergelution.url + 'getShowSurveys/' + user.id;
    console.log("using: " + url);
    this.http.get(url).map(res => res.json()).subscribe(show => {
        console.log("getShowSurveys()");
        console.log(show);
        this.events.publish(Sergelution.SHOWSURVEY_READY, show);
    }, (error) => {
        console.log("getShowSurveys()");
        console.log(error);
        this.events.publish(Sergelution.SHOWSURVEY_READY_ERR, error);
    });
  }



  postSurveyData(userId, survey) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let data: any = {
            'id' : survey.surveyid,
            'frequentlyMensa' : survey.frequentlyMensa,
            'prq_1': survey.prq_1,
            'prq_2': survey.prq_2,
            'prq_3': survey.prq_3,
            'prq_4': survey.prq_4,
            'prq_5': survey.prq_5,
            'prq_6': survey.prq_6,
            'prq_7': survey.prq_7,
            'lt_SI': survey.lt_SI,
            'lt_VV': survey.lt_VV,
            'lt_AR': survey.lt_AR,
            'lt_GS': survey.lt_GS
        };
        let aktUrl = Sergelution.url + 'addSurvey';
        console.log(aktUrl);
        console.log(data);
        this.http.post(aktUrl, data).subscribe((res) => {
                        console.log("data transmittes ok!");
                        console.log(res);
                        this.loading.dismiss();
                        this.events.publish(Sergelution.SURVEY_READY, res);
                        
                    }, error => {
                        console.log("Oooops!");
                        console.log(error);
                        this.loading.dismiss();
                        this.events.publish(Sergelution.SURVEY_READY_ERR, error);
                        
                    });
  }


  postAccessDate(userId, aktMoment) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
//    let aktMoment = moment();   
    let data: any = {
            'day' : aktMoment.format("YYYY.MM.DD"),
            'time' : aktMoment.format("HH:mm"),
            'user' : userId
        };

        let aktUrl = Sergelution.url + 'addMensaUse';
        console.log(aktUrl);
        console.log(data.day);
        console.log(data.time);
        this.http.post(aktUrl, data).subscribe((res) => {
                        console.log("data transmittes ok!");
                        this.loading.dismiss();
                        this.events.publish(Tags.IGONOW_READY, aktMoment);
                        
                    }, error => {
                        this.loading.dismiss();
                        console.log("Oooops!");
                        
                    });
  }

  postUserData(user) {
    let aktMoment = moment();   
    let data: any = {
            'day' : aktMoment.format("YYYY.MM.DD"),
            'time' : aktMoment.format("HH:mm"),
            'user' : user.id,
            'name' : user.name,
            'weight' : user.weight,
            'size' : user.size,
            'age' : user.age,
            'activity' : user.activity,
            'breakfasttyp' : user.breakfasttyp,
            'gender' : user.gender
        };

        let aktUrl = Sergelution.url + 'addOrChangeUserData';
        console.log(aktUrl);
        this.http.post(aktUrl, data).subscribe((res) => {
                        console.log("data transmittes ok!");
                    }, error => {
                        console.log("Oooops!");
                    });
  }

  lastAction: any = "";
  postUserAction(user, action) {
    if(this.lastAction == action) return;
    if(user.id == -1) return;
    this.lastAction = action;
    let data: any = {
            'user' : user.id,
            'action' : action
        };
        let aktUrl = Sergelution.url + 'addUserAction';
        console.log(aktUrl + " user=" + user.id + " action=" + action);
        this.http.post(aktUrl, data).subscribe((res) => {
                        console.log("data transmittes ok!");
                    }, error => {
                        console.log("Oooops!");
                    });
    this.flurryService.logEvent(action);
   }


  callForUses(day) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let url = Sergelution.url + 'mensaUse/' + day;
    console.log("using: " + url);
    this.http.get(url).map(res => res.json()).subscribe(data => {
        this.loading.dismiss();
        this.events.publish(Tags.MENSAUSE_READY, data);
        
    });
  }

  callFor3Uses(aktMoment) {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let akt = aktMoment.subtract(7, 'days').format('YYYY.MM.DD');
    akt += "|";
    akt += aktMoment.subtract(7, 'days').format('YYYY.MM.DD');
    akt += "|";
    akt += aktMoment.subtract(7, 'days').format('YYYY.MM.DD');
    let url = Sergelution.url + 'mensaline/' + akt;
    console.log("using: " + url );
    this.http.get(url).map(res => res.json()).subscribe(data => {
        this.loading.dismiss();
        this.events.publish(Tags.MENSALINE_READY, data);
        
    });
  }




  buildMenues(data: any): void {
    if(!this.loaded) {
        this.fetchData();
    }
    this.firstL = [];
    this.firstD = [];
    this.secondL = [];
    this.secondD = [];
    this.sideL = [];
    this.sideD = [];
    for(let i = 0, j = data.length; i < j; i++) {
        let dat = data[i];
        let model = new MenueModel(
            Number(dat.id), 
            dat.datum, 
            dat.kind, 
            dat.dish, 
            dat.name, 
            Number(dat.portion_grams), 
            Number(dat.kcal), 
            Number(dat.carbs), 
            Number(dat.fiber), 
            Number(dat.sugars), 
            Number(dat.proteins), 
            Number(dat.fats), 
            Number(dat.salt), 
            Number(dat.saturated_fatty_acids), 
            500.50,
            'green-reco',
            '0',
            'https://unibz.markas.info/images/piatti/' + dat.jpg);
        if(dat.dish == 'first') {
            if(dat.kind == 'lunch') {
                this.firstL.push(model);
            } else {
                this.firstD.push(model);
            }
        }
        if(dat.dish=='second') {
            if(dat.kind == 'lunch') {
                this.secondL.push(model);
            } else {
                this.secondD.push(model);
            }
        }
        if(dat.dish=='side') {
            if(dat.kind == 'lunch') {
                this.sideL.push(model);
            } else {
                this.sideD.push(model);
            }
        }
    }
  }

  fetchFirstL(): MenueModel[] {
    return this.firstL;
  }
  fetchFirstD(): MenueModel[] {
    return this.firstD;
  }
  fetchSecondL(): MenueModel[] {
    return this.secondL;
  }
  fetchSecondD(): MenueModel[] {
    return this.secondD;
  }
  fetchSideL(): MenueModel[] {
    return this.sideL;
  }
  fetchSideD(): MenueModel[] {
    return this.sideD;
  }
}
