
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

//import { GoogleAnalytics } from 'ionic-native/dist/es5/plugins/google-analytics';

import { Tags } from '../interfaces/tags';

//import { GoogleAnalytics } from 'ionic-native';


@Injectable()
export class FlurryService {

  flurryAnalytics;

/*
      "initialize",
            "logEvent",
            "endTimedEvent",
            "logPageView",
            "logError",
            "setLocation"

"version"
"continueSessionSeconds"
"userId"
"gender"
"age"
"logLevel"
"enableEventLogging")) {

*/

  initialized: boolean = false;

  constructor(public platform: Platform) {
    console.log('Hello FlurryService');
  }

 
  init(userId:string): void {
    /*  
    GoogleAnalytics.startTrackerWithId('UA-96878337-1').then(() => {
      console.log('Google analytics is ready now');
      GoogleAnalytics.setUserId(userId).then(() => {
         console.log('UserId set: ' + userId);
          GoogleAnalytics.setAppVersion("0.1.1").then(() => {
             console.log('Version set: ' + "0.1.1");
             this.initialized = true;
          });
      });
      // Tracker is ready
      // You can now track pages or set additional information such as AppVersion or UserId
    }).catch(e => console.log('Error starting GoogleAnalytics', e));
    */
  }

  trackView(view: string) {
    /*
    if(this.initialized) {
      GoogleAnalytics.trackView(view).then(() => {
        console.log("calling: trackView");
      });
    }
    */
  }

  logEvent(event: string) {
    /*
    if(this.initialized) {
      GoogleAnalytics.trackView(event).then(() => {
        console.log("calling: trackView: " + event);
      });
      */
    }

/*
      GoogleAnalytics.trackMetric("userAction", event)
      .then(
        () => {
        console.log("userAction: this.flurryAnalytics.logEvent ok") 
      ,
      (err) => {
        console.log("userAction: this.flurryAnalytics.logEvent " + err); 
      };
    });

}
*/  
/*    
    let options = {
      continueSessionSeconds: 3,          // how long can the app be paused before a new session is created, must be less than or equal to five for Android devices
      userId: 'Donald Duck',
      gender: 'm',                        // valid values are "m", "M", "f" and "F"
      age: 27,
      logLevel: 'DEBUG',                  // (VERBOSE, DEBUG, INFO, WARN, ERROR)
      enableLogging: true,                // defaults to false
      enableEventLogging: true,          // should every event show up the app's log, defaults to true
      enableCrashReporting: true,         // should app crashes be recorded in flurry, defaults to false, iOS only
      enableBackgroundSessions: true,     // should the session continue when the app is the background, defaults to false, iOS only
      reportSessionsOnClose: true,       // should data be pushed to flurry when the app closes, defaults to true, iOS only
      reportSessionsOnPause: true        // should data be pushed to flurry when the app is paused, defaults to true, iOS only
    };
   // workaround - create a blank flurryAnalytics for development site since FlurryAnalytics will be available on devices only 
    if (this.platform.is('cordova')) {
      this.flurryAnalytics = (<any>window).plugins.FlurryAnalyticsPlugin;
      console.log(this.flurryAnalytics);
    } else {
      this.flurryAnalytics = {
        execute: function(action: string, args:any[], cb, error) {
            console.log(action );
            console.log(args );       
        }
        
      }
    } //Tracking-ID Google   UA-96878337-1
    this.flurryAnalytics.execute("initialize", "[{'DTSK9FBX342TC56N234B'}," + options + "]",
        function() {
            console.log("ok");
        },
        function(err) {
            console.log("error: " + err);
        }
    );
  }
*/


}