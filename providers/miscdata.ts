import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class MiscData {
  data: any;
  lastTab: number = 0;
  lastChat: any;
  lastRunner: any;

  hadLunch: string = "";
  hadDinner: string = "";

  recomDay: string;
  recomToast: boolean = true;
  waitingAlert: boolean = true;
  settingToast: boolean = true;
  public hourMinutes: any[] = [];

  public static longWait: number = 2000;
  public static superlongWait: number = 3500;
  public static shortWait: number = 1000;

  constructor(
    public storage: Storage,
    ) {
      //this.getMiscData();
      console.log('Hello MiscData Provider');
      this.buildHourMinutes();
  }

 buildHourMinutes() {
    let inter = {hour: 11, minute: 46}; this.hourMinutes.push(inter);
    inter = {hour: 12, minute: 1}; this.hourMinutes.push(inter);
    inter = {hour: 12, minute: 16}; this.hourMinutes.push(inter);
    inter = {hour: 12, minute: 31}; this.hourMinutes.push(inter);
    inter = {hour: 12, minute: 46}; this.hourMinutes.push(inter);
    inter = {hour: 13, minute: 1}; this.hourMinutes.push(inter);
    inter = {hour: 13, minute: 16}; this.hourMinutes.push(inter);
    inter = {hour: 13, minute: 31}; this.hourMinutes.push(inter);
    inter = {hour: 13, minute: 46}; this.hourMinutes.push(inter);
    inter = {hour: 18, minute: 31}; this.hourMinutes.push(inter);
    inter = {hour: 19, minute: 1}; this.hourMinutes.push(inter);
    inter = {hour: 19, minute: 31}; this.hourMinutes.push(inter);
    inter = {hour: 20, minute: 1}; this.hourMinutes.push(inter);
  }
  getHourMinutes(): any[] {
    return this.hourMinutes;
  }

  getLastTab(): number {
    return this.lastTab;
  }

  setLastTab(tab: number): void {
    this.lastTab = tab;
  }

  getActChat(): number {
    return this.lastChat;
  }

  setLastChat(chat: any): void {
    this.lastChat = chat;
  }

  setLastRunner(runner: any) {
    this.lastRunner = runner;
  }
  getLastRunner() : any {
    return this.lastRunner;
  }


  setHadLunch(l) {
    console.log("setHadLunch ");
    console.log(l);
    this.hadLunch = l;
  }
  setHadDinner(l) {
    this.hadDinner = l;
  }
  getHadLunch(): any {
    return this.hadLunch;
  }
  getHadDinner() {
    return this.hadDinner;
  }

  setRecomDay(recom: any) {
    this.recomDay = recom;
  }
  getRecomDay() : any {
    return this.recomDay;
  }

  setRecomToast(toast: boolean) {
    this.recomToast = toast;
    this.saveMiscData();
  }
  getRecomToast() : boolean {
    return this.recomToast;
  }
  setSettingToast(toast: boolean) {
    this.settingToast = toast;
  }
  getSettingToast() : boolean {
    return this.settingToast;
  }
  setWaitingAlert(alert: boolean) {
    this.waitingAlert = alert;
    this.saveMiscData();
  }
  getWaitingAlert() : boolean {
    return this.waitingAlert;
  }
  getMiscData() {
    this.storage.get('miscdata').then((miscdata) => {
      console.log("getMiscData");
      console.log(miscdata);
      if(miscdata) {
        let sd = JSON.parse(miscdata)[0];
        this.recomToast = sd.recomToast;
        this.waitingAlert = sd.waitingAlert;
      } else {
        this.recomToast = true;
        this.waitingAlert = true;
      }
    });
  }
  saveMiscData(): void {
    let saveData = [{
      recomToast: this.recomToast,
      waitingAlert: this.waitingAlert,
    }];
    let newData = JSON.stringify(saveData);
    this.storage.set('miscdata', newData).then((val) => {
      console.log('miscData stored: ' + val);
    });
  }

}