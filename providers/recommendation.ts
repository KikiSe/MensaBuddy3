import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MenueModel } from '../models/menue-model';
import { UserModel } from '../models/user-model';

@Injectable() 
export class Recommendation {

  constructor(public http: Http) {
  }

  public static recommendIt(menues: MenueModel[], user: UserModel): MenueModel[] {
    // naive Implementation!
    //Can be as a inplace implementation or by calling a service!
    // Wenn calling a service, we had to use the ReadyService
    for(let menue of menues) {
      let kcalPro100: number = menue.kcal/menue.portion_grams;
      menue.rank = kcalPro100 * 100;
      if(menue.rank < 200) {
        menue.rec_class = 'green-reco';
        menue.rec_color = '0';
      } else if(menue.rank > 300) {
        menue.rec_class = 'red-reco';
        menue.rec_color = '2';
      } else {
        menue.rec_class = 'yello-reco';
        menue.rec_color = '1';
      }
    }
    menues.sort( ( obj1 :MenueModel, obj2: MenueModel) => {
      if(obj1.rank <obj2.rank) {
        return -1;
      }
      if(obj1.rank > obj2.rank) {
        return 1;
      }
      return 0;
    });
    return menues;
  }

}
