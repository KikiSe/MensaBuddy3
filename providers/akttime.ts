import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import * as moment from 'moment';

import { Events } from 'ionic-angular';

import { Tags } from '../interfaces/tags';

import { Unibz } from '../providers/unibz';
import { Sergelution } from '../providers/sergelution';

@Injectable()
export class TimeTicker {
//  public static readonly STARTLUNCH = moment().set('hour', 11).set('minute', 45);
//  public static readonly STOPLUNCH = moment().set('hour', 14).set('minute', 0);
  public static readonly STARTLUNCH = moment().set('hour', 11).set('minute', 45);
  public static readonly STOPLUNCH = moment().set('hour', 14).set('minute', 0);   // for development only!!
  public static readonly STARTDINNER = moment().set('hour', 18).set('minute', 30);
  public static readonly STOPDINNER = moment().set('hour', 20).set('minute', 30);
  public static readonly SASTARTLUNCH = moment().set('hour', 11).set('minute', 45);
  public static readonly SASTOPLUNCH = moment().set('hour', 14).set('minute', 0);
  
  mensaOpen: boolean = false;
  dayMensaOpen: boolean = false;

  constructor(public events: Events, public sergelution: Sergelution, public unibz: Unibz) {
    console.log('Hello TimeTicker Provider');
    //this.timeout();
    this.timeoutMin();
  }


  timeout() {
    setTimeout(() =>  {
      let akt = moment();
      this.events.publish('aktTime:sec',akt.format("HH:mm:ss"));
      this.timeout();
    }, 1000);
  }
  
  timeoutMin() {
    setTimeout(() => {
        let akt = moment();
        this.events.publish('aktTime:min',akt.format("HH:mm"));
        this.timeoutMin();
    }, 60000);
  }

  checkDayMensaOpen(moment) {
    console.log("checkDayMensaOpen");
      this.events.subscribe(Tags.MENUESL_READY, (first, second, side) => {
        this.events.unsubscribe(Tags.MENUESL_READY);
        console.log("unsubscribe: " + Tags.MENUESL_READY);
        if(first.length == 0) {
          this.dayMensaOpen = false;
        } else {
          this.dayMensaOpen = true;
        }
        this.events.publish(Tags.MENSAOPEN_DAY, this.dayMensaOpen);
        console.log("publish: " + Tags.MENSAOPEN_DAY);      
      });
      this.unibz.setToday(moment.format("YYYY-MM-DD"));
  }

  checkMensaOpen() {
      this.events.subscribe(Tags.MENUESL_READY, (first, second, side) => {
        this.events.unsubscribe(Tags.MENUESL_READY);
        if(first.length == 0) {
          this.mensaOpen = false;
        } else {
          let dinner = this.sergelution.fetchFirstD();
          console.log("lunch/dinner");
          console.log(first);
          console.log(dinner);

          let akt = moment();
          if(dinner.length > 0) {
            if(akt.isBetween(TimeTicker.STARTLUNCH, TimeTicker.STOPLUNCH, 'minute','[]')) {
              this.mensaOpen = true;
            } else if (akt.isBetween(TimeTicker.STARTDINNER, TimeTicker.STOPDINNER, 'minute','[]')) {
              this.mensaOpen = true;
            } else {
              this.mensaOpen = false;
            }
          } else {
            if(akt.isBetween(TimeTicker.SASTARTLUNCH, TimeTicker.SASTOPLUNCH, 'minute','[]')) {
             this.mensaOpen = true;
            } else {
             this.mensaOpen = false;
            }
          }
        }   
        console.log(this.mensaOpen);
        this.events.publish(Tags.MENSAOPEN_TIME, this.mensaOpen);
      });
      this.unibz.setToday(moment().format("YYYY-MM-DD"));
  }

  checkHasDinner(moment) {
      this.events.subscribe(Tags.MENUESD_READY, (first, second, side) => {
        this.events.unsubscribe(Tags.MENUESD_READY);
        if(first.length == 0) {
          this.events.publish(Tags.MENSAOPEN_DINNER, false);
        } else {
          this.events.publish(Tags.MENSAOPEN_DINNER, true);
        }
      });
      this.unibz.setToday(moment.format("YYYY-MM-DD"));
  }
  hasDinner(moment: any) {
    this.checkHasDinner(moment);
  }

  isLunchTime(akt: any): boolean {
    if(akt.isBetween(TimeTicker.STARTLUNCH, TimeTicker.STOPLUNCH, 'minute','[]')) {
      return true;
    }
    return false;
  }
  isDinnerTime(akt: any): boolean {
    if(akt.isBetween(TimeTicker.STARTDINNER, TimeTicker.STOPDINNER, 'minute','[]')) {
      return true;
    }
    return false;
  }
  isAfterLunchTime(akt: any): boolean {
    if(akt.isAfter(TimeTicker.STOPLUNCH, 'minute','[]')) {
      return true;
    }
    return false;
  }
  isAfterDinnerTime(akt: any): boolean {
    if(akt.isAfter(TimeTicker.STOPDINNER, 'minute','[]')) {
      return true;
    }
    return false;
  }

  isDayMensaOpen(moment: any) {
    this.checkDayMensaOpen(moment);
  }

  isMensaOpen() {
    this.checkMensaOpen();
  }

}
