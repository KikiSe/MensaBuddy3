import { Injectable } from '@angular/core';

import { ChatModel } from '../models/chat-model';

@Injectable()
export class Chat { 
  public static readonly longWait: number = 2000;
  public static readonly mediumWait: number = 1500;
  public static readonly shortWait: number = 1000;
  public static readonly veryShortWait: number = 300;

  chats: ChatModel[] = [];
  threeButtons: boolean = false;
  yesNoButtons: boolean = false;
  datePicker: boolean = false;
  timePicker: boolean = false;
  multiPicker: boolean = false;

  inputField: boolean = false;
  iFLabel: string = "";
  iFPlaceholder: string = "";
  iFType: string = "";
  iFValue: string = "";
  skipButtonText: string = "";
  skipButton: boolean = false;

  inChat : boolean = true;

  aktStep: string = "*";
  debug: boolean = false;

  yesButton: string;
  noButton: string;
  yesRunState: number;
  noRunState: number;

  constructor() {
//    console.log('Hello chats Provider');
  }

   getChatsPur(): ChatModel[] {
    return this.chats;
   }

   getChats(three:boolean, yn: boolean): ChatModel[] {
    this.threeButtons = three;
    this.yesNoButtons = yn;
    return this.chats;
  }
  setDatePicker(hide: boolean) {
    this.datePicker = hide;
  }
  setTimePicker(hide: boolean) {
    this.timePicker = hide;
  }
  set3Picker(hide: boolean) {
    this.multiPicker = hide;
  }
  removeLastChats(count:number): void {
    for(let i=0;i<count;i++) {
      this.chats.pop();
    }
  }
  removeLastChat(): void {
    this.chats.pop();
  }

  clearChat(): void {
    this.chats = [];
    this.threeButtons = false;
    this.yesNoButtons = false;
    this.datePicker = false;
    this.timePicker = false;
    this.setInputField(false);
  }
  changeChat(chat: ChatModel) {
    this.removeLastChat();
    this.addChat(chat);
  }

  addChat(chat: ChatModel): void {
    this.yesNoButtons = false;
    this.threeButtons = false;    
    this.chats.push(chat);
  }

  addFirstChat(chat: ChatModel): void {
    this.clearChat();
    this.addChat(chat);
  }

  setInputField(enabled: boolean = false, value: any = "", placeholder: string = "", type:string = "submit", skip:boolean = false, skipText:string = "Skip") {
    this.iFValue = value;
    this.inputField = enabled;
//    this.iFLabel = label;
    this.iFPlaceholder = placeholder;
    this.iFType = type;
    this.skipButton = skip;
    this.skipButtonText = skipText;

  }

//right
  user(msg: string) {
    return new ChatModel(msg,  ChatModel.USER)    
  }
 //left in window 
  bot(msg: string) {
    return new ChatModel(msg,  ChatModel.BOT)    
  }

  //Msg + Icons
  userM(msg: any, head: string) {
    return new ChatModel(msg,  ChatModel.USER, ChatModel.MULTI, head)    
  }
  botM(msg: any, head: string) {
    return new ChatModel(msg,  ChatModel.BOT, ChatModel.MULTI, head);    
  }

  //Graphics
  botC(msg: any, head: string, chartType: any, dataset: any, labels: any) {
    return new ChatModel(msg,  ChatModel.BOT, ChatModel.CANVAS, head, chartType, dataset, labels);    
  }
  botCC(msg: any, head: string, chartType: any, dataset: any, labels: any, colors:any, options: any) {
    return new ChatModel(msg,  ChatModel.BOT, ChatModel.CANVAS, head, chartType, dataset, labels, colors, options);    
  }

  // Multiline with different fontsizes
  botD(msg: any, head: string) {
    return new ChatModel(msg,  ChatModel.BOT, ChatModel.MENUE, head);    
  }
  userD(msg: any, head: string) {
    return new ChatModel(msg,  ChatModel.USER, ChatModel.MENUE, head);    
  }

}