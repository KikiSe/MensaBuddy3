


export class SurveyModel {

 
  //public static SURVEYGENDERS: string[] = ["","female", "male"];
  //public static JOBS: string[] = ["","Student", "Worker", "Both"];
  public static FREQUENTLYMENSA: string[] = ["","Every Day", "No more than 3 days/week","Less than 1 day/week"];
 // public static MEALATMENSA: string[] = ["","Lunch","Dinner","Both"];
  public static PRQ1: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];
  public static PRQ2: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];
  public static PRQ3: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];
  public static PRQ4: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];
  public static PRQ5: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];
  public static PRQ6: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];
  public static PRQ7: string[] = ["","Disagree strongly","Disagree","Neutral","Agree","Agree strongly"];

  public static LSQ1: string[] = ["","try things out.","think about how I'm going to do it."]; //AR
  public static LSQ2: string[] = ["","lay out the material in clear sequential steps. ","give me an overall picture and relate the material to other subjects"]; //SG
  public static LSQ3: string[] = ["","concrete material (facts, data)."," abstract material (concepts, theories)."]; //SI
  public static LSQ4: string[] = ["","pictures, diagrams, graphs, or maps. "," written directions or verbal information. "]; //VV
  public static LSQ5: string[] = ["","a","b"];
  public static LSQ6: string[] = ["","a","b"];
  public static LSQ7: string[] = ["","a","b"];
  public static LSQ8: string[] = ["","a","b"];
  
  //public static PERSORECO: string[] = ["","Taste","A particular diet","Physical characteristics (e.g height, weight, age)","Physical activity(calorie consumption"];
  //public static KNOWCHATBOT: string[] = ["","Yes","No"];
  //public static USAGECHATBOT: string[] = ["","Yes","No"];
  //public static PREFEREDUI: string[] = ['Chatting', 'Standard', 'Combination'];
  //public static ASKRECOS: string[] = ["","No","Yes, sometimes", "Yes, for most of the meals"];
  //public static ATERECOS: string[] = ["","No","Yes, sometimes", "Yes for most of the items"];
  //public static OPINIONGENERALRECO: string[] = ["","They are useful","They are useless"];
  //public static OPINIONBUDDYRECO: string[] = ["","They are useful","They are useless"];
  //public static MENSATIMEL: string[] = ["","12","12:15","12:30","12:45","13:00","13:15","13:30","13:45"];
  //public static MENSATIMETYPL: string = "mensatimetypL";

    constructor(public surveyid: string = '',
                public surveygender: string = '',
                public surveyage: string = '', 
                public frequentlyMensa: number = 0,

                public prq_1: number = 0,
                public prq_2: number = 0,
                public prq_3: number = 0,
                public prq_4: number = 0,  
                public prq_5: number = 0,
                public prq_6: number = 0,
                public prq_7: number = 0,

                public lt_SI: number = 0, // 1 = Sensing 2 = Intuitive
                public lt_VV: number = 0, // 1 = Visual, 2 = Verbal
                public lt_AR: number = 0, // 1 = Active, 2 = Reflective
                public lt_GS: number = 0, // 1 = Sequential , 2 = global 
                  
                
                 ) {
                   
                }

}
