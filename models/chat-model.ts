export class ChatModel {
    public static readonly BOT: boolean = true;
    public static readonly USER: boolean = false;
    public static readonly SINGLE: number = 0;
    public static readonly MULTI: number = 1;
    public static readonly CANVAS: number = 2;
    public static readonly MENUE: number = 3;

    constructor(public text: any,
                public bot: boolean,
                public multi: number = ChatModel.SINGLE,
                public head: String = "",
                public chartType: any = null,
                public dataset: any = null,
                public labels: any = null,
                public colors: any = [{ // white
                    backgroundColor: '#fff',
                    borderColor: '#fff',
                    pointBackgroundColor: '#fff',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#fff'
                 },{
                    backgroundColor: '#ddd',
                    borderColor: '#ddd',
                    pointBackgroundColor: '#ddd',
                    pointBorderColor: '#ddd',
                    pointHoverBackgroundColor: '#ddd',
                    pointHoverBorderColor: '#ddd'
                 }],
                 public options: any = { 
                            legend: {labels:{fontColor:"white", fontSize: 18}},
                            scales: {
                                yAxes: [{
                                    ticks: {
                                         fontColor: "white",
                                   //     fontSize: 18,
                                   //     stepSize: 1,
                                   //     beginAtZero:true
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        fontColor: "white",
                                        fontSize: 14,
                                        stepSize: 1,
                                        beginAtZero:true
                                    }
                                }]
                            }
                    })
                  { }
 
}
