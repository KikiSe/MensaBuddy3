

export class UserModel {

  public static GENDERFEMALE: number = 1;
  public static GENDERMALE: number = 2;
  public static GENDERS: string[] = ["","female", "male"];
  public static GENDERS_BZ: string[] = ["","F", "M"];
  
  public static ACTIVITIES: string[] = ["Choose Activity", "low","medium-low", "medium-high","high"];
  public static ACTIVITY: string = "aktivity";
  public static BREAKFASTS: string[] = ["Choose Breakfast","Light: Brioche + Cappuccino","Average: porridge with fruit","Big: Ham & Cheese Sandwich"];
  public static BREAKFASTS_BZ: string[] = ["","L","AV","AB"];
  public static BREAKFASTTYP: string = "breakfasttyp";
  public static MEAL: string[] = ["","Primo","Secondo","Full"];
  public static MEALTYP: string = "mealtyp";

  public static MENUL: string[] = ["","Light lunch","Average lunch","Abudant lunch"];
  public static MENUTYPL: string = "menutypL";
  public static MENUD: string[] = ["","Light dinner","Average dinner","Abudant dinner"];
  public static MENUTYPD: string = "menutypD";
  public static MENU_BZ: string[] = ["","L","AV","AB"];




  public static MENSATIMELD: string[] = ['11:45-12:00', '12:00-12:15', '12:15-12:30', '12:30-12:45', '12:45-13:00', '13:00-13:15', '13:15-13:30', '13:30-13:45', '13:45-14:00', '18:30-19:00', '19:00-19:30', '19:30-20:00', '20:00-20:30'];
  public static MENSATIMETYPLD: string = "mensatimetypLD";
  public static MENSATIMEDSTART: number = 9;
  //public static MENSATIMEL: string[] = ["","12","12:15","12:30","12:45","13:00","13:15","13:30","13:45"];
  //public static MENSATIMETYPL: string = "mensatimetypL";

    constructor(public name: string = '', 
                public password: string = '',
                public email: string = '',
                public weight: number = null,
                public size: number = null,
                public id: number = -1,
                public age: number = null,
                public activity: number = 0,
                public breakfasttyp: number = 0,  
                public snacks: number = 0,  
                public gender: number = 0,
                public logged: boolean = false,
                public iGoLunch: any  = null,
                public iGoDinner: any = null,
                public mealtyp: number = 0,
                public menutypL: number = 0,
                public menutypD: number = 0
                 ) {
    }

    loggedIn(name: string, email: string, password: string, loggedIn: boolean): void {
        this.name = name;
        this.email = email;
        this.password = password;
        this.logged = loggedIn;
    } 
}
