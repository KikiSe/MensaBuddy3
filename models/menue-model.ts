export class MenueModel {
    //szpreis : string;

    constructor(public id: number, 
                public datum: string,
                public kind: string,
                public dish: string,
                public name: string,
                public portion_grams: number,
                public kcal: number,
                public carbs: number,
                public fiber: number,
                public sugars: number,
                public proteins: number,
                public fats: number,
                public salt: number,
                public saturated_fatty_acids: number,
                public rank: number,
                public rec_class: string,
                public rec_color: string,
                public img: string
                 ) {
                     //this.szpreis = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(price/100);
    }

}