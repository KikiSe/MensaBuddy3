import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class SwitchService {
  // Observable string sources
  private switchSource = new Subject<string>();

  // Observable string streams
  switchSource$ = this.switchSource.asObservable();

  // Service message commands
  switchTo(to: string) {
    console.log("switchservice: switchTo " + to);
    this.switchSource.next(to);
  }

/*
  backTo(to: string) {
    console.log("switchservice: backTo " + to);
    this.switchSource.next(to);
  }
*/
}
  