import { Injectable, Component } from '@angular/core';
import 'rxjs/add/operator/map';
     
import { ModalController, Events } from 'ionic-angular';

import { MiscData } from '../providers/miscdata';
import { Data } from '../providers/userdata';
import { Sergelution } from '../providers/sergelution';
import { Unibz } from '../providers/unibz';
import { TimeTicker } from '../providers/akttime';
import { ModalMenue } from '../pages/modalmenue/modalmenue';
 
import { Datum } from '../providers/datum';
import { Runner } from '../interfaces/runner';
import { Tags } from '../interfaces/tags';

import * as moment from 'moment';

  interface fn {
    (param: any): void;
  }

@Injectable()
export class Ate2Runner implements Runner {
  private static readonly ISLUNCH = 1;  
  private static readonly ISDINNER = 2;  
  private static readonly ISBOTH = 3;  
  
  data: any;
  user: any;
  chat: any;
  caller: any;
  kind: number = 0;
  aktDayHas: number = 0; // 1 == Lunch, 2 == Dinner, 3 == beides
  aktDayWass: number = 0; // 1 == Lunch, 2 == Dinner, 3 == beides


// this is to get rid of the if/if/if and calling the functions via an array
// the drawback is the 'this', that must be explicitly delivered to each function!
  yesFunctions: fn[] = [
    this.cont0Step0,
    this.cont0Step1,
    this.cont0Step2,
    this.cont0Step3,
    this.cont0Step4,
    this.cont0Step5,
    this.cont0Step6,
    this.cont0Step7,
    this.cont0Step8,
    this.cont0Step9,
    this.cont0Step10,
    this.cont0Step11,

  ];
  noFunctions: fn[] = [
    this.cont1Step0,
    this.cont1Step1,
    this.cont1Step2,
    this.cont1Step3,
    this.cont1Step4,
    this.cont1Step5,
    this.cont1Step6,
    this.cont1Step7,
    this.cont1Step8,
  ];
  constructor(    
        public datum: Datum,
        public events: Events,
        public modalCtrl: ModalController,
        public misc: MiscData,
        public userData: Data,
        public sergelution: Sergelution,
        public unibz: Unibz,
        public tt: TimeTicker,
        ) {
          //console.log("Ate2Runner: " + sergelution);
          //console.log(sergelution);
          //console.log(this.sergelution);
         }
 
  // init rhe runner. provide the callback this and the used chat-instance
  init(callerThis, chat): void {
    this.caller = callerThis;
    this.chat = chat;
  }

  cont1() {
    this.noFunctions[this.chat.noRunState](this);
  }
  cont0() {
    //console.log("Ate2Runner:cont0 " + this.sergelution);
    this.yesFunctions[this.chat.yesRunState](this);
  }
  setStates(yes, yesS, no, noS) {
    //console.log("setStates");
    //console.log(this);
    //console.log(yes + " / " + no);
    this.chat.yesButton = yes;
    this.chat.yesRunState = yesS;
    this.chat.noButton = no;
    this.chat.noRunState = noS;
  }
  setYesButton(yes: string) {
    this.chat.yesButton = yes;
  }
  // Enter from homechat chat-selection
  cont0Step0(his: any): void {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s0]);
    his.chat.aktStep = "cont0Step0";
    his.aktDayHas = 0;
    his.aktDayWas = 0;
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
       if(!his.caller.user.logged){
         his.chat.addChat(his.chat.bot("Can't log something to your history because you haven't complete your profile!"));
         his.setStates("Okay", 9, "Thanks Bye", 7);
          his.caller.getAllChats(false,true);
    }else{
      console.log("logged user? " + his.caller.user.logged)
      his.chat.changeChat(his.chat.bot("Okay"));
      let aktMoment = moment(his.caller.today);
      if((aktMoment.format("HH:mm") < '11:45')) {
        //console.log(aktMoment.format("HH:mm"));
        let interDay = aktMoment.isoWeekday();
        //console.log(aktMoment.isoWeekday());
        //console.log(interDay);
        if(interDay === 7) {
          his.chat.addChat(his.chat.bot("Sorry, you can't log yesterday. It was a sunday."));
          his.setStates("", -1, "Another day", 1)
        } else {
          his.chat.addChat(his.chat.bot("Is it yesterday or another day you want to log?"));
          his.setStates("Yesterday", 1, "Another day", 1)
      }
      } else {
        his.chat.addChat(his.chat.bot("Is it today or another day you want to log?"));
        his.setStates("Today", 1, "Another day", 1)
      }
      his.caller.getAllChats(false, true);
      }
    }, MiscData.shortWait);
  }

  // Today from cont0Step0 was selected
  cont0Step2(his: any) {
 
    his.chat.aktStep = "cont0Step2";
  
  }

  // from DatePicker with day set! 
  cont0Step1(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s1]);
    his.chat.aktStep = "cont0Step1";
    his.chat.addChat(his.chat.user(moment(his.caller.today).format("ddd MMM DD, YYYY")));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.events.subscribe(Tags.MENSAOPEN_DAY, (isOpen) => {
      his.events.unsubscribe(Tags.MENSAOPEN_DAY);
      if(!isOpen){
        his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s12]);
        his.chat.changeChat(his.chat.bot("Hey! It's impossible. On " + moment(his.caller.today).format("dddd MMM DD")+ " the Mensa wasn't open."));
        his.chat.addChat(his.chat.bot("Wanna quit or choose another day?"));
        his.setStates("Quit", 9, "Another Day", 1);
        his.caller.getAllChats(false,true);
        return;
      }
    his.events.subscribe(Tags.MENSAOPEN_DINNER, (hasDinner) => {
      his.events.unsubscribe(Tags.MENSAOPEN_DINNER);
      if(hasDinner) {
        his.aktDayHas = Ate2Runner.ISBOTH;
      } else {
        his.aktDayHas = Ate2Runner.ISLUNCH;
      }
      setTimeout(() => { 
      // check if its today!
      //console.log(moment().format() + " / "+ moment(his.caller.today).format());
      if(moment().isSame(moment(his.caller.today), 'day')) {
        if(moment(his.caller.theDay).isBefore(moment().set({'hour': 11, 'minute': 45}))) {
          his.chat.changeChat(his.chat.bot("Hey Buddy, the Mensa wasn't open yet."));
          his.chat.addChat(his.chat.bot("Wanna quit or chose another day?"));
            his.setStates("Quit", 9, "Another day", 1)
            his.caller.getAllChats(false, true);
            return;
        } 
        if(moment(his.caller.theDay).isBefore(moment().set({'hour': 18, 'minute': 35}))) {
          his.chat.changeChat(his.chat.bot("Please select your Lunch."));
          his.aktDayHas = Ate2Runner.ISLUNCH;
          his.setStates("Lunch", 4, "", -1)
          his.caller.getAllChats(false, true);
          return;
        } 
        his.chat.changeChat(his.chat.bot("Please select your Lunch or Dinner."));
        his.setStates("Lunch", 4, "Dinner", 4)
        his.caller.getAllChats(false, true);
        return;
      }
      //console.log(his.events);
      his.events.subscribe(Tags.MENSAOPEN_DINNER, (dinner) => {
        his.events.unsubscribe(Tags.MENSAOPEN_DINNER);
        if(!dinner) {
          his.chat.changeChat(his.chat.bot("Please select your Lunch."));
          his.setStates("Lunch", 4, "", -1)
        } else {
          his.chat.changeChat(his.chat.bot("Please select your Lunch or Dinner."));
          his.setStates("Lunch", 4, "Dinner", 4)
        }
        his.caller.getAllChats(false, true);
      });
      his.tt.checkHasDinner(moment());
    }, MiscData.shortWait);
    });
    his.tt.checkHasDinner(moment(his.caller.today));
  });
   his.tt.checkDayMensaOpen(moment(his.caller.today)); 

  }
  cont0Step3(his: any) {
    his.chat.aktStep = "cont0Step3";
 
  }

  // Selection for Lunch from cont0Step3
  cont0Step4(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s4]);
    his.chat.aktStep = "cont0Step4";
    his.kind = Ate2Runner.ISLUNCH;
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
      his.chat.removeLastChat();
      let d = moment(his.caller.today).format("ddd MMM DD, YYYY");
      his.events.subscribe(Tags.MENUESL_READY,(first, second, side) => {
         his.events.unsubscribe(Tags.MENUESL_READY);
         his.setStates("", 5, "", 5);
         his.doCheckbox(first, second, side, "Lunch on " + d);
      });
      his.unibz.setToday(moment(his.caller.today).format("YYYY-MM-DD"));
    }, MiscData.shortWait);
  }

  // continue after Lunch/Dinner -selection via Checkbox
  cont0Step5(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s5]);
    his.chat.aktStep = "cont0Step5";
    let sb: any[] = [];
    let textA: any[] = [];
//    console.log(his.data);
    let kind = "L"; 
    let art = "My Lunch";
    if(his.kind == Ate2Runner.ISDINNER) {
      art = "My Dinner";
      kind = "D";
      his.aktDayWas += Ate2Runner.ISDINNER;
    } else {
      his.aktDayWas += Ate2Runner.ISLUNCH;
    }
    if(his.data.length > 0){
      for( let dishes of his.data) {
        let textB = {t: dishes.name, rclass: "", c:"-2" };
        textA.push(textB);
        console.log("MENUE "+ textB);
      }
    }
    his.chat.addChat(his.chat.userD(textA, art + " was:"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
        his.chat.changeChat(his.chat.bot("Oh that is one of my favourites!"));
        if(his.data.length > 0){
          for( let dishes of his.data) {
            let it = dishes.name; 
            sb.push(it);
            console.log("MENUE "+ sb);
          }
          his.unibz.updateUserHistory(his.caller.user, kind, sb, moment(his.caller.today).format("YYYY-MM-DD"));
        }
        his.chat.addChat(his.chat.bot("I add it to your history.."));

        if(his.aktDayHas != his.aktDayWas) {
          if(his.aktDayHas - his.aktDayWas == Ate2Runner.ISDINNER) {
            his.chat.addChat(his.chat.bot("So that was Lunch. Will you have Dinner also have logged?"));
            his.setStates("Okay", 10, "No", 7)
            his.caller.getAllChats(false, true);
            return;            
          } else {
            his.chat.addChat(his.chat.bot("So that was Dinner. Will you have Lunch also have logged?"));
            his.setStates("Okay", 4, "Show History", 2)
            his.caller.getAllChats(false, true);
            return;            
          }
        }
        his.chat.addChat(his.chat.bot("By the way.. Do you want to chat a bit about your history.."));
        his.setStates("Okay", 7, "No", 7)
        his.caller.getAllChats(false, true);
    }, MiscData.longWait);;
  }

  // From cont1Step5 with another day selection
  cont0Step6(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s6]);
    his.chat.aktStep = "cont0Step6";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.user("Let me choose a date!"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, select the day you want to enter your courses for."));
        his.caller.getAllChats(false,false);
        his.setStates("",2,"",3);
        his.chat.setDatePicker(true);  
        //console.log(his.caller.datePicker);
        his.caller.datePicker.open();
    }, MiscData.shortWait);
  }

  cont0Step7(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s7]);
    his.chat.aktStep = "cont0Step7";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      if(!his.caller.user.logged){
          his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s7]+0);
         his.chat.addChat(his.chat.bot("Can't show you your history because you haven't complete your profile!"));
         his.setStates("Okay", 9, "Thanks Bye", 7);
          his.caller.getAllChats(false,true);
    }else{
        his.chat.removeLastChat();
        his.chat.addChat(his.chat.bot("Ok, these is the percentage of nutritions you ate today:"));
        his.chat.addChat(his.chat.botC(":", "Today's nutritions (%)", "bar", 
                         his.caller.buildBarChartDataToday(),
                         his.caller.buildBarChartLabel()
                         ));
        his.chat.addChat(his.chat.bot("Want to see more?"));
        his.setStates("Okay", 8, "No", 7)
        his.caller.getAllChats(false,true);
    }
    }, MiscData.shortWait);
  }

  cont0Step8(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s8]);
    his.chat.aktStep = "cont0Step8";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.removeLastChat();
        his.chat.addChat(his.chat.bot("This is the nutrition percentage of the week:"));
        his.chat.addChat(his.chat.botC(":", "This Week's Nutritions (%)", "bar", 
                         his.caller.buildWeeklyBarChartData(), 
                         his.caller.buildBarChartLabel()
                         ));
        his.setStates("See More", 11, "Bye", 7)
        his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  cont0Step9(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s9]);
    his.chat.aktStep = "cont0Step9";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  // Selection for dinner when Lunch is done
  cont0Step10(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s10]);
    his.chat.aktStep = "cont0Step10";
    his.kind = Ate2Runner.ISDINNER;
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
      his.chat.removeLastChat();
      let d = moment(his.caller.today).format("ddd MMM DD, YYYY");
      his.events.subscribe(Tags.MENUESD_READY, (first, second, side) => {
         his.events.unsubscribe(Tags.MENUESD_READY);
         his.setStates("", 5, "", 5);
         his.doCheckbox(first, second, side, "Dinner on " + d);
      });
      his.unibz.setToday(moment(his.caller.today).format("DYYY-MM-DD"));
    }, MiscData.shortWait);
  }

  cont0Step11(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER0s11]);
    his.chat.aktStep = "cont0Step11";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.removeLastChat();
        his.chat.addChat(his.chat.bot("Ok, these are this dishes you ate today:"));
        his.events.subscribe(Unibz.UNIBZ_EATENDISHES,(data) => {
        his.events.unsubscribe(Unibz.UNIBZ_EATENDISHES);
        his.events.unsubscribe(Unibz.UNIBZ_EATENDISHES_ERR);
        console.log("unsubscribe");
        let eatendishes: any[] = [];
        for (let element of data){
          let it = {t: element.name, rclass: "", c: "-2"};
          eatendishes.push(it);
        }
        console.log(eatendishes);
        his.chat.addChat(his.chat.botD(eatendishes, "Eaten Dishes on " + moment(his.caller.today).format("ddd MMM DD")));
        his.setStates("Thanks", 9, "Bye", 7);
        his.caller.getAllChats(false,true);
      })
      his.unibz.getEatenDishes(his.caller.user, moment().format("YYYY-MM-DD"));
    }, MiscData.shortWait);
  }


  doCheckbox(fi, se, si, titel: string) {
    let profileModal = this.modalCtrl.create(ModalMenue, { prM: fi, seM: se, siM: si, titel: titel });
    profileModal.onDidDismiss(data => {
      if(data.length > 0) {
        this.data = data;
        this.cont0();
        return;
      }
      this.cont1();
    });
    profileModal.present();
  }


  cont1Step0(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s0]);
    his.chat.aktStep = "cont1Step0";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  // another day from cont0Step0 was selected
  cont1Step1(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s1]);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.user("Show me a calender!"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, select the day you want to enter your courses for."));
        his.caller.getAllChats(false,false);
        his.setStates("", 1, "", 3);
        his.chat.setDatePicker(true);  
        his.caller.datePicker.open();
    }, MiscData.shortWait);
  }

  cont1Step2(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s2]);
    his.chat.aktStep = "cont1Step2";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.yesRunState = 7;
    his.caller.cont0();
  }
  // CalenderShown and 'Cancel' selected!
  cont1Step3(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s3]);
    his.chat.aktStep = "cont1Step3";
    his.chat.addChat(his.chat.user("No new Date."));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, so I think you want to quit."));
        his.chat.addChat(his.chat.bot("Bye."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  // Decision for Dinner
  cont1Step4(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s4]);
    his.chat.aktStep = "cont1Step4";
    his.kind = Ate2Runner.ISDINNER;
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
      his.chat.removeLastChat();
      let d = moment(his.caller.today).format("ddd MMM DD, YYYY");
      his.events.subscribe(Tags.MENUESD_READY, (first, second, side) => {
         his.events.unsubscribe(Tags.MENUESD_READY);
         his.setStates("", 5,"",5);
         his.doCheckbox(first, second, side, "Dinner on " + d);
      });
      his.unibz.setToday(moment(his.caller.today).format("YYYY-MM-DD"));
    }, MiscData.longWait);
  }

  // From Menue-Popup with no selection
  cont1Step5(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s5]);
    his.chat.aktStep = "cont1Step5";
    his.chat.addChat(his.chat.user("-"));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Oh, you have nothing selected."));
        his.chat.addChat(his.chat.bot("Will you look for another day?"));
        his.setStates("Yes, another day", 6, "No", 6)
        his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  //No other day 
  cont1Step6(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s6]);
    his.chat.aktStep = "cont1Step6";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  //from cont0Step5 with no
  cont1Step7(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s7]);
    his.chat.aktStep = "cont1Step7";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }


  cont1Step8(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.IATERUNNER[Tags.IATERUNNER1s8]);
    his.chat.aktStep = "cont1Step8";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Okaaay.. So you better direcly select from the Menue. "));
      if(moment(new Date().toISOString()).isSame(moment(his.caller.today), 'day')) {
        if(moment(his.caller.theDay).isBefore(moment().set({'hour': 11, 'minute': 45}))) {
          his.chat.addChat(his.chat.bot("Hey Buddy, the Mensa wasn't open yet."));
          his.chat.addChat(his.chat.bot("Wanna quit or chose another day?"));
            his.setStates("Quit", 9, "Another day", 1)
            his.caller.getAllChats(false, true);
            return;
        } 
        if(moment(his.caller.theDay).isBefore(moment().set({'hour': 18, 'minute': 35}))) {
          his.chat.addChat(his.chat.bot("Please select your Lunch."));
            his.setStates("Lunch", 4, "", -1)
            his.aktDayHas = Ate2Runner.ISLUNCH;
            his.caller.getAllChats(false, true);
            return;
        } 
      }
      his.events.subscribe(Tags.MENSAOPEN_DINNER, (dinner) => {
        his.events.unsubscribe(Tags.MENSAOPEN_DINNER);
        if(dinner) {
          his.chat.addChat(his.chat.bot("Please select your Lunch or Dinner."));
          his.aktDayHas = Ate2Runner.ISBOTH;
          his.setStates("Lunch", 4, "Dinner", 4)
        } else {
          his.chat.addChat(his.chat.bot("Please select your Lunch."));
          his.aktDayHas = Ate2Runner.ISLUNCH;
          his.setStates("Lunch", 4, "", -1)
        }
        his.caller.getAllChats(false, true);
      });
      his.tt.hasDinner(moment(his.caller.today));
    }, MiscData.shortWait);
  }
}