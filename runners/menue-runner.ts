import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
     
import { ModalController, Events } from 'ionic-angular';

import { MiscData } from '../providers/miscdata';
import { Data } from '../providers/userdata';
import { Sergelution } from '../providers/sergelution';
import { Unibz } from '../providers/unibz';
import { TimeTicker } from '../providers/akttime';
//import { ModalMenue } from '../pages/modalmenue/modalmenue';
 
import { Datum } from '../providers/datum';
import { Runner } from '../interfaces/runner';
import { Tags } from '../interfaces/tags';


import * as moment from 'moment';

  interface fn {
    (param: any): void;
  }
 
@Injectable()
export class MenueRunner implements Runner {
  data: any;
  chat: any;
  caller: any;
  kind: any = 'lunch';
 
// this is to get rid of the if/if/if and calling the functions via an array
// the drawback is the 'this', that must be explicitly delivered to each function!
  yesFunctions: fn[] = [
    this.cont0Step0,
    this.cont0Step1,
    this.cont0Step2,
    this.cont0Step3,
    this.cont0Step4,
    this.cont0Step5,
    this.cont0Step6,
    this.cont0Step7,
    this.cont0Step8,
    this.cont0Step9,
    this.cont0Step10,
    this.cont0Step11,
    this.cont0Step12,
  ];
  noFunctions: fn[] = [
    this.cont1Step0,
    this.cont1Step1,
    this.cont1Step2,
    this.cont1Step3,
    this.cont1Step4,
    this.cont1Step5,
    this.cont1Step6,
    this.cont1Step7,
    this.cont1Step8,
    this.cont1Step9,
  ];
  constructor(    
        public datum: Datum,
        public events: Events,
        public modalCtrl: ModalController,
        public misc: MiscData,
        public userData: Data,
        public unibz: Unibz,
        public sergelution: Sergelution,
        public tt: TimeTicker,
        ) { }
 
  // init rhe runner. provide the callback this and the used chat-instance
  init(callerThis, chat): void {
    this.caller = callerThis;
    this.chat = chat;
  }

  cont1() {
    this.noFunctions[this.chat.noRunState](this);
  }
  cont0() {
    this.yesFunctions[this.chat.yesRunState](this);
  }
  setStates(yes, yesS, no, noS) {
    console.log("setStates");
    console.log(this);
    console.log(yes + " / " + no);
    this.chat.yesButton = yes;
    this.chat.yesRunState = yesS;
    this.chat.noButton = no;
    this.chat.noRunState = noS;
  }
  setYesButton(yes: string) {
    this.chat.yesButton = yes;
  }
  
  // Enter from homechat chat-selection
  cont0Step0(his: any): void {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s0]);
    his.chat.aktStep = "cont0Step0";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Okay"));
      his.chat.addChat(his.chat.bot("You want to checkout the menu for today?"));
      his.setStates("Today", 1, "Another day", 1)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  //1 Today from cont0Step0 was selected
  cont0Step1(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s1]);
    his.chat.aktStep = "cont0Step1";
    his.chat.addChat(his.chat.user(moment(his.caller.today).format("dddd MMM DD")));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.events.subscribe(Tags.MENSAOPEN_DAY, (isOpen) => {
      his.events.unsubscribe(Tags.MENSAOPEN_DAY);
      if(!isOpen){
        his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s1]+9);
        his.chat.changeChat(his.chat.bot("Hey! The Mensa is closed on " + moment(his.caller.today).format("dddd MMM DD")));
        his.chat.addChat(his.chat.bot("Wanna quit or choose another day?"));
        his.setStates("Quit", 9, "Another Day", 1);
        his.caller.getAllChats(false,true);
        return;
      }
    setTimeout(() => { 
       his.chat.changeChat(his.chat.bot("Great! Good Choice!"));
       his.chat.addChat(his.chat.bot("For Lunch or Dinner?"));
       his.setStates("Lunch", 3, "Dinner", 4)
       his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
        });
   his.tt.checkDayMensaOpen(moment(his.caller.today)); 
}


 // from cont1Step2 
  cont0Step2(his: any) {
     his.chat.aktStep = "cont0Step2";
  }

  // Lunch for Today from cont0Step1 was selected
  cont0Step3(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s3]);
    his.chat.aktStep = "cont0Step3";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    
    setTimeout( () => { 
      
      his.chat.changeChat(his.chat.bot("Lunch Time! Howdy Partner!"));      // check if its today!
      console.log(moment().format() + " / "+ moment(his.caller.today).format());
      his.chat.addChat(his.chat.bot("Here is what you get for Lunch .."));
      //let d = moment(his.caller.today).format("ddd MMM DD, YYYY");

      his.events.subscribe(Tags.MENUESL_READY,(first, second, side) => {
        his.events.unsubscribe(Tags.MENUESL_READY);
        console.log("unsubscribe");
        let sb: any[] = [];
        console.log(first);
        sb.push({t: "Primo", rclass: "", c: "-1"});
        for( let menue of first) {
          let it = {t: menue.name , rclass: menue.rec_class, c: menue.rec_color};
          sb.push(it);
        }
        sb.push({t: "Secondo", rclass: "",  c: "-1"});
        for( let menue of second) {
          let it = {t: menue.name , rclass: menue.rec_class, c: menue.rec_color};
          sb.push(it);
        }
        sb.push({t: "Side Dishes",rclass: "", c: "-1"});
        for( let menue of side) {
          let it = {t: menue.name , rclass: menue.rec_class, c: menue.rec_color};
          sb.push(it);
        }
        his.chat.addChat(his.chat.botD(sb, "Lunch on " + moment(his.caller.today).format("ddd MMM DD YYYY")));
        his.setStates("Thank you!", 4, "Show me Dinner", 4)
        his.caller.getAllChats(false, true);
      })
      his.unibz.setToday(moment(his.caller.today).format("YYYY-MM-DD"));
//      his.setStates("Thank you!", 4, "Show me Dinner", 4)
//      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);

  }
      

  // Selection for Thankyou from cont0Step3
  cont0Step4(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s4]);
    his.chat.aktStep = "cont0Step4";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
        his.chat.changeChat(his.chat.bot("Oh come on Buddy. I do everything for you!"));
        his.chat.addChat(his.chat.bot("Maybe you can do something for me?"));
        his.setStates("Okay?", 7, "No time..", 6)
        his.caller.getAllChats(false, true);
    }, MiscData.longWait);
  }

  cont0Step5(his: any) {
    his.chat.aktStep = "cont0Step5";
    
  }

  // From cont1Step5 with another day selection
  cont0Step6(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s6]);
    his.chat.aktStep = "cont0Step6";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.user("Show me a calender!"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
        his.chat.removeLastChat(); 
        his.chat.changeChat(his.chat.bot("OK, select the day you want to see the menu for."));
        his.caller.getAllChats(false,false);
        his.chat.setDatePicker(true);  
        console.log(his.caller.datePicker);
        his.caller.datePicker.open();
    }, MiscData.shortWait);
  }

//cont0Step4 with Okay
  cont0Step7(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s7]);
    his.chat.aktStep = "cont0Step7";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.removeLastChat();
        his.chat.addChat(his.chat.bot("Okay tell me.."));
        his.chat.addChat(his.chat.bot("Are you planning to go to mensa today for lunch?"));
        his.setStates("True!", 12, "Not Lunch, Dinner!", 9)
        his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  //cont0Step7 with True
  cont0Step8(his:any): void  {
     his.chat.aktStep = "cont0Step8";

  }
  cont0Step9(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s9]);
    his.chat.aktStep = "cont0Step9";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  //empty step
  cont0Step10(his:any): void  {
    his.chat.aktStep = "cont0Step10";
  }

  cont0Step11(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s11]);
    his.chat.aktStep = "cont0Step11";
 //   his.chat.addChat(his.chat.user(UserModel.MENSATIMETYPL[his.caller.user.mensatimetypL]));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Thanks a lot.. The world is a better place now."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  cont0Step12(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER0s12]);
    his.chat.aktStep = "cont0Step12";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithLunchTime();
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, from this you can choose."));
        his.caller.getAllChats(false,false);
        his.chat.set3Picker(true);
        his.caller.threePicker.open();
        his.setStates("", 11, "", 0)
    }, MiscData.shortWait);
  }

  cont1Step0(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s0]);
    his.chat.aktStep = "cont1Step0";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  // another day from cont0Step0 was selected
  cont1Step1(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s1]);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.user("Show me a calender!"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, select the day you want to see the menu."));
        his.caller.getAllChats(false,false);
        his.setStates("",1,"",2);
        his.chat.setDatePicker(true);  
        console.log(his.caller.datePicker);
        his.caller.datePicker.open();
    }, MiscData.shortWait);
  }
  cont1Step2(his:any): void  {
    his.chat.aktStep = "cont1Step2";
    console.log(his.chat.aktstep);
  }
  // CalenderShown and 'Cancel' selected!
  cont1Step3(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s3]);
    his.chat.aktStep = "cont1Step3";
    his.chat.addChat(his.chat.user("No new Date.."));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Okay, so I don't know what you want."));
        his.chat.addChat(his.chat.bot("Bye."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  // Decision for Dinner
  cont1Step4(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s4]);
    his.chat.aktStep = "cont1Step4";
    his.kind = 'dinner';
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
      his.chat.removeLastChat();
      his.chat.addChat(his.chat.bot("Dinner Dinner Chicken Winner!"));
      his.chat.addChat(his.chat.bot("So here is the Dinner menu!"));

      his.events.subscribe('menuesD:ready',(first, second, side) => {
        his.events.unsubscribe('menuesD:ready');
        console.log("unsubscribe");
        let sb: any[] = [];
        console.log(first);
        sb.push({t: "Primo", rclass: "", c: "-1"});
        for( let menue of first) {
          let it = {t: menue.name , rclass: menue.rec_class, c: menue.rec_color};
          sb.push(it);
        }
        sb.push({t: "Secondo", rclass: "",  c: "-1"});
        for( let menue of second) {
          let it = {t: menue.name , rclass: menue.rec_class, c: menue.rec_color};
          sb.push(it);
        }
        sb.push({t: "Side Dishes",rclass: "", c: "-1"});
        for( let menue of side) {
          let it = {t: menue.name , rclass: menue.rec_class, c: menue.rec_color};
          sb.push(it);
        }
        his.chat.addChat(his.chat.botD(sb, "Dinner on " + moment(his.caller.today).format("ddd MMM DD YYYY")));
      his.setStates("Thanks!", 4, "That's all", 6)
        his.caller.getAllChats(false, true);
      })
      his.unibz.setToday(moment(his.caller.today).format("YYYY-MM-DD"));
    }, MiscData.shortWait);
  }

  // From Menue-Popup with no selection
  cont1Step5(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s5]);
    his.chat.aktStep = "cont1Step5";
    his.chat.addChat(his.chat.user("-"));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.addChat(his.chat.bot("Oh, you have nothing selected."));
        his.chat.addChat(his.chat.bot("Will you look for another day?"));
        his.setStates("Yes, another day", 6, "No", 6)
        his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  //
  cont1Step6(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s6]);
    his.chat.aktStep = "cont1Step6";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you want something."));
        his.chat.addChat(his.chat.bot("We'll meet again."));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  //From datepicker to cont0Step8
  cont1Step7(his:any): void  {
    his.chat.aktStep = "cont1Step7";
 
  }

// from cont0Step7 I have no idea
  cont1Step8(his:any): void  {
   
    his.chat.aktStep = "cont1Step8";
   
  }

cont1Step9(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.MENUECHATRUNNER[Tags.MENUECHATRUNNER1s9]);
    his.chat.aktStep = "cont1Step9";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.yesRunState = 12;
    his.caller.cont0();
  }
}