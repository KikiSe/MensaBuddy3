import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
     
import { ModalController, Events } from 'ionic-angular';

import { MiscData } from '../providers/miscdata';
import { Data } from '../providers/userdata';
import { Sergelution } from '../providers/sergelution';
import { TimeTicker } from '../providers/akttime';
import { UserModel } from '../models/user-model';
 
import { Datum } from '../providers/datum';
import { Unibz } from '../providers/unibz';
import { Runner } from '../interfaces/runner';
import { Tags } from '../interfaces/tags';

import * as moment from 'moment';

  interface fn {
    (param: any): void;
  }

@Injectable()
export class RecommenderRunner implements Runner {
  private static readonly ISLUNCH = 1;  
  private static readonly ISDINNER = 2;  

  data: any;
  chat: any;
  caller: any;
  kind: number = 0;
  user: any;
  dummies: number = 0;
  receptMsg: any[] = [];
  receptMsg1: any[] = [];
  eatensalt: any;
  eatensfa: any;
  eatensugars: any;
  menusugars: any;
  menusfa: any;
  menusalt: any;
  dailyneedsugar: any;
  dailyneedsfa: any;
  dailyneedsalt: any;

// this is to get rid of the if/if/if and calling the functions via an array
// the drawback is the 'this', that must be explicitly delivered to each function!
  yesFunctions: fn[] = [
    this.cont0Step0,
    this.cont0Step1,
    this.cont0Step2,
    this.cont0Step3,
    this.cont0Step4,
    this.cont0Step5,
    this.cont0Step6,
    this.cont0Step7,
    this.cont0Step8,
    this.cont0Step9,
    this.cont0Step10,
    this.cont0Step11,
    this.cont0Step12,
    this.cont0Step13,
 
  ];
  noFunctions: fn[] = [
    this.cont1Step0,
    this.cont1Step1,
    this.cont1Step2,
    this.cont1Step3,
    this.cont1Step4,
    

  ];
  constructor(    
        public datum: Datum,
        public events: Events,
        public modalCtrl: ModalController,
        public misc: MiscData,
        public userData: Data,
        public sergelution: Sergelution,
        public unibz: Unibz,
        public tt: TimeTicker,
        ) { }
 
  // init rhe runner. provide the callback this and the used chat-instance
  init(callerThis, chat): void {
    this.caller = callerThis;
    this.chat = chat;
  }

  cont1() {
    if(this.dummies != 0) {
      this.chat.removeLastChats(this.dummies);
      this.dummies = 0;
    }
    this.noFunctions[this.chat.noRunState](this);
  }
  cont0() {
    if(this.dummies != 0) {
      this.chat.removeLastChats(this.dummies);
      this.dummies = 0;
    }
    this.yesFunctions[this.chat.yesRunState](this);
  }
  setStates(yes, yesS, no, noS) {
    console.log("setStates");
    console.log(this);
    console.log(yes + " / " + no);
    this.chat.yesButton = yes;
    this.chat.yesRunState = yesS;
    this.chat.noButton = no;
    this.chat.noRunState = noS;
  }
  setYesButton(yes: string) {
    this.chat.yesButton = yes;
  }

// To push up the display for multi-picker 
  addChatDummies(his:any, count: number = 3) {
    his.dummies = 4;
    console.log("Adding " + count + " chat dummies!");
    for(let i=0;i<count;i++) {
        his.chat.addChat(his.chat.bot("."));
    }  
  }

  eaten: any;
  recom0: any;
  recom1: any;
  recom2: any;
  recom3: any;
  recom4: any;
  recom5: any;

  buildWarnings(fit) {
    console.log("eatensfa "+ this.eatensfa);
    this.menusalt = fit[2].salt_in_menu;
    this.menusugars = fit[2].sugars_in_menu;
    this.menusfa = fit[2].sfa_in_menu;
    this.dailyneedsugar = fit[2].sugars_daily_limit;
    this.dailyneedsfa = fit[2].sfa_daily_limit;
    this.dailyneedsalt = fit[2].salt_daily_limit; ;
    console.log("menusfa "+ this.menusfa);
    
    let menusugarstext =  this.menusugars;
    let menusalttext =  this.menusalt;
    let menusfatext =  this.menusfa;

    if(+this.menusugars > +this.dailyneedsugar){
      this.menusugars = this.menusugars.toFixed(1);
      menusugarstext = "<strong><font color='#FF6F00' >"  +  this.menusugars  +  "</font></strong>";
    }
    if(+this.menusalt > +this.dailyneedsalt){
      this.menusalt = this.menusalt.toFixed(1);
      menusalttext = "<strong><font color='#FF6F00' >"  +  this.menusalt +  "</font></strong>";
    }
    if(+this.menusfa > +this.dailyneedsfa){
      this.menusfa = this.menusfa.toFixed(1);
      menusfatext = "<strong><font color='#FF6F00'>"  +  this.menusfa  +  "</font></strong>";
      console.log(this.menusfa + " > " + this.dailyneedsfa);
    }
    this.receptMsg.push("Limiting the intake of sugars, saturated fatty" 
    +"acids and salt lowers the risk of food-related diseases." +
    "Your daily limits are "
    + this.dailyneedsugar + " of sugar, "
    + this.dailyneedsfa +  " of saturated fatty acids and " 
    + this.dailyneedsalt+ " of salt. " 
     );
    this.receptMsg1.push("This menu provides you "
    + menusugarstext + " of sugar, "  
    + menusfatext+ " of saturated fatty acids and " 
    + menusalttext + " grams of salt." 
     );
  }

  buildRecoms(his:any, data: any) {
      let fits0: any[];
      let fits1: any[];
      let fits2: any[];
      let fits3: any[];
      let fits4: any[];
      let fits5: any[];
      switch(+his.caller.user.mealtyp) {
        case 1: //primo
          fits0 = data.firsts[0];
          fits1 = data.firsts[1];
          fits2 = data.firsts[2];
          fits3 = data.firsts[3];
          fits4 = data.firsts[4];
          fits5 = data.firsts[5];
          his.recom0 = { F : fits0[0][0].name, SD : fits0[0][1].name };
          if(fits1) {
            his.recom1 = { F : fits1[0][0].name, SD : fits1[0][1].name };
          }
          if(fits2) {
            his.recom2 = { F : fits2[0][0].name, SD : fits2[0][1].name };
          }
          if(fits3) {
            his.recom3 = { F : fits3[0][0].name, SD : fits3[0][1].name };
          }
          if(fits4) {
            his.recom4 = { F : fits4[0][0].name, SD : fits4[0][1].name };
          }
          if(fits5) {
            his.recom5 = { F : fits5[0][0].name, SD : fits5[0][1].name };
          }
          break;        
        case 2: //secundo
          fits0 = data.seconds[0];
          fits1 = data.seconds[1];
          fits2 = data.seconds[2];
          fits3 = data.seconds[3];
          fits4 = data.seconds[4];
          fits5 = data.seconds[5];
          his.recom0 = { S : fits0[0][0].name, SD : fits0[0][1].name };
          if(fits1) {
            his.recom1 = { S : fits1[0][0].name, SD : fits1[0][1].name };
          }
          if(fits2) {
            his.recom2 = { S : fits2[0][0].name, SD : fits2[0][1].name };
          }
          if(fits3) {
            his.recom3 = { S : fits3[0][0].name, SD : fits3[0][1].name };
          }
          if(fits4) {
            his.recom4 = { S : fits4[0][0].name, SD : fits4[0][1].name };
          }
          if(fits5) {
            his.recom5 = { S : fits5[0][0].name, SD : fits5[0][1].name };
          }

          break;        
        case 3: //full
          fits0 = data.fulls[0];
          fits1 = data.fulls[1];
          fits2 = data.fulls[2];
          fits3 = data.fulls[3];
          fits4 = data.fulls[4];
          fits5 = data.fulls[5];
          his.recom0 = { F : fits0[0][0].name, S : fits0[0][1].name, SD : fits0[0][2].name };
          if(fits1) {
            his.recom1 = { F : fits1[0][0].name, S : fits1[0][1].name, SD : fits1[0][2].name };
          }
          if(fits2) {
            his.recom2 = { F : fits2[0][0].name, S : fits2[0][1].name, SD : fits2[0][2].name };
          }
          if(fits3) {
            his.recom3 = { F : fits3[0][0].name, S : fits3[0][1].name, SD : fits3[0][2].name };
          }
          if(fits4) {
            his.recom4 = { F : fits4[0][0].name, S : fits4[0][1].name, SD : fits4[0][2].name };
          }
          if(fits5) {
            his.recom5 = { F : fits5[0][0].name, S : fits5[0][1].name, SD : fits5[0][2].name };
          }
          break;        
      }
      his.receptMsg = [];
      his.buildWarnings(fits0); 
      this.dailyneedsfa = fits0[2].sfa_daily_limit;
      console.log("dailysfa "+ this.dailyneedsfa);
      this.dailyneedsugar = fits0[2].sugars_daily_limit;
      this.dailyneedsalt = fits0[2].salt_daily_limit; 
      if(fits0[2].sfa_eaten_today){
        this.eatensfa = 0;
      }else{
         this.eatensfa = fits0[2].sfa_eaten_today.toFixed(1);
      }
      if(!fits0[2].sugars_eaten_today){
        this.eatensugars = 0;
      }else{
         this.eatensugars = fits0[2].sugars_eaten_today.toFixed(1);
      }
      if(!fits0[2].salt_eaten_today){
        this.eatensalt = 0;
      }else{
        this.eatensalt = fits0[2].salt_eaten_today.toFixed(1);
      } 
      if(! his.fits1) {
      
      } else {
        his.buildWarnings(fits1);         
      }
     if(! his.fits2) {

      } else {
       his.buildWarnings(fits2);         
      }
      if(! his.fits3) {

      } else {
        his.buildWarnings(fits3);         
      }
      if(! his.fits4) {

      } else {
       his.buildWarnings(fits4);          
      }
      if(! his.fits5) {

      } else {
        his.buildWarnings(fits5);       
      }
  }


  // Enter from homechat chat-selection
  cont0Step0(his: any): void {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s0]);
    his.chat.aktStep = "x0x0";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Okay.."));
      his.chat.addChat(his.chat.bot("So you need a recommendation for your meal today?"));
      his.setStates("Exactly", 1, "", 1)
      
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  // Today from cont0Step0 was selected
  cont0Step1(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s1]);
    his.chat.aktStep = "x0x1";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Tell me do you want a recommendation for lunch or dinner?"));
      his.setStates("Lunch", 2, "Dinner", 2)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  // from cont1Step2 
  cont0Step2(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s2]);
    his.chat.aktStep = "x0x2";
    his.kind = RecommenderRunner.ISLUNCH;
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithMENUDINNERTyp();
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("To give you better recommendations, i need some information."));
      his.chat.addChat(his.chat.bot("Tell me how big your Dinner will be tonight?"));
      setTimeout(() => { 
      his.addChatDummies(his, 3);
      his.caller.getAllChats(false,false);
      his.chat.set3Picker(true);
      his.caller.threePicker.open();
      his.setStates("", 7, "", 0)
      }, MiscData.superlongWait);
    }, MiscData.shortWait);
  }

cont0Step3(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s3]);
    his.chat.aktStep = "x0x3";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithMealTyp();
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Which kind of menu are you looking for?"));
      setTimeout(() => { 
      his.addChatDummies(his, 3);
      his.caller.getAllChats(false,false);
      his.chat.set3Picker(true);
      his.caller.threePicker.open();
      his.setStates("", 4, "", 0)
      }, MiscData.longWait);
    }, MiscData.shortWait);
  }


  cont0Step4(his: any) {
    console.log("cont0Step4");
    console.log(his.user);
    console.log(his.caller.user);
    
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s4]);
    his.chat.aktStep = "x0x4";
    his.chat.addChat(his.chat.user(UserModel.MEAL[his.caller.user.mealtyp]));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      
      his.chat.changeChat(his.chat.bot("Now i will calculate the best choices for you.."));
      his.events.subscribe(Unibz.UNIBZ_SUGESTION, (data) => {
      his.events.unsubscribe(Unibz.UNIBZ_SUGESTION);
      his.events.unsubscribe(Unibz.UNIBZ_SUGESTION_ERR);
      if(data.fulls.length == 0) {
        his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s4]+0);
        his.chat.addChat(his.chat.bot("Sorry, no recommendations for today!"));
        his.setStates("", -1, "Okay", 0)
        his.caller.getAllChats(false, true);
        return;
      }
      his.buildRecoms(his, data);

      his.chat.addChat(his.chat.bot(his.receptMsg));
      his.chat.addChat(his.chat.bot(his.receptMsg1));
      let sb: any[] = [];
        
      if(his.recom0.F) {
        
        sb.push({t: "As First Course", rclass: "", c: "-1"});
        let it = {t: his.recom0.F , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom0.S) {
        sb.push({t: "As Second Course", rclass: "", c: "-1"});
        let it = {t: his.recom0.S , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom0.SD) {
        sb.push({t: "As Side Dish", rclass: "", c: "-1"});
        let it = {t: his.recom0.SD , rclass: "", c: "0"};
        sb.push(it);
      }
      his.chat.addChat(his.chat.botD(sb, "Recommendation for " + moment(his.caller.today).format("ddd MMM DD")));
      his.eaten = his.recom0;
      his.setStates("Show another", 5, "I will eat this", 3)
      his.caller.getAllChats(false, true);
    });
    his.events.subscribe(Unibz.UNIBZ_SUGESTION_ERR, (err) => {
      his.events.unsubscribe(Unibz.UNIBZ_SUGESTION);
      his.events.unsubscribe(Unibz.UNIBZ_SUGESTION_ERR);
      his.chat.addChat(his.chat.bot("Sorry, no recommendations for today!"));
      his.setStates("", -1, "Bye", 0)
      his.caller.getAllChats(false, true);
      return;
    });

    let wanted: string = "L";
    if(his.kind != RecommenderRunner.ISLUNCH) {
      wanted = "D";
    }
    let menu: string = "mensa";    
    if(his.kind == RecommenderRunner.ISLUNCH) {
      menu = UserModel.MENU_BZ[+his.caller.user.menutypL];
    } else {
      menu = UserModel.MENU_BZ[+his.caller.user.menutypD];
    }
    if(menu == "") {
      menu = "mensa";      
    }
    let other = "";
    if(his.kind == RecommenderRunner.ISLUNCH) {
      other = UserModel.MENU_BZ[+his.caller.user.menutypD];
    } else {
      other = UserModel.MENU_BZ[+his.caller.user.menutypL];
    }
    if(other == "") {
      other = "mensa";
    }
    his.unibz.getRecommends(his.caller.user,wanted, menu, other);
    }, MiscData.shortWait);
  }


  cont0Step5(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s5]);
    his.chat.aktStep = "x0x5";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
   setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Okay Here we go.."));
      // ADD the second ranked recommendation here
      if(! his.recom1) {
        his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s5]+0);
        his.chat.addChat(his.chat.bot("Sorry, there is no alternative recommendation today. Maybe take the other one?"));
        his.setStates("", -1, "Okay", 0)
        his.caller.getAllChats(false, true);
      }
      let sb: any[] = [];
     
       if(his.recom1.F) {
         
        sb.push({t: "As First Course", rclass: "", c: "-1"});
        let it = {t: his.recom1.F , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom1.S) {
        sb.push({t: "As Second Course", rclass: "", c: "-1"});
        let it = {t: his.recom1.S , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom1.SD) {
        sb.push({t: "As Side Dish", rclass: "", c: "-1"});
        let it = {t: his.recom1.SD , rclass: "", c: "0"};
        sb.push(it);
      }
      his.chat.addChat(his.chat.bot(his.receptMsg1));
      his.chat.addChat(his.chat.botD(sb, "Recommendation for " + moment(his.caller.today).format("ddd MMM DD")));
      his.eaten = his.recom1;
      his.setStates("Show another", 6, "I will eat this", 3)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  
  cont0Step6(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s6]);
    his.chat.aktStep = "x0x6";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Not satisfied enough?"));
      his.chat.addChat(his.chat.bot("So check this out!"));
      // ADD the first ranked recommendation here
      his.chat.changeChat(his.chat.bot("Okay Here we go.."));
      // ADD the second ranked recommendation here
      if(! his.recom2) {
         his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s6]+0);
        his.chat.addChat(his.chat.bot("Sorry, there is no alternative recommendation today. Take one of the others maybe?"));
        his.setStates("", -1, "Okay", 0)
        his.caller.getAllChats(false, true);
      }
      let sb: any[] = [];
     
       if(his.recom2.F) {
        sb.push({t: "As First Course", rclass: "", c: "-1"});
        let it = {t: his.recom2.F , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom2.S) {
        sb.push({t: "As Second Course", rclass: "", c: "-1"});
        let it = {t: his.recom2.S , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom2.SD) {
        sb.push({t: "As Side Dish", rclass: "", c: "-1"});
        let it = {t: his.recom2.SD , rclass: "", c: "0"};
        sb.push(it);
      }
      his.chat.addChat(his.chat.bot(his.receptMsg1));
      his.chat.addChat(his.chat.botD(sb, "Recommendation for " + moment(his.caller.today).format("ddd MMM DD")));
      his.eaten = his.recom2;
      his.setStates("Show more", 11, "I will eat this", 3)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont0Step7(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s7]);
    his.chat.aktStep = "cont0Step7";
    console.log("kind: " + his.kind);
    console.log("his.caller.user.menutypD: " + his.caller.user.menutypD);
    console.log("his.caller.user.menutypL: " + his.caller.user.menutypL);

    if(his.kind == RecommenderRunner.ISLUNCH) {
      his.chat.addChat(his.chat.user(UserModel.MENUD[his.caller.user.menutypD]));
    } else {
      his.chat.addChat(his.chat.user(UserModel.MENUL[his.caller.user.menutypL]));
    }
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Thanks! So now we need one other thing to know.."));
      his.setStates("Okay",3 , "Bye", 0)
      his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  cont0Step8(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s8]);
    his.chat.aktStep = "cont0Step8";
    let sb: any[] = [];
//    console.log(his.data);
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      let kind = "L";
      if(his.kind != RecommenderRunner.ISLUNCH) {
        kind = "D";
      }
      let dishes = [];
      if(his.eaten.F) {
        dishes.push(his.eaten.F);
      }
      if(his.eaten.S) {
        dishes.push(his.eaten.S);
      }
      if(his.eaten.SD) {
        dishes.push(his.eaten.SD);
      }
      his.caller.events.subscribe(Unibz.UNIBZ_UPDATEUSERHISTORY, (data) => {
        his.caller.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY);
        his.caller.events.unsubscribe(Unibz.UNIBZ_UPDATEUSERHISTORY_ERR);
        console.log("update");
        console.log(data);
        his.caller.history = data;
        his.caller.carbs = data.carbs;
        his.caller.fiber = data.fiber;
        his.caller.proteins = data.proteins;
        his.caller.fats = data.fats;
        his.caller.calories = data.kcal;

        his.caller.todayCarb += data.carbs;
        his.caller.todayFiber += data.fiber;
        his.caller.todayProteins += data.proteins;
        his.caller.todayFats += data.fats;
        his.caller.todayCalories += data.kcal;
      });
      his.unibz.updateUserHistory(his.caller.user, kind, dishes);
      his.chat.changeChat(his.chat.bot("I add it to your history.."));
      his.setStates("More recommendations",1 , "Bye", 0)
      his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  cont0Step9(his: any) {
    his.cont1Step0(his);
  }
  cont0Step10(his: any) {
    his.cont1Step2(his);
  }
  cont0Step11(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s11]);
    his.chat.aktStep = "x0x11";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Not satisfied enough?"));
      
      if(! his.recom3) {
        his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s11]+0);
        his.chat.addChat(his.chat.bot("Sorry, there is no alternative recommendation today. "));
        his.setStates("", -1, "Okay", 0)
        his.caller.getAllChats(false, true);
      }
      let sb: any[] = [];
     
       if(his.recom3.F) {
        sb.push({t: "As First Course", rclass: "", c: "-1"});
        let it = {t: his.recom3.F , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom3.S) {
        sb.push({t: "As Second Course", rclass: "", c: "-1"});
        let it = {t: his.recom3.S , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom3.SD) {
        sb.push({t: "As Side Dish", rclass: "", c: "-1"});
        let it = {t: his.recom3.SD , rclass: "", c: "0"};
        sb.push(it);
      }
      his.chat.addChat(his.chat.bot(his.receptMsg1));
      his.chat.addChat(his.chat.botD(sb, "Another alternative Recommendation for " + moment(his.caller.today).format("ddd MMM DD")));
      his.eaten = his.recom3;
      his.setStates("Show more", 12, "I will eat this", 3)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }
  cont0Step12(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s12]);
    his.chat.aktStep = "x0x12";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Okay so look at this.."));
      
      if(! his.recom4) {
        his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s12]+0);
        his.chat.addChat(his.chat.bot("Sorry, there is no alternative recommendation today.,"));
        his.setStates("", -1, "Okay", 0)
        his.caller.getAllChats(false, true);
      }
      let sb: any[] = [];
       if(his.recom4.F) {
        sb.push({t: "As First Course", rclass: "", c: "-1"});
        let it = {t: his.recom4.F , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom4.S) {
        sb.push({t: "As Second Course", rclass: "", c: "-1"});
        let it = {t: his.recom4.S , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom4.SD) {
        sb.push({t: "As Side Dish", rclass: "", c: "-1"});
        let it = {t: his.recom4.SD , rclass: "", c: "0"};
        sb.push(it);
      }
      his.chat.addChat(his.chat.bot(his.receptMsg1));
      his.chat.addChat(his.chat.botD(sb, "Another alternative Recommendation for " + moment(his.caller.today).format("ddd MMM DD")));
      his.eaten = his.recom4;
      his.setStates("More", 13, "I will eat this", 3)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }
  cont0Step13(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s13]);
    his.chat.aktStep = "x0x13";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("You want it all!"));
     
      if(! his.recom5) {
          his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER0s13]+0);
        his.chat.addChat(his.chat.bot("Sorry, there is no alternative recommendation today. Take one of the others maybe?"));
        his.setStates("", -1, "Okay", 0)
        his.caller.getAllChats(false, true);
      }
      let sb: any[] = [];
       if(his.recom5.F) {
        sb.push({t: "As First Course", rclass: "", c: "-1"});
        let it = {t: his.recom5.F , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom5.S) {
        sb.push({t: "As Second Course", rclass: "", c: "-1"});
        let it = {t: his.recom5.S , rclass: "", c: "0"};
        sb.push(it);
      }
      if(his.recom5.SD) {
        sb.push({t: "As Side Dish", rclass: "", c: "-1"});
        let it = {t: his.recom5.SD , rclass: "", c: "0"};
        sb.push(it);
      }
      his.chat.addChat(his.chat.bot(his.receptMsg1));
      his.chat.addChat(his.chat.botD(sb, "Another alternative Recommendation for " + moment(his.caller.today).format("ddd MMM DD")));
      his.eaten = his.recom5;
      his.setStates("Maybe Dinner?", 10, "I will eat this", 3)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }



//Quit Conversation
  cont1Step0(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER1s0]);
    his.chat.aktStep = "cont1Step0";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you need me."));
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

// Anotherday was selected
  cont1Step1(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER1s1]);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("You want recommendation for Lunch or Dinner"));
        his.setStates("Lunch", 2, "Dinner", 2)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont1Step2(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER1s2]);
    his.chat.aktStep = "cont1Step2";
    his.kind = RecommenderRunner.ISDINNER;
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithMENULUNCHTyp();     
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Okay, Dinner so let's go!"));
        his.chat.addChat(his.chat.bot("To give you better recommendations, i need some information.."));
        his.chat.addChat(his.chat.bot("What kind of lunch did you have or plan to have?"));
        his.caller.getAllChats(false,false);
      his.chat.set3Picker(true);
      his.caller.threePicker.open();
      his.setStates("", 7, "", 0)
    }, MiscData.shortWait);
  }

  cont1Step3(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER1s3]);
    his.chat.aktStep = "cont1Step3";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false); 
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Okay, so i will save that you will eat this today!"));
        his.chat.addChat(his.chat.bot("Are you sure?"));
        his.setStates("Save", 8, "No, show others", 3)
        his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }
  cont1Step4(his:any): void  {
  //  his.sergelution.postUserAction(his.caller.user, Tags.RECOMMENDERCHATRUNNER[Tags.RECOMMENDERCHATRUNNER1s3]);
    his.chat.aktStep = "cont1Step4";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false); 
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("So you didn't choose anything.."));
        his.chat.addChat(his.chat.bot("Maybe we start again.."));
        his.setStates("Yes", 1, "No, Thanks", 9)  
        his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

 
}