import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
     
import { ModalController, Events } from 'ionic-angular';

import { MiscData } from '../providers/miscdata';
import { Data } from '../providers/userdata';
import { UserModel } from '../models/user-model';

import { Sergelution } from '../providers/sergelution';
import { Unibz } from '../providers/unibz';
import { TimeTicker } from '../providers/akttime';
//import { ModalMenue } from '../pages/modalmenue/modalmenue';
//import { HomeChatPage } from '../pages/homechat/homechat';
 
import { Datum } from '../providers/datum';
import { Tags } from '../interfaces/tags';

import { Runner } from '../interfaces/runner';


//import * as moment from 'moment';

  interface fn {
    (param: any): void;
  }

@Injectable()
export class IntroRunner implements Runner {
  data: any;
  chat: any;
  caller: any;
  kind: any = 'lunch';
  user: any;

// this is to get rid of the if/if/if and calling the functions via an array
// the drawback is the 'this', that must be explicitly delivered to each function!
  yesFunctions: fn[] = [
    this.cont0Step0,
    this.cont0Step1,
    this.cont0Step2,
    this.cont0Step3,
    this.cont0Step4,
    this.cont0Step5,
    this.cont0Step6,
    this.cont0Step7,
    this.cont0Step8,
    this.cont0Step9,
    this.cont0Step10,
    this.cont0Step11,

  ];
  noFunctions: fn[] = [
    this.cont1Step0,
    this.cont1Step1,
    this.cont1Step2,
    this.cont1Step3,
    this.cont1Step4,
    this.cont1Step5,
    this.cont1Step6,
    this.cont1Step7,
    this.cont1Step8,
    this.cont1Step9,
    this.cont1Step10,

  ];
  constructor(    
        public datum: Datum,
        public events: Events,
        public modalCtrl: ModalController,
        public misc: MiscData,
        public unibz: Unibz,
        public sergelution: Sergelution,
        public tt: TimeTicker,
        public userService: Data,
        
        ) { }
 
  // init rhe runner. provide the callback this and the used chat-instance
  init(callerThis, chat): void {
    this.caller = callerThis;
    this.chat = chat;
    console.log("Intro-runner called!")
  }

  cont1() {
    this.noFunctions[this.chat.noRunState](this);
  }
  cont0() {
    this.yesFunctions[this.chat.yesRunState](this);
  }
  setStates(yes, yesS, no, noS) {
    this.chat.yesButton = yes;
    this.chat.yesRunState = yesS;
    this.chat.noButton = no;
    this.chat.noRunState = noS;
  }
  setYesButton(yes: string) {
    this.chat.yesButton = yes;
  }
  // Enter from cont0step9
  cont0Step0(his: any): void {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s0]);
    his.chat.aktStep = "cont0Step0";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Great! So Hello " + his.caller.user.name + "!"));
      his.chat.addChat(his.chat.bot("Let me introduce myself! I'm your Mensa Buddy"))
      his.chat.addChat(his.chat.bot("I can help you checking out the menu, the webcam or give you some recommendations!"));
      his.setStates("Tell me more", 1, "Ok Bye", 0)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  // Today from cont0Step0 was selected
  cont0Step1(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s1]);
    his.chat.aktStep = "cont0Step1";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("You are curious! I like that."));
      his.chat.addChat(his.chat.bot("To give you better recommendations, i need to know some things!"));
      his.chat.addChat(his.chat.bot("Are you female or male?"));
      his.setStates(UserModel.GENDERS[UserModel.GENDERFEMALE], 2, UserModel.GENDERS[UserModel.GENDERMALE], 2)
      console.log("gender: " + his.caller.user.gender);
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  // from cont1Step2 
  cont0Step2(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s2]);
    his.chat.aktStep = "cont0Step2";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.gender = UserModel.GENDERFEMALE;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Bongiorno Madame " + his.caller.user.name + "!"));
        his.chat.addChat(his.chat.bot("And please tell me your breakfast habits."));
        his.setStates("Choose", 10, "Adios", 0)
        his.caller.getAllChats(false, true);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  // Back from the 3Picker with the given values stored!
  cont0Step3(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s3]);
    his.chat.aktStep = "cont0Step3";
    console.log("cont0Step3");
    console.log(UserModel.BREAKFASTS[his.caller.user.breakfasttyp]);
    console.log(his.caller.user.breakfasttyp);
    his.chat.addChat(his.chat.user(UserModel.BREAKFASTS[his.caller.user.breakfasttyp]));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Fine. What's your age?"));
      his.chat.addChat(his.chat.bot("Just enter it."));
      his.setStates("", 4, "", 7)
      his.chat.setInputField(true, "", "Enter your age","number", true, "Ok");
//      his.setStates("Yes!",4,"You don't need to know more, machine!",0);
      his.caller.getAllChats(false,false);
      setTimeout(() => {
        his.caller.inputId.setFocus();
      }, 300);
    }, MiscData.shortWait);
  }

  // Back from Input via cont0 call from caller
  cont0Step4(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s4]);
    let age = his.chat.iFValue;
    his.chat.aktStep = "cont0Step4";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user("Age: " + age));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.age = age;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Thanks!"));
        his.chat.addChat(his.chat.bot("So tell me how tall are you (cm)?"));
        his.setStates("", 5, "", 8)
        his.chat.setInputField(true, "", "Enter your height","number", true, "Ok");
        his.caller.getAllChats(false,false);
        setTimeout(() => {
          his.caller.inputId.setFocus();
        }, 300);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  // cont from 4, we have size input
  cont0Step5(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s5]);
    let size = his.chat.iFValue;
    his.chat.aktStep = "cont0Step5";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user("Size: " + size));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.size = size;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        //Open Timepicker
        his.chat.changeChat(his.chat.bot("Are you a giant? Haha"));
        his.chat.addChat(his.chat.bot("Next tell me your weight(kg) please?"));
        his.setStates("", 6, "", 10)
        his.chat.setInputField(true, "", "Enter your weight","number", true, "Ok");
        his.caller.getAllChats(false,false);
        setTimeout(() => {
          his.caller.inputId.setFocus();
        }, 300);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  // cont from 5, we have weight input
  cont0Step6(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s6]);
    let weight = his.chat.iFValue;
    his.chat.aktStep = "cont0Step6";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user("Weight: " + weight));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.weight = weight;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("So One more.."));
        his.chat.addChat(his.chat.bot("What about your activity..? Please choose your level of activity during the day."));
        his.caller.fillPickerWithActivity(); // So theres time to reakt for thr PickerModel
        his.setStates("Ok", 7, "Nope", 0)
        his.caller.getAllChats(false, true);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  cont0Step7(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s7]);
    his.chat.aktStep = "cont0Step7";
    his.chat.set3Picker(true);
    his.caller.threePicker.open();
    his.setStates("Ok", 8, "Nope", 7)
/*
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      //Open Timepicker
      his.chat.changeChat(his.chat.bot("You did it! Great!"));
      his.chat.addChat(his.chat.bot("Here is a little overview.. is this all correct?"));
       his.chat.addChat(his.chat.bot("If you want to make changes, just let me know later!"));
      his.setStates("Correct", 8, "Change something", 7)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
*/  
  }

  cont0Step8(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s8]);
    his.chat.aktStep = "cont0Step8";
    his.chat.addChat(his.chat.user(UserModel.ACTIVITIES[his.caller.user.activity]));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("You did it! Great!"));
      his.chat.addChat(his.chat.bot("Here is a little overview.. is this all correct?"));
      let sb: any[] = [];
      sb.push({t: "Name:        " + his.caller.user.name, rclass: "", c: "-2"});
      sb.push({t: "Age:         " + his.caller.user.age, rclass: "", c: "-2"});
      sb.push({t: "Gender:      " + UserModel.GENDERS[+his.caller.user.gender], rclass: "", c: "-2"});
      sb.push({t: "Size:        " + his.caller.user.size, rclass: "", c: "-2"});
      sb.push({t: "Weight     : " + his.caller.user.weight, rclass: "", c: "-2"});
      sb.push({t: "Breakfasttyp: " + UserModel.BREAKFASTS[his.caller.user.breakfasttyp], rclass: "", c: "-2"});
      sb.push({t: "Activity:    " + UserModel.ACTIVITIES[his.caller.user.activity], rclass: "", c: "-2"});
      sb.push({t: "Snacks:        " + his.caller.user.snacks, rclass: "", c: "-2"});

      his.chat.addChat(his.chat.botD(sb, "Your Settings are: ")); // + his.caller.user.id +

      if(his.caller.userService.isAllSet(his.caller.user)){
          his.chat.addChat(his.chat.bot("Should i save your settings?"));
          his.setStates("Save", 11, "No Thanks", 0)
         his.caller.getAllChats(false,true);
        }else{
        his.chat.addChat(his.chat.bot("I can't save your information, if they are not complete, so go to your profile and complete all."));
        his.chat.addChat(his.chat.bot("Just tab on the Button at the bottom to start a new chat and talk about something with me!"));
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
        }
    }, MiscData.shortWait);
  }

  cont0Step9(his:any) {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s9]);
    let name = his.chat.iFValue;
    console.log("entering cont0Step9");
    console.log(name);
    console.log(his.caller);
    his.chat.aktStep = "cont0Step9";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user(name));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);

    his.events.subscribe(Tags.USERDATA_READY,(data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("So Hello " + name + ", I will remember your name for later."));
        his.chat.addChat(his.chat.bot("Let me introduce myself! I'm your Mensa Buddy"))
        his.chat.addChat(his.chat.bot("I can help you checking out the menu, the webcam or give you some recommendations!"));
        his.setStates("Tell me more", 1, "Ok Bye", 0)
        his.caller.getAllChats(false, true);
     }, MiscData.shortWait);
    });
    his.caller.user.name = name;
    his.caller.userService.save(his.caller.user);
  }

  // from cont0step2
   cont0Step10(his:any): void  {
     his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s10]);
    his.chat.aktStep = "cont0Step10";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithBreakfast();
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, from this you can choose."));
        his.caller.getAllChats(false,false);
        his.chat.set3Picker(true);
        his.caller.threePicker.open();
        his.setStates("Ok", 3, "Not now", 0)
    }, MiscData.shortWait);
  }


//Quit Conversation
   _cont0Step10(his:any): void  {
     his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER_0s10]);
    his.chat.aktStep = "cont0Step10";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you need me."));
        his.chat.addChat(his.chat.bot("For giving you recommendations, check if your settings are complete!"));        
        his.chat.addChat(his.chat.bot("Just tab on the button down there and start a chat!"));        
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }

  cont0Step11(his:any): void  {
    his.chat.aktStep = "cont0Step11";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
     his.events.subscribe(Tags.USERDATA_READY,(data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
    setTimeout(() => { 
        his.events.subscribe(Unibz.UNIBZ_REGISTRATION, (data) => {
        his.events.unsubscribe(Unibz.UNIBZ_REGISTRATION);
        his.events.unsubscribe(Unibz.UNIBZ_REGISTRATION_ERR);
        console.log("UNIBZ_REGISTRATION" );
        console.log(data);
        his.events.subscribe(Tags.USERDATA_REG_READY, (data) => {
            his.events.unsubscribe(Tags.USERDATA_REG_READY);
            his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER0s8]+1);
            his.chat.addChat(his.chat.bot("Profile was saved!"));
            his.chat.addChat(his.chat.bot("Now you are ready to explore this App with me!"));
            his.chat.addChat(his.chat.bot("Just tab on the button down there and start a chat!"));        
            his.chat.addChat(his.chat.bot("See you soon!"));
            his.caller.getAllChats(false,false);
            his.chat.inChat = true;
            his.caller.chatTitle = "Chat Home";
        });
        his.caller.user.email = data.username;
        his.caller.user.logged = true;
        console.log("registrationsave mit:" + his.caller.user);
        his.caller.userService.registrationSave(his.caller.user);
       });
       console.log(his.caller.user);
       his.unibz.doRegister(his.caller.user);
    }
     , MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user);
  }
  


//Quit Conversation
  cont1Step0(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s0]);
    his.chat.aktStep = "cont1Step0";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you need me."));
        his.chat.addChat(his.chat.bot("But remember, i can only give you good recommendations if you complete your settings! Go to your Settings and we will check this out!"));
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }


  cont1Step1(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s1]);
    his.chat.setInputField(false);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Okay that's not your right name?"));
        his.chat.addChat(his.chat.bot("Tell me your real name!"));
        his.chat.setInputField(true, his.caller.user.name, "Enter new name","text", true, "Ok");
        his.setStates("", 0, "", 9); 
        his.caller.getAllChats(false, false);
        setTimeout(() => {
          his.caller.inputId.setFocus();
        }, 300);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user);
  }

   cont1Step2(his:any): void  {
     his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s2]);
    his.chat.aktStep = "cont1Step2";
    his.caller.user.gender =  UserModel.GENDERMALE;
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Bongiorno Monsieur!"));
      his.chat.addChat(his.chat.bot("And maybe your breakfast habbits?"));
      his.setStates("Choose", 10, "Bye", 1 )
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont1Step3(his:any): void  {
    his.chat.aktStep = "cont1Step3";
    his.chat.yesRunState = 3;
    his.caller.cont0();
  }

  cont1Step4(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s4]);
    his.chat.aktStep = "cont1Step4";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Okay with your help, i'm trying to get the very best.."));
       his.setStates("Okay", 4, "Bye", 10 )
      his.caller.getAllChats(false, true);

    }, MiscData.shortWait);
  }

   cont1Step5(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s5]);
   his.chat.aktStep = "cont1Step4";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
       his.chat.changeChat(his.chat.bot("Okay maybe later!"));
       his.chat.addChat(his.chat.bot("But maybe you can tell me what's your weight?"));
       his.setStates("Yes", 6, "Nope", 0 )
      his.caller.getAllChats(false, true);

    }, MiscData.shortWait);
  }

  cont1Step6(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s6]);
    his.chat.aktStep = "cont1Step7";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Okay i need to ask you, what you wanna change.."));
        his.setStates("Weight", 6, "Activity Type", 0 )
        his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }
  
 cont1Step7(his:any): void  {
    his.chat.aktStep = "cont1Step7";
    his.setStates("", 4, "", 0)
    his.cont0()
  }
 cont1Step8(his:any): void  {
    his.chat.aktStep = "cont1Step8";
    his.setStates("", 5, "", 0)
    his.cont0()
  }
 cont1Step9(his:any): void  {
   his.sergelution.postUserAction(his.caller.user, Tags.INTRORUNNER[Tags.INTRORUNNER1s9]);
    his.chat.aktStep = "cont1Step9";
    let name = his.chat.iFValue;
    his.chat.setInputField(false);
    his.setYesButton(name);
    his.caller.getAllChats(false, false);
    his.caller.user.name = name;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      his.setStates(name, 0, "", 0)
      his.cont0()
    });
    his.caller.userService.save(his.caller.user)
  }
 cont1Step10(his:any): void  {
    his.chat.aktStep = "cont1Step10";
    his.setStates("", 6, "", 0)
    his.cont0()
  }



}