import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
     
import { ModalController, Events } from 'ionic-angular';

import { MiscData } from '../providers/miscdata';
import { Data } from '../providers/userdata';
import { Sergelution } from '../providers/sergelution';
import { TimeTicker } from '../providers/akttime';
//import { ModalMenue } from '../pages/modalmenue/modalmenue';
 
import { Datum } from '../providers/datum';
import { Runner } from '../interfaces/runner';
import { Tags } from '../interfaces/tags';


import * as moment from 'moment';

  interface fn {
    (param: any): void;
  }

@Injectable()
export class WebcamRunner implements Runner {
  data: any;
  chat: any;
  caller: any;
  kind: any = 'lunch';
  today: any;

// this is to get rid of the if/if/if and calling the functions via an array
// the drawback is the 'this', that must be explicitly delivered to each function!
  yesFunctions: fn[] = [
    this.cont0Step0,
    this.cont0Step1,
    this.cont0Step2,
    this.cont0Step3,
    this.cont0Step4,
    this.cont0Step5,
    this.cont0Step6,
    this.cont0Step7,
  ];
  noFunctions: fn[] = [
    this.cont1Step0,
    this.cont1Step1,
    this.cont1Step2,
    this.cont1Step3,
  ];
  constructor(    
        public datum: Datum,
        public events: Events,
        public modalCtrl: ModalController,
        public misc: MiscData,
        public userData: Data,
        public sergelution: Sergelution,
        public tt: TimeTicker,
        ) { }
 
  // init rhe runner. provide the callback this and the used chat-instance
  init(callerThis, chat): void {
    this.caller = callerThis;
    this.chat = chat;
  }

  cont1() {
    this.noFunctions[this.chat.noRunState](this);
  }
  cont0() {
    this.yesFunctions[this.chat.yesRunState](this);
  }
  setStates(yes, yesS, no, noS) {
    console.log("setStates");
    console.log(this);
    console.log(yes + "(" + yesS + ") / " + no + "(" + noS + ")");
    this.chat.yesButton = yes;
    this.chat.yesRunState = yesS;
    this.chat.noButton = no;
    this.chat.noRunState = noS;
  }
  setYesButton(yes: string) {
    this.chat.yesButton = yes;
  }

  // Enter from homechat chat-selection - CHECK IN
  cont0Step0(his: any): void {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s0]);
    his.chat.aktStep = "cont0Step0";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Okay!"));
      his.events.subscribe(Tags.MENSAOPEN_DAY, (isOpen) => {
        his.events.unsubscribe(Tags.MENSAOPEN_DAY);
        console.log("arrived " + Tags.MENSAOPEN_DAY);
        if(!isOpen) {
          his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s0]+0);
          his.chat.addChat(his.chat.bot("Sorry, Mensa isn't open today!"));
          his.setStates("", -1, "Okay..",0)
          his.caller.getAllChats(false, true)
          return;      
        }
        if(his.tt.isAfterDinnerTime(moment())) {
          his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s0]+0+0);
          his.chat.addChat(his.chat.bot("Even for Dinner the Mensa is closed now!"));
          his.setStates("", -1, "Ok, ok!",0)
          his.caller.getAllChats(false, true)
          return;      
        }
 
      // if its not LunchTime or DinnerTime, then only Later
        if(his.tt.isLunchTime(moment() || his.tt.isDinerTime(moment()) )) {
           his.chat.addChat(his.chat.bot("Are you gonna go to mensa now or later?"));
           his.setStates("Now", 1, "Later", 3)
           his.caller.getAllChats(false, true)
           return;      
        }
        his.chat.addChat(his.chat.bot("Mensa is closed in the moment. So you can go later."));
        his.setStates("", -1, "Later", 3)
        his.caller.getAllChats(false, true)
      });
      his.tt.checkDayMensaOpen(moment());
    }, MiscData.shortWait);
  }

  // Now from cont0Step0 was selected
  cont0Step1(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s1]);
    his.chat.aktStep = "cont0Step1";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.events.subscribe(Tags.IGONOW_READY, (akt) => {
      console.log(Tags.IGONOW_READY + " received");
      his.events.unsubscribe(Tags.IGONOW_READY);
      his.events.subscribe(Tags.USERDATA_READY, (user) => {
        his.events.unsubscribe(Tags.USERDATA_READY);
        his.chat.changeChat(his.chat.bot("Great! Buon Appetito!"));
        his.chat.addChat(his.chat.bot("Do you want to see how many people are usually in the waitingline?"));
        his.setStates("Yes", 2, "No, Bye!", 0)
        his.caller.getAllChats(false, true);
      });
      if(his.tt.isLunchTime(akt)) {
        his.caller.user.iGoLunch = akt;
        his.caller.userService.save(his.caller.user);
      } else if(his.tt.isDinnerTime(akt)) {
        his.caller.user.iGoDinner = akt;
        his.caller.userService.save(his.caller.user);
      } else {
        his.caller.userService.save(his.caller.user);
      }
    });
    console.log("calling postAccessData");
    his.sergelution.postAccessDate(his.caller.user.id, moment());
  }

//from cont0step1
  // from cont1Step2 
  cont0Step2(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s2]);
    his.chat.aktStep = "cont0Step2";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => {
      his.chat.changeChat(his.chat.bot("See the waiting predictions for today or another day?"));
      his.setStates("Today", 3, "Tomorrow", 2)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }


  cont0Step3(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s3]);
    his.today = moment();
    his.chat.aktStep = "cont0Step3";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.events.subscribe('mensaLine:ready',(days) => {
      his.events.unsubscribe('mensaLine:ready');
      his.chat.changeChat(his.chat.bot("I show you the average number of visitors of today for the last 3 weeks!"));

      his.chat.addChat(his.chat.botCC(":", "Lunch waiting line predictions", "line", 
      his.caller.buildWaitingLineChartData(days, 1), 
      his.caller.buildWaitingLineChartLabel(1),
      his.caller.buildWaitingLineChartColors(1),
      his.caller.buildWaitingLineChartOptions(1)));

      his.chat.addChat(his.chat.botC(":", "Dinner waiting line prediction", "line", 
      his.caller.buildWaitingLineChartData(days, 2), 
      his.caller.buildWaitingLineChartLabel(2) /*,
      his.caller.buildWaitingLineChartColor(2)*/));

      his.chat.addChat(his.chat.bot("Want to see more?"));

      //Put in here the prediction from the line of today
      his.setStates("Tell me more", 4, "Thanks Bye!", 0)
      his.caller.getAllChats(false, true);
    });
    his.sergelution.callFor3Uses(his.today.clone());
  }

   cont0Step4(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s4]);
    his.chat.aktStep = "cont0Step4";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Yeah maybe you can tell me the time when you plan to go today, so i can give better predicitons to you!"));
      //Put in here the prediction from the line of today
      his.setStates("Okay", 5, "No Thanks", 0)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont0Step5(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s5]);
    his.chat.aktStep = "cont0Step5";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithLunchTime();
    setTimeout(() => { 
       his.chat.changeChat(his.chat.bot("OK, you can choose from this."));
        his.caller.getAllChats(false,false);
        his.chat.set3Picker(true);
        his.caller.threePicker.open();
        his.setStates("", 6, "", 0)
    }, MiscData.shortWait);
  }

  cont0Step6(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s6]);
    his.chat.aktStep = "cont0Step6";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Great Thanks for your help!"));
      his.setStates("De Niente", 7, "Bye", 0)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont0Step7(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER0s7]);
    his.chat.aktStep = "cont0Step7";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
       his.chat.changeChat(his.chat.bot("You know i'm always listening, if you want to ask me something!"));
        his.chat.addChat(his.chat.bot("See you Later!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }


//Quit Conversation
  cont1Step0(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER1s0]);
    his.chat.aktStep = "cont1Step0";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you need me."));
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }


  cont1Step1(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER1s1]);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("It is always good to know, how many people are going to mensa.."));
        his.setStates("Right..", 2, "Bye Bye", 0)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

// CHoose tomorrow for predictions
   cont1Step2(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.WEBCAMCHATRUNNER[Tags.WEBCAMRUNNER1s2]);
    his.today = moment();
    his.chat.aktStep = "cont1Step2";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.events.subscribe('mensaLine:ready',(days) => {
      his.events.unsubscribe('mensaLine:ready');
      his.chat.changeChat(his.chat.bot("I show you the average number of visitors of today for the last 3 weeks!"));

      his.chat.addChat(his.chat.botCC(":", "Lunch waiting line predictions", "line", 
      his.caller.buildWaitingLineChartData(days, 1), 
      his.caller.buildWaitingLineChartLabel(1),
      his.caller.buildWaitingLineChartColors(1),
      his.caller.buildWaitingLineChartOptions(1)));

      his.chat.addChat(his.chat.botC(":", "Dinner waiting line prediction", "line", 
      his.caller.buildWaitingLineChartData(days, 2), 
      his.caller.buildWaitingLineChartLabel(2) /*,
      his.caller.buildWaitingLineChartColor(2)*/));

      his.chat.addChat(his.chat.bot("Want to see more?"));

      //Put in here the prediction from the line of today
      his.setStates("Great!", 4, "Thanks Bye!", 0)
      his.caller.getAllChats(false, true);
    });
    his.sergelution.callFor3Uses(his.today.clone().add(1,"days"));
  }


// No, Later was selectet cont0Step1
  cont1Step3(his:any): void  {
    his.chat.aktStep = "cont1Step3";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    if(his.tt.isAfterLunchTime(moment())) {
      his.caller.fillPickerWithDinnerTime();
    } else {
      his.caller.fillPickerWithLunchTime();
    }
    setTimeout(() => { 
       his.chat.changeChat(his.chat.bot("Okay Later.. Please choose.."));
        his.caller.getAllChats(false,false);
        his.setStates("", 6, "", 0)
        his.chat.set3Picker(true);
        his.caller.threePicker.open();
     }, MiscData.shortWait);
  }
  
}
