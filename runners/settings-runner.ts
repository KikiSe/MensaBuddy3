import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
     
import { ModalController, Events } from 'ionic-angular';

import { Chat } from '../providers/chat';
import { MiscData } from '../providers/miscdata';
import { Data } from '../providers/userdata';
import { UserModel } from '../models/user-model';

import { Unibz } from '../providers/unibz';
import { Sergelution } from '../providers/sergelution';
import { TimeTicker } from '../providers/akttime';
 
import { Datum } from '../providers/datum';
import { Runner } from '../interfaces/runner';
import { Tags } from '../interfaces/tags';

  interface fn {
    (param: any): void;
  }

@Injectable()
export class SettingsRunner implements Runner {
  data: any;
  chat: any;
  caller: any;
  kind: any = 'lunch';
  user: any;

// this is to get rid of the if/if/if and calling the functions via an array
// the drawback is the 'this', that must be explicitly delivered to each function!
  yesFunctions: fn[] = [
    this.cont0Step0,
    this.cont0Step1,
    this.cont0Step2,
    this.cont0Step3,
    this.cont0Step4,
    this.cont0Step5,
    this.cont0Step6,
    this.cont0Step7,
    this.cont0Step8,
    this.cont0Step9,
    this.cont0Step10,
    this.cont0Step11,
    this.cont0Step12,
    this.cont0Step13,
    this.cont0Step14,
    this.cont0Step15,
    this.cont0Step16,
  ];
  noFunctions: fn[] = [
    this.cont1Step0,
    this.cont1Step1,
    this.cont1Step2,
    this.cont1Step3,
    this.cont1Step4,
    this.cont1Step5,
    this.cont1Step6,
    this.cont1Step7,
    this.cont1Step8,
    this.cont1Step9,
    this.cont1Step10,
    this.cont1Step11,
    this.cont1Step12,
  ];
  constructor(    
        public datum: Datum,
        public events: Events,
        public modalCtrl: ModalController,
        public misc: MiscData,
        public sergelution: Sergelution,
        public unibz: Unibz,
        public tt: TimeTicker,
        public userService: Data,
        
        ) { }
 
  // init rhe runner. provide the callback this and the used chat-instance
  init(callerThis, chat): void {
    this.caller = callerThis;
    this.chat = chat;
  }

  cont1() {
    this.noFunctions[this.chat.noRunState](this);
  }
  cont0() {
    this.yesFunctions[this.chat.yesRunState](this);
  }
  setStates(yes, yesS, no, noS) {
    this.chat.yesButton = yes;
    this.chat.yesRunState = yesS;
    this.chat.noButton = no;
    this.chat.noRunState = noS;
  }
  setYesButton(yes: string) {
    this.chat.yesButton = yes;
  }
  buildSettingForChat(user): any[] {

      let sb: any[] = [];
      sb.push({t: "Name:        " + user.name, rclass: "", c: "-2"});
      sb.push({t: "Gender:      " + UserModel.GENDERS[+user.gender], rclass: "", c: "-2"});
      sb.push({t: "Age:         " + user.age, rclass: "", c: "-2"});
      sb.push({t: "Snacks:    " + user.snacks, rclass: "", c: "-2"});
      sb.push({t: "Breakfasttyp: " + UserModel.BREAKFASTS[user.breakfasttyp], rclass: "", c: "-2"});
      sb.push({t: "Activity:    " + UserModel.ACTIVITIES[user.activity], rclass: "", c: "-2"});
      sb.push({t: "Height:        " + user.size, rclass: "", c: "-2"});
      sb.push({t: "Weight     : " + user.weight, rclass: "", c: "-2"});
      return sb;
}


  // Enter from homechat chat-selection
  cont0Step0(his: any): void {
    his.chat.aktStep = "cont0Step0";
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s0]);
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Great! So Hello " + his.caller.user.name + "!"));
      if(!his.caller.user.logged){
      his.chat.addChat(his.chat.bot("If you complete your settings, we can save your settings and show you your history and give recommendations"));
      }
      his.chat.addChat(his.chat.botD(his.buildSettingForChat(his.caller.user), "Your Settings are: "));
      //  (" + his.caller.user.id + ") 
      his.setStates("Change Weight", 1, "Others", 1)
      his.caller.getAllChats(false, true);
    }, Chat.shortWait);
  }

  // Weight from cont0Step0 was selected
  cont0Step1(his: any) {
    his.chat.aktStep = "cont0Step1";
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s1]);
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Fine. Just enter your actual weight."));
      his.setStates("", 2, "", 2)
      his.chat.setInputField(true, his.caller.user.weight, "Enter your weight","number", true, "Ok");
      console.log(his.chat);
      his.caller.getAllChats(false,false);
      setTimeout(() => {
        his.caller.inputId.setFocus();
      }, 300);
    }, MiscData.shortWait);
  }

  // from cont0Step1 with new weight 
  cont0Step2(his: any) {
    his.chat.aktStep = "cont0Step2";
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s2]);
    let weight = his.chat.iFValue;
    let oldWeight = his.caller.user.weight;
    his.chat.setInputField(false);
    his.chat.aktStep = "cont0Step2";
    his.chat.addChat(his.chat.user("New weight: " + weight+ " from " + his.caller.user.weight));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.weight = weight;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        let diff = weight - oldWeight;
        console.log(weight);
        console.log(oldWeight);
        console.log(diff);
        if(diff < 0) {
          his.chat.changeChat(his.chat.bot("Whow " + his.caller.user.name + "! The recommendations seem to help you to loose something of you weight."));
        }
        if (diff > 0) {
          his.chat.changeChat(his.chat.bot("Hey " + his.caller.user.name + "! The mensafood seems to taste you ;)"));
        } 
        if(diff == 0) {
          his.chat.changeChat(his.chat.bot("Will you kidden me, " + his.caller.user.name + "? There's no change."));
        }
        his.chat.addChat(his.chat.bot("Maybe change your number of snacks?"));
        his.setStates("Change snacks", 3, "Others", 3)
        his.caller.getAllChats(false, true);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  // from cont0Step2 to change snacknumber!
  cont0Step3(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s3]);
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Just enter the number.."));
      his.chat.setInputField(true, his.caller.user.snacks, "Enter snacks number","number", true, "Ok");
      his.setStates("", 4, "", 2)
      his.caller.getAllChats(false,false);
      setTimeout(() => {
        his.caller.inputId.setFocus();
      }, 300);
    }, MiscData.shortWait);
  }

  cont0Step4(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s4]);
    let snacks = his.chat.iFValue;
    his.chat.aktStep = "cont0Step4";
    his.chat.setInputField(false);
    his.chat.addChat(his.chat.user("Snacks: " + snacks));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.snacks = snacks;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Thanks! I'll store that for you."));
        his.chat.addChat(his.chat.bot("So maybe a change to your breakfast habbits?"));
        his.setStates("Breakfast", 5, "Other", 5)
        his.caller.getAllChats(false, true);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  cont0Step5(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s5]);
    his.chat.aktStep = "cont0Step5";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithBreakfast();
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, from this you can choose."));
        his.caller.getAllChats(false,false);
        his.chat.set3Picker(true);
        his.caller.threePicker.open();
        his.setStates("", 6, "", 0)
    }, MiscData.shortWait);
  }

  // from cont0step5 via 3Pickker with Picker values set.
  cont0Step6(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s6]);
    his.chat.aktStep = "cont0Step6";
    his.chat.addChat(his.chat.user(UserModel.BREAKFASTS[his.caller.user.breakfasttyp]));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("What about your activity..? Will you change that?"));
      his.setStates("Activity", 7, "Other", 7)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont0Step7(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s7]);
    his.chat.aktStep = "cont0Step7";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.fillPickerWithActivity();
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, from this you can choose."));
        his.caller.getAllChats(false,false);
        his.chat.set3Picker(true);
        his.caller.threePicker.open();
        his.setStates("", 14, "", 0)
    }, MiscData.shortWait);
  }

  cont0Step8(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s8]);
    his.chat.aktStep = "cont0Step8";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("So there are four more things you can change."));
      his.chat.addChat(his.chat.bot("Your Age"));
      his.chat.setInputField(true, his.caller.user.age, "Enter new age","number", true, "Ok");
      his.setStates("", 9, "", 2); //9 
      his.caller.getAllChats(false,false);
      setTimeout(() => {
        his.caller.inputId.setFocus();
      }, 300);
    }, MiscData.shortWait);
  }

  cont0Step9(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s9]);
    let age = his.chat.iFValue;
    his.chat.setInputField(false);
    his.chat.aktStep = "cont0Step9";
    his.chat.addChat(his.chat.user("New age " + age));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.age = age;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Thanks! I'll store that for you."));
        his.chat.addChat(his.chat.bot("Your height."));
        his.chat.setInputField(true, his.caller.user.size, "Enter new height","number", true, "Ok");
        his.setStates("", 10, "", 2) //10
        his.caller.getAllChats(false, false);
        setTimeout(() => {
          his.caller.inputId.setFocus();
        }, 300);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  cont0Step10(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s10]);
    let height = his.chat.iFValue;
    his.chat.setInputField(false);
    his.chat.aktStep = "cont0Step10";
    his.chat.addChat(his.chat.user("New height " + height));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.height = height;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Thanks! I'll store that for you."));
        his.chat.addChat(his.chat.bot("Your Name."));
        his.chat.setInputField(true, his.caller.user.name, "Enter new name","text", true, "Ok");
        his.setStates("", 11, "", 2); //11
        his.caller.getAllChats(false, false);
        setTimeout(() => {
          his.caller.inputId.setFocus();
        }, 300);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user);
  }

   cont0Step11(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s11]);
    let name = his.chat.iFValue;
    his.chat.setInputField(false);
    his.chat.aktStep = "cont0Step11";
    his.chat.addChat(his.chat.user("New name " + name));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.name = name;
    his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Thanks! I'll store that for you."));
        his.chat.changeChat(his.chat.bot("So your gender is the last thing that can be changed."));
        his.setStates("Change gender", 12, "enough already!",12 )
        his.caller.getAllChats(false,true);
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
  }

  cont0Step12(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s12]);
    his.chat.aktStep = "cont0Step12";
    his.chat.addChat(his.chat.user("Change gender"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("So let me know?"));
      his.setStates(UserModel.GENDERS[UserModel.GENDERFEMALE], 13, UserModel.GENDERS[UserModel.GENDERMALE], 4)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont0Step13(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s13]);
    his.chat.aktStep = "cont0Step13";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.caller.user.gender = UserModel.GENDERFEMALE;
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Let's see your profile, before saving?"));
      his.setStates("Show Profile", 16, "No Bye Bye", 0)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont0Step14(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s14]);
    his.chat.aktStep = "cont0Step14";
    his.chat.addChat(his.chat.user(UserModel.ACTIVITIES[his.caller.user.activity]));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("So there are four more things you can change. Will you go on or are you done?"));
      his.setStates("Go on", 8, "I'm done", 12)
      his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

   cont0Step15(his: any) {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s15]);
    his.chat.aktStep = "cont0Step15";
    his.chat.addChat(his.chat.user(his.chat.yesButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      if(!his.caller.user.logged){
       his.events.subscribe(Unibz.UNIBZ_REGISTRATION, (data) => {
        his.events.unsubscribe(Unibz.UNIBZ_REGISTRATION);
        his.events.unsubscribe(Unibz.UNIBZ_REGISTRATION_ERR);
        console.log("UNIBZ_REGISTRATION" );
        console.log(data);
        his.events.subscribe(Tags.USERDATA_REG_READY, (data) => {
            his.events.unsubscribe(Tags.USERDATA_REG_READY);
            his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s15]+1);
            his.chat.addChat(his.chat.bot("Profil was saved!"));
            his.chat.addChat(his.chat.bot("OK, you know I'm listening when you need me."));
            his.chat.addChat(his.chat.bot("See you soon!")); 
            his.caller.getAllChats(false,false);
            his.chat.inChat = true;
            his.caller.chatTitle = "Chat Home";
        });
        
        his.caller.user.email = data.username;
        his.caller.user.logged = true;
        his.caller.userService.registrationSave(his.caller.user);
       });
       his.unibz.doRegister(his.caller.user);
      }else{
        console.log("changeSettings" );
        his.events.subscribe(Unibz.UNIBZ_PROFILE_PUT, (data) => {
          his.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT);
          his.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT_ERR);
          console.log("UNIBZ_PROFILE_PUT" );
      //    console.log(data);
          his.events.subscribe(Tags.USERDATA_REG_READY, (data) => {
            his.events.unsubscribe(Tags.USERDATA_REG_READY);
            his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s15]+2);
             his.chat.changeChat(his.chat.bot("Profil was changed!"));
             his.chat.addChat(his.chat.bot("OK, you know I'm listening when you need me."));
             his.chat.addChat(his.chat.bot("See you soon!")); 
             his.caller.getAllChats(false,false);
             his.chat.inChat = true;
             his.caller.chatTitle = "Chat Home";
          });
          his.caller.user.logged = true;
          his.caller.userService.registrationSave(his.caller.user);
        });
        his.events.subscribe(Unibz.UNIBZ_PROFILE_PUT_ERR, (err) => {
          his.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT);
          his.events.unsubscribe(Unibz.UNIBZ_PROFILE_PUT_ERR);
          console.log("UNIBZ_PROFILE_PUT_ERR" );
          console.log(err);
           his.chat.addChat(his.chat.bot("Profil was not changed because of an error!"));
        });
        his.unibz.putProfile(his.caller.user);
      }
      
    }, MiscData.shortWait);
  }

  cont0Step16(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s16]);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user("Show Profile"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.events.subscribe(Tags.USERDATA_READY, (data) => {
      his.events.unsubscribe(Tags.USERDATA_READY);
      his.caller.user = data;
      his.sergelution.postUserData(his.caller.user);      
      setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("Here is your new profile"));
        his.chat.addChat(his.chat.botD(his.buildSettingForChat(his.caller.user), "Your Settings are :"));
        if(his.caller.userService.isAllSet(his.caller.user)){
        his.chat.addChat(his.chat.bot("Should i save your settings?"));
          his.setStates("Save", 15, "No Thanks", 0)
         his.caller.getAllChats(false,true);
        }else{
        his.chat.addChat(his.chat.bot("Please complete your profile, before we save your settings!"));
        his.chat.addChat(his.chat.bot("Just tab on the Button at the bottom to start a new chat and talk about something with me!"));
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
        }
      }, MiscData.shortWait);
    });
    his.caller.userService.save(his.caller.user)
    });
  }

  


//Quit Conversation
  cont1Step0(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s0]);
    his.chat.aktStep = "cont1Step0";
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    his.sergelution.postUserData(his.caller.user);      
    setTimeout(() => { 
        his.chat.changeChat(his.chat.bot("OK, you know I'm listening when you need me."));
        his.chat.addChat(his.chat.bot("See you soon!"));
        his.caller.getAllChats(false,false);
        his.chat.inChat = true;
        his.caller.chatTitle = "Chat Home";
    }, MiscData.shortWait);
  }


  cont1Step1(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s1]);
    his.chat.aktStep = "cont1Step1";
    his.chat.addChat(his.chat.user("Others"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Maybe change your number of snacks?"));
      his.setStates("Change snacks", 3, "Others", 3)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  // This is used for using the OK (store)-Button if shown right of the textfield
  // to forward to the 0-Event.
  cont1Step2(his:any): void  {
    his.cont0();
  }

  cont1Step3(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s3]);
    his.chat.aktStep = "cont1Step3";
    his.chat.addChat(his.chat.user("No snacks change, other"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("So maybe a change to your breakfast habbits?"));
      his.setStates("Breakfast", 5, "Other", 5)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

  cont1Step4(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER0s13]+1);
    his.chat.aktStep = "cont1Step4";
    his.caller.user.gender =  UserModel.GENDERMALE;
    his.chat.addChat(his.chat.user(his.chat.noButton));
    his.chat.addChat(his.chat.bot("..."));
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Let's see your profile, before saving?"));
      his.setStates("See profile", 16, "No Bye", 0)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

   cont1Step5(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s5]);
   his.chat.aktStep = "cont1Step5";
    his.chat.addChat(his.chat.user("No breakfast change, other"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("What about your activity..? Will you change that?"));
      his.setStates("Aktivity", 7, "Other", 7)
      his.caller.getAllChats(false, true);
    }, MiscData.shortWait);
  }

   cont1Step6(his:any): void  {
    his.setStates("", 16, "", 0)
    his.cont0();

  }
  cont1Step7(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s7]);
    his.chat.aktStep = "cont1Step7";
    his.chat.addChat(his.chat.user("No activity change, other"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("There are four more things you can change. You can go through or quit and save."));
      his.setStates("Four more", 8, "Quit and Save", 8)
      his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }
  cont1Step8(his:any): void  {
    his.chat.aktStep = "cont1Step8";
    his.setStates("Quit and Save", 13, "", 0)
    his.caller.cont0();
  }
  // Skip from cont0step8
  cont1Step9(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s9]);
    his.chat.setInputField(false);
    his.chat.aktStep = "cont1Step9";
    his.chat.addChat(his.chat.user("No new age"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Your height."));
      his.chat.setInputField(true, his.caller.user.size, "Enter new height","number", true);
      his.setStates("", 10, "", 10)
      his.caller.getAllChats(false,false);
      setTimeout(() => {
        his.caller.inputId.setFocus();
      }, 300);
    }, MiscData.shortWait);
  }

  // Skip from cont0step9 or cont1step9
  cont1Step10(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s10]);
    his.chat.setInputField(false);
    his.chat.aktStep = "cont1Step10";
    his.chat.addChat(his.chat.user("No new height"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("Your name."));
      his.chat.setInputField(true, his.caller.user.name, "Enter new name","text", true);
      his.setStates("", 11, "", 11)
      his.caller.getAllChats(false,false);
      setTimeout(() => {
        his.caller.inputId.setFocus();
      }, 300);
    }, MiscData.shortWait);
  }

  cont1Step11(his:any): void  {
    his.sergelution.postUserAction(his.caller.user, Tags.SETTINGSRUNNER[Tags.SETTINGSRUNNER1s11]);
    his.chat.setInputField(false);
    his.chat.aktStep = "cont1Step11";
    his.chat.addChat(his.chat.user("No new name"));
    his.chat.addChat(his.chat.bot("..."));
    his.caller.getAllChats(false, false);
    setTimeout(() => { 
      his.chat.changeChat(his.chat.bot("So your gender is the last thing that can be changed if it has changed."));
      his.setStates("Change gender", 12, "enough already!", 6)
      his.caller.getAllChats(false,true);
    }, MiscData.shortWait);
  }

  cont1Step12(his:any): void  {
    his.chat.aktStep = "cont1Step12";
    his.chat.yesRunState = 16;
    his.caller.cont0();
  }
}
