import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { Chat } from '../providers/chat';
import { Data } from '../providers/userdata';
import { Datum } from '../providers/datum';
import { FlurryService } from '../providers/flurryservice';
import { MiscData } from '../providers/miscdata';
import { TimeTicker } from '../providers/akttime';
import { Sergelution } from '../providers/sergelution';
import { Unibz } from '../providers/unibz';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { HomeMenuPage } from '../pages/homemenu/homemenu';
import { HomeChatPage } from '../pages/homechat/homechat';
import { IntroPage} from '../pages/intro/intro';
import { MenuePage } from '../pages/menue/menue';
import { ModalMenue } from '../pages/modalmenue/modalmenue';
import { ModalMenueLD } from '../pages/modalmenueld/modalmenueld';
import { Modalinfo } from '../pages/modalinfo/modalinfo';
import { ModalSurvey } from '../pages/modalsurvey/modalsurvey';
import { ModalDaysFreq } from '../pages/daysfreq/daysfreq';
import { RecommendationPage} from '../pages/recommendation/recommendation';
import { SettingsPage} from '../pages/settings/settings';
import { TabsPage} from '../pages/tabs/tabs';
import { WebcamPage} from '../pages/webcam/webcam';
import { EnterPage} from '../pages/enter/enter';

import '../../node_modules/chart.js/dist/Chart.bundle.min.js';
import { ChartsModule } from 'ng2-charts';

import { MultiPickerModule } from 'ion-multi-picker';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomeChatPage,
    HomeMenuPage,
    IntroPage,
    MenuePage,
    ModalMenueLD,
    ModalMenue,
    ModalDaysFreq,
    RecommendationPage,
    SettingsPage,
    TabsPage,
    WebcamPage,
    Modalinfo,
    EnterPage,
    ModalSurvey,
    
  ],
  imports: [
    HttpModule,
    BrowserModule,
    MultiPickerModule,
    ChartsModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: 'Back',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'top',
       platforms: {
          ios: {
            tabsPlacement: 'bottom',
          }
        }
    }),
    IonicStorageModule.forRoot(
 //   {
   //   name: '__mydb',
   //   driverOrder: ['sqlite','indexeddb','websql']
   // }

    )
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomeChatPage,
    HomeMenuPage,
    MenuePage,
    ModalMenue,
    ModalMenueLD,
    ModalDaysFreq,
    IntroPage,
    RecommendationPage,
    SettingsPage,
    TabsPage,
    WebcamPage,
    Modalinfo,
    EnterPage,
    ModalSurvey,
  ],
  providers: [
    Storage,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MiscData,
    Unibz,
    Sergelution,
    FlurryService, 
    Data,
    Datum,
    TimeTicker
  ]
})
export class AppModule {}
