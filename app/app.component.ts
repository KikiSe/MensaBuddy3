import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SwitchService }     from '../services/switchservice';
import { Sergelution } from '../providers/sergelution';
import { Unibz } from '../providers/unibz';
import { Data } from '../providers/userdata';
import { Datum } from '../providers/datum';
import { MiscData } from '../providers/miscdata';

import { SettingsPage} from '../pages/settings/settings';
import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage } from '../pages/intro/intro';
import { HomeChatPage } from '../pages/homechat/homechat';
import { EnterPage} from '../pages/enter/enter';


import { Tags } from '../interfaces/tags';
import * as moment from 'moment';
@Component({
  templateUrl: 'app.html',

  providers: [SwitchService]  //, FlurryService]
})
export class MyApp {
  rootPage:any;

  constructor(public events: Events, 
              public platform: Platform, 
              public statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private switchService: SwitchService, 
              private sergelutionService: Sergelution,
              private miscData: MiscData,  
              private unibz: Unibz, 
              private user: Data,
              private datum: Datum              
              ) {
      platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      //this.user.reset();

      miscData.getMiscData();

      this.events.subscribe(Tags.USERDATA_READY, (userModel) => {
        this.events.unsubscribe(Tags.USERDATA_READY);
        console.log("app.component.ts: user.id=" + userModel.id);

        if(userModel.logged){
          this.rootPage = EnterPage;
        }else{
          this.rootPage = IntroPage;
        }
      });
      console.log("app.component.ts: calling prepData")
      this.user.prepData();
//      this.user.assureId();
// switchService can't be unsubscribe. So it can be used only in situations,
// where the code can never be thrown away.
      switchService.switchSource$.subscribe(
        to => {
          console.log("MyApp: " + to);
          if(to === "chat") {
            this.rootPage = HomeChatPage;
          }
          if(to === "menu") {
           this.rootPage = TabsPage;
           //this.rootPage.setTabPos(this.tabpos);
          }
        },
        (error: any) => {
          console.log("MyApp: " + error);
        },
        () => {
          console.log("MyApp: completed!");
        }  
  
      );
      let d = moment().format("YYYY-MM-DD");  
      unibz.setToday(d);
    });
  }
}

